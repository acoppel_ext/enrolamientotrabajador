-- Function: fnguardarparametroscontrol(character, integer, integer, integer, character)

-- DROP FUNCTION fnguardarparametroscontrol(character, integer, integer, integer, character);

CREATE OR REPLACE FUNCTION fnguardarparametroscontrol(
    character,
    integer,
    integer,
    integer,
    character)
  RETURNS integer AS
$BODY$
	DECLARE

    cParametrosEnvio        ALIAS FOR $1;
    iFolioEnrolamiento      ALIAS FOR $2;
    iResponse               ALIAS FOR $3;
    iOpcion                 ALIAS FOR $4;
    ipModulo                ALIAS FOR $5;
    iRespuesta              INTEGER;
    iKeyx                   INTEGER;
    

/***
* Pendiente: 961
* Empleado: 92919235 - Eduardo Chavez 
* Descripcion: Guarda los parametros de ejecucion de componentes en la
* pagina enrolamientotrabajador.zip, asi como la respuesta y folioenro-
* lamiento
***/

	BEGIN

        iKeyx      = 0;
        iRespuesta = 0;


    

        IF ( iOpcion = 1 ) THEN       
	
            INSERT INTO tbctrlenrolamiento (parametrosenvio,folioenrolamiento,ipmodulo)
            VALUES(cParametrosEnvio,iFolioEnrolamiento,ipModulo);


            iRespuesta = 1;
        
        ELSIF ( iOpcion = 2 ) THEN

            IF EXISTS( SELECT 'x' FROM tbctrlenrolamiento WHERE folioenrolamiento = iFolioEnrolamiento AND 
                    fechaalta::DATE = CURRENT_DATE AND parametrosenvio = cParametrosEnvio AND response = 0 ) THEN

                SELECT MAX(keyx) INTO iKeyx FROM tbctrlenrolamiento WHERE folioenrolamiento = iFolioEnrolamiento 
                AND fechaalta::DATE = CURRENT_DATE AND parametrosenvio = cParametrosEnvio;

                UPDATE tbctrlenrolamiento SET response = iResponse WHERE folioenrolamiento = iFolioEnrolamiento 
                AND fechaalta::DATE = CURRENT_DATE AND keyx = iKeyx;

            ELSE

                INSERT INTO tbctrlenrolamiento (parametrosenvio,folioenrolamiento,ipmodulo)
                VALUES(cParametrosEnvio,iFolioEnrolamiento,ipModulo);
            
            END IF;


            iRespuesta = 1;

        ELSE

            iRespuesta = -1;

        END IF;


	    RETURN  iRespuesta;
				
	END;                                              
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnguardarparametroscontrol(character, integer, integer, integer, character)
  OWNER TO postgres;
