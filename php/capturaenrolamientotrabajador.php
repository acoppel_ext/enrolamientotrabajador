<?php
include_once ('clases/CEnrolTrabajador.php');
include_once("JSON.php");
$json = new Services_JSON();
$arrResp = array();
$ar = '';


//Se declaran variables donde se Optiene el valor que les fue mandado del js atraves de ajax
//Primera seccion donde se obtienen los datos del formulario
$iEmpleado =  isset($_POST['empleado']) ? $_POST['empleado']: '0';
$opcion = isset($_POST['opcion']) ? $_POST['opcion']: '0';
$curp = isset($_POST['curp']) ? $_POST['curp']: '';
$solicitud = isset($_POST['solicitud']) ? $_POST['solicitud']: '0';
$iFolio = isset($_POST['folio']) ? $_POST['folio']: '0';
$chExcepcion = isset($_POST['excepcion']) ? $_POST['excepcion']: '0';
$estatus = isset($_POST['estatus']) ? $_POST['estatus']: '0';
$iFolioSolicitud = isset($_POST['foliosolicitud']) ? $_POST['foliosolicitud']: '0';
$iFolioEnrol = isset($_POST['folioenrol']) ? $_POST['folioenrol']: '0';
$iSelecciono = isset($_POST['selecciono']) ? $_POST['selecciono']: '0';
$idedos = isset($_POST['dedo']) ? $_POST['dedo']: '0';
$iEstatusCancelar = isset($_POST['estatusCancelar']) ? $_POST['estatusCancelar']: '0';
$sTexto = isset($_POST['stexto']) ? $_POST['stexto']: '';
//$iTipoServicio = isset($_POST['tiposervicio']) ? $_POST['tiposervicio']: '0';
$iFolioServicio = isset($_POST['folioservicio']) ? $_POST['folioservicio']: '0';
$opcionmensaje = ISSET($_POST['opcionmensaje']) ? $_POST['opcionmensaje']: '0';
$iNumEmpGerente = ISSET($_POST['numempgerente']) ? $_POST['numempgerente']: '0';
$iautorizaciongerencial = ISSET($_POST['autorizaciongerencial']) ? $_POST['autorizaciongerencial']: '0';
$itipoenrolamiento = ISSET($_POST['tipoenrolamiento']) ? $_POST['tipoenrolamiento'] : '0';
$iExcepcionesHuellas = ISSET($_POST['excepcioneshuellas']) ? $_POST['excepcioneshuellas'] : '0';
$chCurp = isset($_POST['chCurp']) ? $_POST['chCurp']: '';

$cParametrosEnvio    = isset($_POST['parametrosenvio']) ? $_POST['parametrosenvio'] : '0';           
$iResponse           = isset($_POST['response'])        ? $_POST['response']        : '0';
$iOpcion             = isset($_POST['iopcion'])         ? $_POST['iopcion']         : '0'; 

//CLogImpresion::escribirLog("Valor del FolioServicio: ".$iFolioServicio);
switch($opcion) 
{
	case 1:
		//consulta el enrolamiento del promotor
		$arrResp = CEnrolTrabajador::validarEstadoPromotor($iEmpleado);
	    break;
	case 2:
		$arrResp = CEnrolTrabajador::guardarDatos($iEmpleado,$solicitud,$curp,$iFolioSolicitud);
		break;
	case 3:
		$arrResp = CEnrolTrabajador::guardaExcepciones($iFolio,$chExcepcion,$idedos);
		break;
	case 4:
		$arrResp = CEnrolTrabajador::cancelarEnrolamiento($iFolio,$estatus);
		break;
	case 5:
		$arrResp = CEnrolTrabajador::obtenerLigaFormatoEnrolamiento($iFolio);
		break;
	case 6:
		$arrResp = CEnrolTrabajador::obtIpTienda();
		break;
	case 7:
		$arrResp = CEnrolTrabajador::validaNIST($iFolioEnrol);
		break;
	case 8:
		$arrResp = CEnrolTrabajador::consultarDedosEnManos($iFolio,$iSelecciono);
		break;
	case 9:
		$arrResp = CEnrolTrabajador::actualizarEstatusEnrol($iFolioEnrol,$iEstatusCancelar);
		break;
	case 10:
		$arrResp = CEnrolTrabajador::borrarExcepciones($iFolio);
		break;
	case 11:
		$arrResp = CEnrolTrabajador::verificacionEnrolAutoriza($iFolio);
		break;
	case 12:
		$arrResp = CEnrolTrabajador::limpiarHuellasEnrol($iFolio);
		break;
	case 13:
		$arrResp = CEnrolTrabajador::actualizarEnrolamiento($iFolio);
		break;
	case 14:
		$arrResp = CEnrolTrabajador::grabarLogJs($sTexto);
		break;
	case 15:
		$arrResp = CEnrolTrabajador::excepcionarDedosEnRolTrabajador($iFolioEnrol);
		break;
	/* NOTA: Pend. 301 - SE INABILITA ESTA SECCION (CAMBIOS QUE SE HABIAN METIDO EN EL Pend. 280), DEBIDO A CAMBIOS EN LA NORMATIVA, Fecha del cambio: 06/Nov/2017
	case 16:
		$arrResp = CEnrolTrabajador::ValidarObtencionDeSelloDeVoluntad($iFolioServicio);
		break;
	case 17:
		$arrResp = CEnrolTrabajador::ObtencionFlujoServicio($iFolioServicio);
		break;
	*/
	case 18:
		$arrResp = CEnrolTrabajador::ObtenerDatosConfiguracion();
		break;
	/*
	case 19:
		$arrResp = CEnrolTrabajador::RechazarEnrolamiento($iFolioServicio);
		break;
	*/
	case 20:
		$arrResp = CEnrolTrabajador::llamarMarcaEI($iEmpleado, $iFolioServicio);  
		break;
	case 21:
	    $arrResp = CEnrolTrabajador::validarLevantamientoEI($iFolioServicio, $curp);
	    break;
	default:
		break;

	//inicia pendiente 316.1 -----------------------------------------------------------------
	case 22:
		$arrResp = CEnrolTrabajador::fnObtenerIpServidor();
		break;
	case 23:
		$arrResp = CEnrolTrabajador::fnconsultarnumempleado($iEmpleado);
		break;
	case 24:
		$arrResp = CEnrolTrabajador::fnobtenermensajegerenteenrol($opcionmensaje);
		break;
	case 25:
		$arrResp = CEnrolTrabajador::fnguardarbitacoraenrolamiento($iFolioSolicitud, $iFolioEnrol, $iEmpleado, $iNumEmpGerente, $curp, $iautorizaciongerencial, $itipoenrolamiento, $iExcepcionesHuellas);
		break;
	case 26:
		$arrResp = CEnrolTrabajador::fnobtenerclaveoperacion( $iFolioSolicitud );
		break;
	case 27:
			date_default_timezone_set('America/Mazatlan');
			setlocale(LC_TIME, 'spanish');
			$arrResp['status'] = 1;
			$arrResp['descripcion'] = 'CONSULTA EXITOSA';
			$arrResp['fecha'] = strftime("%Y%m%d");
			$arrResp['fechafirma'] = strftime( "%d-%m-%Y" );
		break;
	case 28:
		$arrResp = CEnrolTrabajador::f_obtenerclaveoperacionservicios( $iFolioSolicitud );
		break;
	case 29:
		$arrResp = CEnrolTrabajador::verificarFolio($curp,$iFolioSolicitud,$solicitud);
	break;
	case 30:
		$arrResp = CEnrolTrabajador::guardarParametrosControl($cParametrosEnvio,$iFolioEnrol,$iResponse,$iOpcion);
	break;
	case 31:
		$arrResp = CEnrolTrabajador::guardarExcepcion($chCurp);
		break;
	case 32:
		$arrResp = CEnrolTrabajador::verificaconsecutivoeibio($iFolioServicio);
		break;
	

}
echo $json->encode($arrResp);

?>