-- Table: tbctrlenrolamiento

-- DROP TABLE tbctrlenrolamiento;

CREATE TABLE tbctrlenrolamiento
(
  keyx serial NOT NULL,
  parametrosenvio text NOT NULL DEFAULT ''::text,
  folioenrolamiento integer NOT NULL DEFAULT 0,
  response integer NOT NULL DEFAULT 0,
  fechaalta timestamp with time zone NOT NULL DEFAULT timenow(),
  ipmodulo character(15) NOT NULL DEFAULT '0'::bpchar
)
WITH (
  OIDS=FALSE
);
ALTER TABLE tbctrlenrolamiento
  OWNER TO sysaforeglobal;
