var iEmpleado;
var VERIFICACION_MORPHO = 0;
var MANO_DERECHA = 1;
var MANO_IZQUIERDA = 2;
var VERIFICACION_EMPLEADO = 3;
var CAPTURA_FIRMA = 4;
var PULGARES =  5;
var GUARDAR = 6;
var GUARDAR_IZQUIERDA = 7;
var VALIDAR_OPCION = 8;
var AMBAS_MANOS = 9;
var VALIDAR_NIST = 10;
var FALTAN_DEDOS = 11;
var ESTA_CONECTADO_SIGNPAD = 12;
var VERIFICACION = 13;
var VERIFICACION_EMPLEADO_HE = 33; //pendiente 603
var iOpcion = 0;
var iEliminaCont = 1;
var iSelecciono = 0;
var numeroEmpleadoAutoriza = 0;
var sIpSerHuellas = '';
var iFolio = 0; //Folio generado al ser aceptado el enrolador
var sRutaM ="C:\\sys\\progs\\HE442CTE.EXE";
var smTipoSolicitud = 0;
var chCurp = '';
var cont = 0;
var iFolioSolicitud = 0;
var ipServidor;
var OBJ = '';
var iContPulgar = 0;
var iContIntentosSelloVoluntad = 2;
var iEstatusSello = 0;
var cFlujoServicio = "";
var iMinutos = 0;
var iDias = 0;
var iIntentos = 0;
var iBanderaSalir = false;
//variables para la recaptura
var reDerecha = 0;
var reIzquierda = 0;
var reAmbas = 0;
var contIntentos = 0;
var contadorNIST = 0;
var sCodigo1 = '';
var sCodigo2 = '';
var sCodigo3 = '';
var sCodigo4 = '';
var sCodigo5 = '';
var sCodigo6 = '';
var sCodigo7 = '';
var sCodigo8 = '';
var sCodigo9 = '';
var sCodigo10 = '';
var iBandDed1 = 0;
var iBandDed2 = 0;
var iBandDed3 = 0;
var iBandDed4 = 0;
var iBandDed5 = 0;
var iBandDed6 = 0;
var iBandDed7 = 0;
var iBandDed8 = 0;
var iBandDed9 = 0;
var iBandDed10 = 0;
var numeroIntentos = 0;
var sMensajeAux = '';

var iNumCapturaHuella = 0;//Variable para guardar cuantas capturas completas de huellas se han realizado.
var ENROLAMIENTODOCTOS = 50; //VFG ENROLAMIENTODOCTOS
var sRutaDigiDoctos = "C:\\SYS\\PAFSCANIMGNET\\DIGIDOCTOSSERVICIOS.EXE";
var iValExpIde = 0;

var contadorMsj = 0;


//variables para la peticion 316
var sRutaHE = "C:\\sys\\progs\\HE.Exe"; //ruta de he para la validacion del gerente
var servidorIP = null;
const VALIDACION_HUELLA_GERENTE = 14;
const TOMAR_FOTO_CLIENTE = 15;
const CAPTURAR_FIRMA_GERENTE = 16;
const CAPTURA_LEYENDA_EXCEPCION = 17;
const OK___ = 1;
const OBTENER_CLAVE_OPERACION = 26;
const OBTENER_CLAVE_OPERACION_SERVICIO = 28;
var iNumEmpSensor = 0;
var intentosSensor = 0;
var iexcepcion = null; // 1-> solo mano derecha, 2-> solo mana izquierda, 3-> ambas manos lesionadas, 4-> ambas manos amputadas
var cRutaCamara = "C:\\sys\\ts\\TSFOTOENROLAMIENTO.EXE";
var cRutaFirma = "C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";
var iAutorizacionGerencial = 0;
var iclaveoperacion = null;
var iclaveoperacionservicio = null;
var dFechaactual = null;
var iExcepcionesHuellas = 0;
// fin variables peticion 316

//PDTE 961: Enrolamiento en blanco
var cParametrosEnvio = '';
var iFolioEnrola     = 0;
var iResponse        = 0;
var contIntentosEnrol = 0;

//Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
var OSName="Desconocido";
if (navigator.appVersion.indexOf("Win")!=-1) OSName="Windows";
if (navigator.appVersion.indexOf("Mac")!=-1) OSName="MacOS";
if (navigator.appVersion.indexOf("X11")!=-1) OSName="UNIX";
if (navigator.appVersion.indexOf("Linux")!=-1) OSName="Linux";
if (navigator.appVersion.indexOf("Android")!=-1) OSName="Android";

$(document).ready(function(){
	$('.content-loader').hide();
	document.oncontextmenu = function(){return false;};

	$(document).keydown(function(tecla){
		//Validar tecla F5 NO REALICE NINGUNA ACCION
		if (tecla.keyCode == 116) {
			return false;
		}
	});
	//Validar tecla F2
	$(document).keydown(function (tecla) {
		//Validar tecla F2 NO REALICE NINGUNA ACCION
		if (tecla.keyCode == 113) {
			var sMensaje = "El promotor: " + iEmpleado + " dio click en la tecla F2";
			grabarLogJs(sMensaje);
			return false;
		}
	});
	//Validar tecla F6
	$(document).keydown(function (tecla) {
		//Validar tecla F6 NO REALICE NINGUNA ACCION
		if (tecla.keyCode == 117) {
			var sMensaje = "El promotor: " + iEmpleado + " dio click en la tecla F6";
			grabarLogJs(sMensaje);
			return false;
		}
	});
	// Validar ctrl + r
	$(document).on("keydown", function (e) {
		e = e || window.event;
		if (e.ctrlKey) {
			var c = e.which || e.keyCode;
			if (c == 82) {
				e.preventDefault();
				e.stopPropagation();
			}
		}
	});


	/*Obtener Variables pasadas por URL*/
	if(OSName == "Android"){
		smTipoSolicitud = getParameterByName("tiposolicitud");
		chCurp = getParameterByName("curp");
		iFolioSolicitud = getParameterByName("foliosolicitud");
		iEmpleado = getParameterByName("empleado");
		chCurp = chCurp.toUpperCase();
	}else{
		smTipoSolicitud = getQueryVariable("tiposolicitud");
	chCurp = getQueryVariable("curp");
	iFolioSolicitud = getQueryVariable("foliosolicitud");
	iEmpleado = getQueryVariable("empleado");
	chCurp = chCurp.toUpperCase();
	}

	smTipoSolicitud = getQueryVariable("tiposolicitud");
	chCurp = getQueryVariable("curp");
	iFolioSolicitud = getQueryVariable("foliosolicitud");
	iEmpleado = getQueryVariable("empleado");
	chCurp = chCurp.toUpperCase();

	fnobteneripservidor();

	var sMensaje = '[capturaenrolamientotrabajador] --> --------------------------------------------------------------------------------------------------------------------------------------------------';
	grabarLogJs(sMensaje);
	var sMensaje = '[capturaenrolamientotrabajador] --> ------------------------------------------------------------INICIO DEL ENROLAMIENTO---------------------------------------------------------------';
	grabarLogJs(sMensaje);
	var sMensaje = '[capturaenrolamientotrabajador] --> --------------------------------------------------------------------------------------------------------------------------------------------------';
	grabarLogJs(sMensaje);

	var sMensaje = '[capturaenrolamientotrabajador] --> DATOS DE ENTRADA: TipoSolicitud: ' + smTipoSolicitud + ', Curp: ' + chCurp + ', FolioSolicitud: ' + iFolioSolicitud + ', Empleado: ' + iEmpleado;
	grabarLogJs(sMensaje);

	//msj = 'Se  proseguir\u00E1  con  la  captura  de  las  huellas dactilares.';
	//mostrarMensajePrincipal(msj);
	sTexto = 'Entro al enrolamiento por primera vez para  el folio ->[' + iFolioSolicitud + ']';
	grabarLogJs(sTexto);
	ifolio = verificarFolio();

	$("#carga").dialog
	({
		title: 'Formato de Enrolamiento Trabajador',
		autoOpen: false,
		resizable: false,
		width: '820',
		height: '820',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
		},
		buttons:
		{
			"Aceptar" : function()
			{
				sTexto = '[capturaenrolamientotrabajador::Inicio] --> Promotor dio clic al boton Aceptar, Folio Solicitud: ' + iFolioSolicitud + ' Folio: ' + iFolio + ' TipoSolicitud: ' + smTipoSolicitud;
				grabarLogJs(sTexto);
				//Se agrega esta validacion para actualizar el estatus del enrolamiento a 0 cuando el tipo de solicitud no sea una afiliacion
				mensajeLog = 'SE INICIA UN TipoSolicitud = ' + smTipoSolicitud;
				grabarLogJs(mensajeLog);
				
				if(smTipoSolicitud != 26 && smTipoSolicitud != 27 && smTipoSolicitud != 33)
				{
					if(actualizarEnrolamiento(iFolio))
					{
						//llamarMarcaEI(iEmpleado, iFolioSolicitud);  //AQUI SE LE MANDA EL:   FolioServicio, EN LA VARIABLE: iFolioSolicitud
						iValExpIde = 0;
						
						if (smTipoSolicitud == 2040 || smTipoSolicitud == 2022 )  //SI ES EL NUEVO SERVICIO.
						{
							ValidarLevantamientoEI();
						}

						if(iValExpIde == 1)  //NO LEVANTARA LA PAGINA DEL: Exp. Identif., PORQUE YA LO TIENE
						{
							console.log("Entro a ejecutar DIGIDOCTOSSERVICIOS");
							grabarLogJs("Entro a ejecutar DIGIDOCTOSSERVICIOS");
							//VFG Llamar DigiDoctos
							iOpcion = 50; //llamar enrolamiento
							sParametrox = "1 50 "+iFolioSolicitud+ '-S ' +iEmpleado;

							//PDTE 961: Tabla control de los params de ejecucion.
							cParametrosEnvio = ruta + ' ' + sParametrox;
							guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
							////cParametrosEnvio='';

							ejecutaWebService(sRutaDigiDoctos, sParametrox);
						}
						else
						{
							if (smTipoSolicitud == 2040 || smTipoSolicitud == 2022)  //SI ES EL NUEVO SERVICIO, DE LO CONTRARIO, SI ES UNA MOD. DE DATOS QUE NOOOO MARQUE LA CUENTA.
							{
								grabarLogJs("Entro a marcado de la cuenta, finaliza servicio 2040");
								llamarMarcaEI(iEmpleado, iFolioSolicitud);  //AQUI SE LE MANDA EL:   FolioServicio, EN LA VARIABLE: iFolioSolicitud
							}
							
							/*mensajeFin = 'Se ha concluido con el enrolamiento, de clic en el boton aceptar para finalizar.';
							mensajeFinal(mensajeFin);*/
							cerrarNavegador();
						}
						//verificaconsecutivoeibio(iFolioSolicitud);

						//NOTA: Pend. 301 - SE INABILITA ESTA SECCION (CAMBIOS QUE SE HABIAN METIDO EN EL Pend. 280), DEBIDO A CAMBIOS EN LA NORMATIVA, Fecha del cambio: 06/Nov/2017
						//mensajeFinalPrevio("Promotor. Favor de indicarle al trabajador que debe regresar en "+iDias+" d&iacuteas h&aacutebiles para obtener respuesta del Expediente Biom&eacutetrico y Expediente de Identificaci&oacuten.");
					}
					else
					{
						mensajeFin = 'Ocurrio un error al finalizar el enrolamiento, favor de intentar de nuevo.';
						mensajeFinal(mensajeFin);
					}
				}
				else
				{
					/*mensajeFin = 'Se ha concluido con el enrolamiento, de clic en el boton aceptar para finalizar.';
					mensajeFinal(mensajeFin);*/
					cerrarNavegador();
				}
				$(this).dialog("close");
			}
		},
	});

	if(OSName == "Android"){
		ObtenerExcepcion2(chCurp);
		$("#huellas").dialog
		({
			title: 'Captura de Huellas',
			autoOpen: false,//cambie le false
			resizable: false,
			width: 400, //<-400
			height:350, //<-350
			modal: true,
			docClose: true,
			closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
			open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
			close:'.closeBtn',
			buttons:
			{
				"Aceptar" : function()
				{
					iSelecciono = 0;
					iSelecMano = 0;
					iSelecciono = $('input:radio[name=rbnHuella]:checked').val();
					iSelecMano = iSelecciono;
					//Pasa a variable Global para validar solo la mano derecha o solo mano izquierda.
					iSeleccionoMano = iSelecciono;
					
					if (iSelecciono == 1) //Ambas manos
					{
						mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS = ' + iSelecciono;
						grabarLogJs(mensajeLog); //jcla
						iexcepcion = 0;
						iOpcion = 1;
						reAmbas = 3;
						sParametrox = "2 0 1 3 "+ iFolio;
						//ejecutarApplet(sParametrox,sRutaM); //abrimos la mano derecha ya que selecciono las dos manos
						if(OSName == "Android"){
							ejecutaWebService(1, 1);
						}else{
							ejecutaWebService(sRutaM, sParametrox);
						}
						$(this).dialog("close");
					}
					else if (iSelecciono == 2) //Solo mano Derecha
					{
						//Manda llamar el dialogo correspondiente a la opcion Solo mano Derecha.
						mensajeLog = 'PROMOTOR SELECCIONO SOLO MANO DERECHA = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						reDerecha = 1;
						mostraDialogoHuellasDerecha();
						$(this).dialog("close");
					}
					else if (iSelecciono == 3)//solo mano izquierda
					{
						mensajeLog = 'PROMOTOR SELECCIONO SOLO MANO IZQUIERDA = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						reIzquierda = 2;
						mostraDialogoHuellasIzquierda();
						$(this).dialog("close");
					}

					// else if (iSelecciono == 4)//sin manos
					// {
					//     mostraDialogoSinManos();
					// 	$(this).dialog("close");
					// }

					else if(iSelecciono == 4) //ambas manos amputadas
					{
						mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS AMPUTADAS = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						guardarExcepciones(iFolio,'001',0);
						$(this).dialog("close");
						iexcepcion = 4;

						//ejecutarApplet(sParametrox,sRutaHE);
						if(OSName == "Android"){
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;
						}else{
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;
							ejecutaWebService(sRutaHE, sParametrox);
						}
						/*
						mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
						mensajeHuellaVerificacion(mensajeVerificacion);
						*/
					}
					else if(iSelecciono == 5) //ambas manos lesionadas
					{
						mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS LESIONADAS = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						guardarExcepciones(iFolio,'004',0);
						$(this).dialog("close");
						iexcepcion = 3;

						// ejecutarApplet(sParametrox,sRutaHE);
						if(OSName == "Android"){
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;
						}else{
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;
							ejecutaWebService(sRutaHE, sParametrox);
						}
						/*
						mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
						mensajeHuellaVerificacion(mensajeVerificacion);
						*/
					}
				},
				"Cancelar" : function()
				{
					//aqui llamo la funci�n para cancelar
					mensajeCancelar = 'La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada';
					mostrarMensajeCancelar(mensajeCancelar);
					$(this).dialog("close");
				}
			},
			position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
		});
	}else{
		$("#huellas").dialog
		({
			title: 'Captura de Huellas',
			autoOpen: false,//cambie le false
			resizable: false,
			width: 470, //<-400
			height:420, //<-350
			modal: true,
			docClose: true,
			closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
			open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
			close:'.closeBtn',
			buttons:
			{
				"Aceptar" : function()
				{
					iSelecciono = 0;
					iSelecMano  = 0;
					iSelecciono = $('input:radio[name=rbnHuella]:checked').val();
					iSelecMano  = iSelecciono;
					//Pasa a variable Global para validar solo la mano derecha o solo mano izquierda.
					iSeleccionoMano = iSelecciono;
					if (iSelecciono == 1) //Ambas manos
					{
						mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS = ' + iSelecciono;
						grabarLogJs(mensajeLog); //jcla
						
						iexcepcion = 0;
						iOpcion = 1;
						reAmbas = 3;
						sParametrox = "2 0 1 3 "+ iFolio;

						cParametrosEnvio = sRutaM + ' ' + sParametrox;
						guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
						//cParametrosEnvio='';

						//ejecutarApplet(sParametrox,sRutaM); //abrimos la mano derecha ya que selecciono las dos manos
						if(OSName == "Android"){
							ejecutaWebService(1, 1);
						}else{
							ejecutaWebService(sRutaM, sParametrox);
						}
						$(this).dialog("close");
					}
					else if (iSelecciono == 2) //Solo mano Derecha
					{
						//Manda llamar el dialogo correspondiente a la opcion Solo mano Derecha.
						mensajeLog = 'PROMOTOR SELECCIONO SOLO MANO DERECHA = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						reDerecha = 1;
						mostraDialogoHuellasDerecha();
						$(this).dialog("close");
					}
					else if (iSelecciono == 3)//solo mano izquierda
					{
						mensajeLog = 'PROMOTOR SELECCIONO SOLO MANO IZQUIERDA = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						reIzquierda = 2;
						mostraDialogoHuellasIzquierda();
						$(this).dialog("close");
					}

					// else if (iSelecciono == 4)//sin manos
					// {
					//     mostraDialogoSinManos();
					// 	$(this).dialog("close");
					// }

					else if(iSelecciono == 4) //ambas manos amputadas
					{
						mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS AMPUTADAS = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						guardarExcepciones(iFolio,'001',0);
						$(this).dialog("close");
						iexcepcion = 4;

						//ejecutarApplet(sParametrox,sRutaHE);
						if(OSName == "Android"){
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;
						}else{
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;

							//PDTE 961: Tabla control de los params de ejecucion.
							cParametrosEnvio = sRutaHE + ' ' + sParametrox;
							guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
							////cParametrosEnvio='';

							ejecutaWebService(sRutaHE, sParametrox);
						}
						/*
						mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
						mensajeHuellaVerificacion(mensajeVerificacion);
						*/
					}
					else if(iSelecciono == 5) //ambas manos lesionadas
					{
						mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS LESIONADAS = ' + iSelecciono;
						grabarLogJs(mensajeLog);//JCLA
						guardarExcepciones(iFolio,'004',0);
						$(this).dialog("close");
						iexcepcion = 3;

						// ejecutarApplet(sParametrox,sRutaHE);
						if(OSName == "Android"){
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;
						}else{
							obtenermensajegerente(iexcepcion);
							iOpcion = VALIDACION_HUELLA_GERENTE;
							sParametrox = servidorIP;

							//PDTE 961: Tabla control de los params de ejecucion.
							cParametrosEnvio = sRutaHE + ' ' + sParametrox;
							guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
							////cParametrosEnvio='';

							ejecutaWebService(sRutaHE, sParametrox);
						}
						/*
						mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
						mensajeHuellaVerificacion(mensajeVerificacion);
						*/
					}
				},
				"Cancelar" : function()
				{
					//aqui llamo la funci�n para cancelar
					mensajeCancelar = 'La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada';
					mostrarMensajeCancelar(mensajeCancelar);
					$(this).dialog("close");
				}
			},
			position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
		});
	}

	//Solo Mano Derecha
	$("#soloManoDerecha").dialog
	({
		title: 'Captura de Huellas',
		autoOpen: false,
		resizable: false,
		width: 300,
		height: 250,
		modal: true,
		docClose: true,
		closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
		close:'.closeBtn',
		buttons:
		{
			"Aceptar" : function()
			{
			//	$('#huellas').dialog("close");
				$(this).dialog("close");
				iSelecciono = 0;
				iSelecciono = $('input:radio[name=rbnManoDerecha]:checked').val();
				iSeleccionoMano = iSelecciono;

				//le indicara que se procesara solo mano derecha.
				if (iSelecciono == 006)
				{
					// guardarExcepciones(iFolio,'006',0);
					// iOpcion = 1;
					// sParametrox = "2 0 1 3 "+ iFolio;
					// //ejecutarApplet(sParametrox,sRutaM);
					// ejecutaWebService(sRutaM, sParametrox);

					// peticion 316
					guardarExcepciones(iFolio,'006',0);
					iexcepcion = 1;

					if(OSName == "Android"){
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
						//ejecutaWebService(6, 6);
						//ejecutaWebService(2, 2);
					}else{
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;

						//PDTE 961: Tabla control de los params de ejecucion.
						cParametrosEnvio = sRutaHE + ' ' + sParametrox;
						guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
						////cParametrosEnvio='';

						ejecutaWebService(sRutaHE, sParametrox);
					}

				/*	sParametrox = "2 0 1 3 "+ iFolio;
					ejecutarApplet(sParametrox,sRutaM);
				*/
				}
				else if (iSelecciono == 003)
				{
					// guardarExcepciones(iFolio,'003',0);
					// iOpcion = 1;
					// sParametrox = "2 0 1 3 "+ iFolio;
					// //ejecutarApplet(sParametrox,sRutaM);
					// ejecutaWebService(sRutaM, sParametrox);

					// peticion 316
					guardarExcepciones(iFolio,'003',0);
					iexcepcion = 1;
					
					if(OSName == "Android"){
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
						//ejecutaWebService(2, 2);
					}else{
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;

						//PDTE 961: Tabla control de los params de ejecucion.
						cParametrosEnvio = sRutaHE + ' ' + sParametrox;
						guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
						////cParametrosEnvio='';
						
						ejecutaWebService(sRutaHE, sParametrox);
					}
				/*	sParametrox = "2 0 1 3 "+ iFolio;
					ejecutarApplet(sParametrox,sRutaM);
				*/
				}
				iSelecciono = 2;
			},
			"Cancelar" : function()
			{
					if (smTipoSolicitud == 33 || smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527) {
						localStorage.removeItem("contador");
						window.location.href='http://'+ servidorIP +'/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=' + iEmpleado,true;
					}else{
					mostraDialogoHuellas();
					}
					$(this).dialog("close");
				}
		},
		position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
	});

	//Solo Mano Izquierda
	$("#soloManoIzquierda").dialog
	({
		title: 'Captura de Huellas',
		autoOpen: false,
		resizable: false,
		width: 300,
		height: 250,
		modal: true,
		docClose: true,
		closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
		close:'.closeBtn',
		buttons:
		{
			"Aceptar" : function()
			{
				$(this).dialog("close");
				iSelecciono = 0;
				iSelecciono = $('input:radio[name=rbnManoIzquierda]:checked').val();
				
				if (iSelecciono == 005)
				{
					// guardarExcepciones(iFolio,'005', 0);
					// iOpcion = 2;
					// sParametrox = "2 1 1 3 " + iFolio;
					// ejecutaWebService(sRutaM, sParametrox);

					guardarExcepciones(iFolio,'005', 0);
					iexcepcion = 2;
					
					if(OSName == "Android"){
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
						ejecutaWebService(3, 3);
					}else{
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;

						//PDTE 961: Tabla control de los params de ejecucion.
						cParametrosEnvio = sRutaHE + ' ' + sParametrox;
						guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
						////cParametrosEnvio='';

						ejecutaWebService(sRutaHE, sParametrox);
					}

					// iOpcion = 2;
					// sParametrox = "2 1 1 3 " + iFolio;
					// ejecutarApplet(sParametrox,sRutaM);
				}
				else if (iSelecciono == 002)
				{
					// guardarExcepciones(iFolio,'002', 0);
					// iOpcion = 2;
					// sParametrox = "2 1 1 3 " + iFolio;
					// ejecutaWebService(sRutaM, sParametrox);

					guardarExcepciones(iFolio,'002', 0);
					iexcepcion = 2;
					
					if(OSName == "Android"){
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
						ejecutaWebService(3, 3);
					}else{
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;

						//PDTE 961: Tabla control de los params de ejecucion.
						cParametrosEnvio = sRutaHE + ' ' + sParametrox;
						guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
						////cParametrosEnvio='';

						ejecutaWebService(sRutaHE, sParametrox);
					}

					// iOpcion = 2;
					// sParametrox = "2 1 1 3 " + iFolio;
					// ejecutarApplet(sParametrox,sRutaM);
				}
				iSelecciono = 3;
			},
			"Cancelar" : function()
			 {
					if (smTipoSolicitud == 33 || smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527) {
						localStorage.removeItem("contador");
						window.location.href='http://'+ servidorIP +'/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=' + iEmpleado,true;
					}else{
					mostraDialogoHuellas();
					}
					$(this).dialog("close");
				}
		},
		position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
	});

	//Sin Manos
	$("#sinManos").dialog
	({
		title: 'Captura de Huellas',
		autoOpen: false,
		resizable: false,
		width: 300,
		height: 250,
		modal: true,
		docClose: true,
		closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
		close:'.closeBtn',
		buttons:
		{
			"Aceptar" : function()
			{
				$('#huellas').dialog("close");
				$(this).dialog("close");
				iSelecciono = 0;
				iSelecciono = $('input:radio[name=rbnsinmano]:checked').val();
				//le indicara que se procesara solo mano derecha.
				
				if (iSelecciono == 001)
				{
					guardarExcepciones(iFolio,'001',0);
				}
				else if (iSelecciono == 004)
				{
					guardarExcepciones(iFolio,'004',0);
				}

				//mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
				//mensajeHuellaVerificacion(mensajeVerificacion);
				huellaDeVerificacion();

			},
			"Cancelar" : function()
			{
					if (smTipoSolicitud == 33 || smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527) {
						localStorage.removeItem("contador");
						window.location.href='http://'+ servidorIP +'/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=' + iEmpleado,true;
					}else{
					mostraDialogoHuellas();
					}
					$(this).dialog("close");
				}
		},
		position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
	});
//dialogo Principal Que muestra las huellas faltantes.
	$("#huellasFaltantes").dialog
	({
		title: 'Captura de Huellas',
		autoOpen: false,
		resizable: false,
		//width: 450,
		width: 550,
		height: 400,
		modal: true,
		docClose: true,
		closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
		close:'.closeBtn',
		buttons:
		{
			"Aceptar" : function()
			{
				if (iBandDed1 == 1)
				{
					sCodigo1 =  $('#cboPulDer').val();  //Pulgar  Derecho
				}
				if (iBandDed2 == 1)
				{
					sCodigo2 =  $('#cboIndDer').val();  //Indice  Derecho
				}
				if (iBandDed3 == 1)
				{
					sCodigo3 =  $('#cboMedDer').val();  //Medio  Derecho
				}
				if (iBandDed4 == 1)
				{
					sCodigo4 =  $('#cboAnuDer').val();  //Anular  Derecho
				}
				if (iBandDed5 == 1)
				{
					sCodigo5 =  $('#cboMenDer').val();  //Me�ique  Derecho
				}
				if (iBandDed6 == 1)
				{
					sCodigo6 =  $('#cboPulIzq').val();  //Pulgar  Izquierdo
				}
				if (iBandDed7 == 1)
				{
					sCodigo7 =  $('#cboIndIzq').val();  //Indice  Izquierdo
				}
				if (iBandDed8 == 1)
				{
					sCodigo8 =  $('#cboMedIzq').val();  //Medio  Izquierdo
				}
				if (iBandDed9 == 1)
				{
					sCodigo9 =  $('#cboAnuIzq').val();  //Anular  Izquierdo
				}
				if (iBandDed10 == 1)
				{
					sCodigo10 = $('#cboMenIzq').val();  //Me�ique  Izquierdo
				}
				if (sCodigo1 == '0' || sCodigo2 == '0' || sCodigo3 == '0' || sCodigo4 == '0' || sCodigo5 == '0' || sCodigo6 == '0' || sCodigo7 == '0' || sCodigo8 == '0' || sCodigo9 == '0' || sCodigo10 == '0')
				{
					$(this).dialog("close");
					sMensaje =  "Promotor: Favor de seleccionar una opci\u00F3n valida.";
					mostrarMensajeDialogFaltante(sMensaje);
				}
				else
				{
					//Metodo que almacena las excepciones de los dedos faltantes.
					GuardarDedosFaltantes(OBJ,sCodigo1,sCodigo2,sCodigo3,sCodigo4,sCodigo5,sCodigo6,sCodigo7,sCodigo8,sCodigo9,sCodigo10);
				}
			},
			"Cancelar" : function()
			{
				$(this).dialog("close");
				//Llamar la Funcion que limpia las tabalas para las huellas
				LimpiarHuellas(iFolio);
				mostraDialogoHuellas();
			}
		},
		position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
	});

	//div para el mensaje dirigido al gerente cuando se haya seleccionado una opcion de invalides
	if(OSName == "Android"){
		$('#divMensajeGerente').dialog
		({
			title: 'ATENCION',
			autoOpen: false,
			resizable: false,
			//width: 450,
			width: 550,//1050
			height: 400,//700
			modal: true,
			docClose: true,
			closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
			close:'.closeBtn',
			buttons:
			{
				"Aceptar" : function()
				{
					// SOLO MANO DERECHA
					if(iSelecciono == 2){
						ejecutaWebService(2, 2);
					}
					// SOLO MANO IZQUIERDA
					if(iSelecciono == 3){
						ejecutaWebService(3, 3);
					}
					// MANOS AMPUTADAS
					if(iSelecciono == 4){
						ejecutaWebService(4, 4);
					}
					// MANOS LESIONADAS
					if(iSelecciono == 5){
						ejecutaWebService(5, 5);
					}

					$(this).dialog( "close" );
				},
			},
			position:  {my: "top",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
		});
	}else{
		$('#divMensajeGerente').dialog
		({
			title: 'ATENCION',
			autoOpen: false,
			resizable: false,
			//width: 450,
			width: 550,//1050
			height: 400,//700
			modal: true,
			docClose: true,
			closeOnEscape: true, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); },
			close:'.closeBtn',
			buttons:{},
			position:  {my: "top",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
		});
	}
	
});

function grabarLogJs(sTexto)
{
	var cadena = '';

	cadena += 'opcion=14&stexto='+sTexto;

	$.ajax(
		{
			async: false,
			cache: false,
			data:cadena,
			 url: 'php/capturaenrolamientotrabajador.php',
			 type: 'POST',
			 dataType: 'json',
			 success: function(data)
			 {
				//No se realiza ninguna accion
			},
			error: function(a, b, c){
				//console.log(JSON.stringify(a));
			},
			beforeSend: function(){

			}
		}
	);
}
function mostrarMensajeDialogFaltante(sMensaje)
{
	$('.content-loader').hide();
	$("#divMensaje18").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+sMensaje+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Aqui llamar la funcion del cancelar
				iOpcion = 15;
				obtenerDedosFaltantes();
				$(this).dialog( "close" );
			},
		}
	});
}

function mostrarMensajeCancelar(mensajeCancelar)
{
	$('.content-loader').hide();
$("#divMensaje3").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeCancelar+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Aqui llamar la funcion del cancelar
				cancelarEnrolamiento();
				$(this).dialog( "close" );
			},
		}
	});
}

function mostrarMensajeCancelarFirma(mensajeCancelar)
{
	$('.content-loader').hide();
$("#divMensaje12").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeCancelar+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Aqui llamar la funcion del cancelar
				cancelarEnrolamiento();
				$(this).dialog( "close" );
			},
			"Cancelar" : function()
			{
				iOpcion = 4;
				
				if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
				{
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "_FTRAB";
				}
				else
				{
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "-S_FTRAB";
				}

				ruta =	"C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = ruta + ' ' + sParametros;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(ruta, sParametros);
				$(this).dialog("close");
			}
		}
	});
}

/*
//funcion que permite la ejecucion del applet
function ejecutarApplet(sParametros,sRuta)
{
	document.getElementById('divApplet').innerHTML =
			"<APPLET CODE  = \"appafore.class\" " +
					"ARCHIVE  = \"../applet/appafore.jar\" " +
					"ID = \"appafore\" " +
					"WIDTH    = 1 " +
					"HEIGHT   = 1 > " +
				"<param name=\"opcion\" value=\"3\"> " +
				"<param name=\"comando\" value=\""+ sRuta + "\"> " +
				"<param name=\"argumentos\" value=\"" + sParametros + "\"> " +
			"</APPLET>";

}*/

/*Inicia implementaci�n de WebService
 *----------------------------------------------------------------------------------------
 *Ar�mburo Castro Judith 98104985
 */

//Ejecuta el servicio correspondiente a la sRuta y le envia los sParametros
function ejecutaWebService(sRuta, sParametros)
{
	$('.content-loader').show();
	if(OSName == "Windows"){
		soapData 	= "",
		httpObject 	= null,
		docXml 		= null,
		iEstado 	= 0,
		sMensaje 	= "";
		sUrlSoap 	= "http://127.0.0.1:20044/";

		//Cadena para la ejecuci�n de la aplicaci�n
		soapData=
		'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
				' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"'+
				' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"'+
				' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"'+
				' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"'+
				' xmlns:ns2=\"urn:ServiciosWebx\">'+
				'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">'+
					'<ns2:ejecutarAplicacion>'+
						'<inParam>'+
							'<Esperar>1</Esperar>'+
							'<RutaAplicacion>' + sRuta + '</RutaAplicacion>'+
							'<parametros><![CDATA[' + sParametros + ']]></parametros>'+
						'</inParam>'+
					'</ns2:ejecutarAplicacion>'+
				'</SOAP-ENV:Body>'+
			'</SOAP-ENV:Envelope>';

		//Objeto para la petici�n soap
		httpObject = getHTTPObject();

		if(httpObject)
		{
			if(httpObject.overrideMimeType)
				httpObject.overrideMimeType("false");

			httpObject.open('POST', sUrlSoap, false); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange=function()
			{
				grabarLogJs('------INICIA RESULTADO EJECUTAWEBSERVICE------');
				grabarLogJs('iOpcion: ' + iOpcion);
				grabarLogJs('httpObject.readyState: ' + httpObject.readyState);
				grabarLogJs('httpObject.status: ' + httpObject.status);
				grabarLogJs('------FIN RESULTADO EJECUTAWEBSERVICE------');
				
				if(httpObject.readyState == 4 && httpObject.status == 200)
				{
					parser = new DOMParser();
					docXml = parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;

					//Llamada a la funci�n que recibe la respuesta del WebService
					//En caso de ser necesario agregar un "settime"
					respuestaWebService(iEstado);
				}
				else
				{
					console.log(httpObject.status);
					if (iOpcion == ENROLAMIENTODOCTOS)
					{
						grabarLogJs('verificaconsecutivoeibio-WebService');
						verificaconsecutivoeibio(iFolioSolicitud);
					}
				}
			};
			httpObject.send(soapData);
		}
	}else{
		if(sRuta==1){
			var iOpcEnrol=1; /* Huellas a Capturar - Todas */
			var iOpc=1; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
		if(sRuta==2){
			var iOpcEnrol=2; /* Huellas a Capturar - Mano Derecha*/
			var iOpc=2; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
		if(sRuta==3){
			var iOpcEnrol=3; /* Huellas a Capturar - Mano Izquierda */
			var iOpc=3; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
		if(sRuta==4){
			var iOpcEnrol=4; /* Huellas a Capturar - Manos Amputadas */
			var iOpc=4; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
		if(sRuta==5){
			var iOpcEnrol=4; /* Huellas a Capturar - Manos Lesionadas */
			var iOpc=5; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
		if(sRuta==6){
			var iOpcEnrol=4; /* Huellas a Capturar - Manos Lesionadas */
			var iOpc=6; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
		if(sRuta==7){
			var iOpcEnrol=4; /* Huellas a Capturar - Manos Lesionadas */
			var iOpc=7; /* Retorno codigo de opcion que hace la llamada */
			Android.llamaComponenteHuellas(iOpcEnrol, iFolioSolicitud, 0, iOpc);
		}
	}
}

//M�todo para crear el objeto para la petici�n xml
function getHTTPObject()
{
    var xhr = false;
    if (window.ActiveXObject) {
        try {
            xhr = new ActiveXObject("Msxml2.XMLHTTP");
        } catch(e) {
            try {
                xhr = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e) {
                xhr = false;
            }
        }
    } else if (window.XMLHttpRequest) {
        try {
            xhr = new XMLHttpRequest();
        } catch(e) {
            xhr = false;
        }
    }
    return xhr;
}

/*Termina implementaci�n de WebService
-----------------------------------------------------------------------------------
 */


//funcion que permite obtener la respuesta que arrojo el applet
//function respuestaApplet(iRespuesta)
function respuestaWebService(iRespuesta)
{
	//PDTE 961: Actualiza la respuesta del consumo del ejecutable.
	if(OSName == 'Windows'){
		guardarParametrosControl(cParametrosEnvio, iFolio, iRespuesta, 2);
		//cParametrosEnvio = '';
	}
	

	$('.content-loader').hide();
	var sMensaje = "";
	var cDatoAux = '';
	var sMensaje = '[capturaenrolamientotrabajador::respuestaApplet] --> INICIO DE: RESPUESTA DEL APPLET: ' + iRespuesta + ', Valor Selecciono: ' + iSelecciono + ' Valor Opcion: ' + iOpcion;
	grabarLogJs(sMensaje);

	if (iOpcion != ENROLAMIENTODOCTOS) {
		iEmpleado = iEmpleado;
		if (iRespuesta == 99 ||iRespuesta == 31 ) {

			if(iRespuesta == 99 || iRespuesta == 31 && smTipoSolicitud == 33 || smTipoSolicitud == 26 ||smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527){
				iBanderaSalir = true;
				//cancelarEnrolamientoCte();
				$('.content-loader').show();
				window.location.href='http://'+ servidorIP +'/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=' + iEmpleado,true;

			}
			if (iSelecciono == 1) {
				$('#huellas').dialog("close");
				mostraDialogoHuellas();
			}
			else {
				//borrar el enrolexcepcionestrabajador
				borrarExcepciones(iFolio);
				LimpiarHuellas(iFolio);
			}
		}
	}
	
	 switch(iOpcion)
	 {
	 	case VERIFICACION_MORPHO:
		   if (iRespuesta == 1)
		   {
			//	desbloquearUI();
			//	msj = 'Se  proseguir\u00E1  con  la  captura  de  las  huellas dactilares';
			//	mostrarMensajePrincipal(msj);
		   }
		   else if (iRespuesta == 2)
		   {
			 //BLOQUEAR
		   }
	   break;
	   //Flujo para el llamado de las huellas y Guardado
	   case MANO_DERECHA: // 1
			//levantamos la Mano izquierda
			mensajeLog = 'Respuesta Morpho [MANO_DERECHA] = ' + iRespuesta + ' ' + 'Opcion seleccionada = ' + iSelecciono;
			grabarLogJs(mensajeLog);//jcla
			if (iRespuesta == 1)
			{
				if (iSelecciono == 1) //ambas manos
				{
					iOpcion = 5; //llamar pulgares
					sParametrox = "";
					sParametrox = "2 1 1 3 "+ iFolio; //llamamos izquierda

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaM, sParametrox);
				}
				else
				{ //solo mano derecha
					iOpcion = 6; //llamar guardar
					sParametrox = "";
					sParametrox = "2 2 1 3 "+ iFolio;//llamamos los pulgares
					
					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaM, sParametrox);
				}
			}
			else if (iRespuesta == 2 || iRespuesta == 3 || iRespuesta == 4 || iRespuesta == 5 || iRespuesta == 6 || iRespuesta == 7 || iRespuesta == 8 || iRespuesta == 9 || iRespuesta == 10  || iRespuesta == 11 || iRespuesta == 12 || iRespuesta == 13 || iRespuesta == 14 || iRespuesta == 15 || iRespuesta == 16 || iRespuesta == 17 || iRespuesta == 18 || iRespuesta == 20 || iRespuesta == 21 || iRespuesta == 22 || iRespuesta == 24 || iRespuesta == 25)
			{
				//validar la calidad del NIST
				contIntentos = contIntentos + 1;
				
				if (contIntentos == 3)
				{
					mensajeSinHuellas = 'Trabajador se enrolo sin huellas dactilares';
					mensajeEnroladoSinHuellas(mensajeSinHuellas);//obtenerDedosFaltantes();//vemos que dedos faltan para mostrar el Dialog
					contIntentos = 0;
				}
				else
				{
					//varia = validarNISTMorpho(iFolio);
					varia = validarNIST(iFolio);
					sMensaje = '[capturaenrolamientotrabajador::respuestaApplet] --> Respuesta: ' + varia;
	 				grabarLogJs(sMensaje);
				}
			}
			else if (iRespuesta == -1 && iSelecciono == 3) //paara pedir mano izquierda de nuevo
			{
				//Reintento de la captura de mano izquierda, ya que el usuario no coloco las huella en el dispositivo
				iOpcion = 5; //llamar pulgares
				sParametrox = "";
				sParametrox = "2 1 1 3 "+ iFolio; //llamamos izquierda

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (iRespuesta == -1 && iSelecciono == 2) //paara pedir mano derecha de nuevo
			{
				//Reintento de la captura de mano izquierda, ya que el usuario no coloco las huella en el dispositivo
				iOpcion = 5; //llamar pulgares
				sParametrox = "";
				sParametrox = "2 0 1 3 "+ iFolio; //llamamos izquierda
				
				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}
	   break;
	    case MANO_IZQUIERDA: // 2
			//levantamos la Mano izquierda
			mensajeLog = 'Respuesta Morpho [MANO_IZQUIERDA] = ' + iRespuesta + ' ' + 'Opcion seleccionada = ' + iSelecciono;
			grabarLogJs(mensajeLog);
			if (iRespuesta == 1)
			{
				iOpcion = 6; //llamar guardar
				sParametrox = "";
				sParametrox = "2 2 1 3 "+ iFolio;//llamamos los pulgares
				//ejecutarApplet(sParametrox,sRutaM);
				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (iRespuesta == 2 || iRespuesta == 3 || iRespuesta == 4 || iRespuesta == 5 || iRespuesta == 6 || iRespuesta == 7 || iRespuesta == 8 || iRespuesta == 9 || iRespuesta == 10  || iRespuesta == 11 || iRespuesta == 12 || iRespuesta == 13 || iRespuesta == 14 || iRespuesta == 15 || iRespuesta == 16 || iRespuesta == 17 || iRespuesta == 18 || iRespuesta == 20 || iRespuesta == 21 || iRespuesta == 22 || iRespuesta == 24 || iRespuesta == 25)
			{
				//validar la calidad del NIST
				contIntentos = contIntentos + 1;
				if (contIntentos == 3)
				{
					mensajeSinHuellas = 'Trabajador se enrolo sin huellas dactilares';
					mensajeEnroladoSinHuellas(mensajeSinHuellas);//obtenerDedosFaltantes();//vemos que dedos faltan para mostrar el Dialog
					contIntentos = 0;

				}
				else
				{
					//varia = validarNISTMorpho(iFolio);
					varia = validarNIST(iFolio);
					sMensaje = '[capturaenrolamientotrabajador::respuestaApplet] --> Respuesta: ' + varia;
	 				grabarLogJs(sMensaje);
				}
			}
			else if (iRespuesta == -1 && iSelecciono == 3) //paara pedir mano izquierda de nuevo
			{
				//Reintento de la captura de mano izquierda, ya que el usuario no coloco las huella en el dispositivo
				iOpcion = 5; //llamar pulgares
				sParametrox = "";
				sParametrox = "2 1 1 3 "+ iFolio; //llamamos izquierda
				//ejecutarApplet(sParametrox,sRutaM);
				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (iRespuesta == -1 && iSelecciono == 2) //paara pedir mano derecha de nuevo
			{
				//Reintento de la captura de mano izquierda, ya que el usuario no coloco las huella en el dispositivo
				iOpcion = 5; //llamar pulgares
				sParametrox = "";
				sParametrox = "2 0 1 3 "+ iFolio; //llamamos izquierda
				//ejecutarApplet(sParametrox,sRutaM);
				ejecutaWebService(sRutaM, sParametrox);
			}
	   	break;
	    case PULGARES: // 5
			//levantamos los pulgares
			mensajeLog = 'Respuesta Morpho [PULGARES] = ' + iRespuesta + ' ' + 'Opcion seleccionada = ' + iSelecciono;
			grabarLogJs(mensajeLog);//jcla
			
			if (iRespuesta == 1)
			{
				iOpcion = 6; //llamar guardar
				sParametrox = "";
				sParametrox = "2 2 1 3 "+ iFolio;//llamamos los pulgares

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (iRespuesta == -1 && iSelecciono == 3) //paara pedir mano izquierda de nuevo
			{
				//Reintento de la captura de mano izquierda, ya que el usuario no coloco las huella en el dispositivo
				iOpcion = 5; //llamar pulgares
				sParametrox = "";
				sParametrox = "2 1 1 3 "+ iFolio; //llamamos izquierda

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (iRespuesta == -1 && iSelecciono == 2) //paara pedir mano derecha de nuevo
			{
				//Reintento de la captura de mano derecha, ya que el usuario no coloco las huella en el dispositivo
				iOpcion = 5; //llamar pulgares
				sParametrox = "";
				sParametrox = "2 0 1 3 "+ iFolio; //llamamos derecha

				
				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}

	   break;
	   case GUARDAR: //6
			//llamamos guardar en la base de datos
			mensajeLog = 'Respuesta del metodo [GUARDAR] = ' + iRespuesta;
			grabarLogJs(mensajeLog);//jcla
			
			if (iRespuesta == 1)
			{
				iNumCapturaHuella++;//Sumamos una captura completa de huellas
				sTexto = '[capturaenrolamientotrabajador::respuestaApplet] --> ENTRO GUARDAR Numero de captura de huellas: ' + iNumCapturaHuella + ';';
				grabarLogJs(sTexto);
				iOpcion = 11; //Llamar Guardar
				sParametrox = "";
				sParametrox = "2 0 2 0 "+ iFolio;

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';


				//SE GUARDA EL ENROLAMIENTO
				//ejecutarApplet(sParametrox,sRutaM);
				ejecutaWebService(sRutaM, sParametrox);

				// SE EJECUTA METODO PARA OBTENER DATOS PARAMETRIZABLES...
				ObtenerDatosConfiguracion();
			}
			else if (iRespuesta == 2 || iRespuesta == 3 || iRespuesta == 4 || iRespuesta == 5 || iRespuesta == 6 || iRespuesta == 7 || iRespuesta == 8 || iRespuesta == 9 || iRespuesta == 10  || iRespuesta == 11 || iRespuesta == 12 || iRespuesta == 13 || iRespuesta == 14 || iRespuesta == 15 || iRespuesta == 16 || iRespuesta == 18 || iRespuesta == 20 || iRespuesta == 21 || iRespuesta == 22 || iRespuesta == 24 || iRespuesta == 25)
			{
				//validar la calidad del NIST
				contIntentos = contIntentos + 1;
				
				if (contIntentos == 3)
				{
					mensajeSinHuellas = 'Trabajador se enrolo sin huellas dactilares';
					mensajeEnroladoSinHuellas(mensajeSinHuellas);//obtenerDedosFaltantes();//vemos que dedos faltan para mostrar el Dialog
					contIntentos = 0;
				}
				else
				{
					//varia = validarNISTMorpho(iFolio);
					varia = validarNIST(iFolio);
				}
			}
			else if (iRespuesta == 19)
            {
            	contIntentos = contIntentos + 1;
            	
                  if (contIntentos == 3)
                      {
            		    //aqui llamo la funci�n para cancelar ya que supero el numero de intentos.
						borrarExcepciones(iFolio);
            	        LimpiarHuellas(iFolio);
            			contIntentos = 0;
						mensajeCancelar = 'Promotor: Excediste el numero de intentos,La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada.';
						mostrarMensajeCancelar(mensajeCancelar);
            		    $(this).dialog("close");
                      }
            	else
            	      {
            	       //Se presento un problema con el WS [19] se perdio la conexion y no guardo el templates
            	       //Pedir de nuevo
            	          sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo(WS 19).';
            	          grabarLogJs(sMensajeAux);
            	          borrarExcepciones(iFolio);
            	          LimpiarHuellas(iFolio);
            	          mensajeNuevoEnrol = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo (WS 19).';
            	          mensajeNuevoEnrolamiento(mensajeNuevoEnrol);
            	      }
            }
		    else if (iRespuesta == 26)
		    {
		       contIntentos = contIntentos + 1;
                  if (contIntentos == 3)
                      {
            		    //aqui llamo la funci�n para cancelar ya que supero el numero de intentos.
						borrarExcepciones(iFolio);
            	        LimpiarHuellas(iFolio);
            			contIntentos = 0;
		    	        mensajeFinPromotor = 'Promotor: Excediste el numero de intentos,La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada.';
		    	        mensajeHuellaPromotor(mensajeFinPromotor)
            		    $(this).dialog("close");
                      }
            	else
            	      {
            	       //Se presento un problema con el guardado una de las huellas del trabajador coincide con la del promotor
            	       //Pedir de nuevo
            	       sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo(WS 26).';
            	       grabarLogJs(sMensajeAux);
            	       borrarExcepciones(iFolio);
            	       LimpiarHuellas(iFolio);
		               mensajeNuevoEnrol = 'Una de las huellas del trabajador coincide con la del promotor,Favor de intentar de nuevo (WS 26).';
			           mensajeNuevoEnrolamiento(mensajeNuevoEnrol);
            	      }
		    }
			else //if (iRespuesta == -1)
			{
				//contador pulgar
				iContPulgar++;

				if (iContPulgar == 3)
				{
					iContPulgar=0;
					iOpcion = 11; //llamar guardar
					sParametrox = "";
					sParametrox = "2 0 2 0 "+ iFolio;

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					//SE GUARDA EL ENROLAMIENTO
					//ejecutarApplet(sParametrox,sRutaM);
					ejecutaWebService(sRutaM, sParametrox);

					// SE EJECUTA METODO PARA OBTENER DATOS PARAMETRIZABLES...
					ObtenerDatosConfiguracion();
				}
				else
				{
					iOpcion = 6; //llamar guardar
					sParametrox = "";
					sParametrox = "2 2 1 3 "+ iFolio;//llamamos los pulgares

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaM, sParametrox);
				}
			}
	   break;
	   case FALTAN_DEDOS:
	
	   if (iRespuesta == 1)
		{
			//llamar Funci�n para verificar los dedos faltantes
			//parametros de esta funcion son Folio Enrolamiento y Opcion de la Mano seleccionada
			iOpcion = 15;
			obtenerDedosFaltantes();
		}
		else if (iRespuesta == 2 || iRespuesta == 3 || iRespuesta == 4 || iRespuesta == 5 || iRespuesta == 6 || iRespuesta == 7 || iRespuesta == 8 || iRespuesta == 9 || iRespuesta == 10  || iRespuesta == 11 || iRespuesta == 12 || iRespuesta == 13 || iRespuesta == 14 || iRespuesta == 15 || iRespuesta == 16 || iRespuesta == 18 || iRespuesta == 20 || iRespuesta == 21 || iRespuesta == 22 || iRespuesta == 24 || iRespuesta == 25)
		{
			//validar la calidad del NIST
			contIntentos++;
			
			if (contIntentos == 3)
			{
				mensajeSinHuellas = 'Trabajador se enrolo sin huellas dactilares';
				mensajeEnroladoSinHuellas(mensajeSinHuellas);//obtenerDedosFaltantes();//vemos que dedos faltan para mostrar el Dialog
				contIntentos = 0;
			}
			else
			{
				//varia = validarNISTMorpho(iFolio);
				varia = validarNIST(iFolio);
				sMensaje = '[capturaenrolamientotrabajador::respuestaApplet] --> Respuesta: ' + varia;
				grabarLogJs(sMensaje);
			}
	   	}
		else if (iRespuesta == 19)
        {
			contIntentos++;
        	
              if (contIntentos == 3)
                  {
        		    //aqui llamo la funci�n para cancelar ya que supero el numero de intentos.
		            borrarExcepciones(iFolio);
        	        LimpiarHuellas(iFolio);
        			contIntentos = 0;
		            mensajeCancelar = 'Promotor: Excediste el numero de intentos,La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada.';
		            mostrarMensajeCancelar(mensajeCancelar);
        		    $(this).dialog("close");
                  }
        	else
        	      {
        	       //Se presento un problema con el WS [19] se perdio la conexion y no guardo el templates
        	       //Pedir de nuevo
        	          sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo(WS 19).';
        	          grabarLogJs(sMensajeAux);
        	          borrarExcepciones(iFolio);
        	          LimpiarHuellas(iFolio);
        	          mensajeNuevoEnrol = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo (WS 19).';
        	          mensajeNuevoEnrolamiento(mensajeNuevoEnrol);
        	      }
        }
		else if (iRespuesta == 26)
		{
			contIntentos++;
		   
              if (contIntentos == 3)
                  {
        		    //aqui llamo la funci�n para cancelar ya que supero el numero de intentos.
		            borrarExcepciones(iFolio);
        	        LimpiarHuellas(iFolio);
        			contIntentos = 0;
			        mensajeFinPromotor = 'Promotor: Excediste el numero de intentos,La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada.';
			        mensajeHuellaPromotor(mensajeFinPromotor)
        		    $(this).dialog("close");
                  }
        	else
        	      {
        	       //Se presento un problema con el guardado una de las huellas del trabajador coincide con la del promotor
        	       //Pedir de nuevo
        	       sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo(WS 26).';
        	       grabarLogJs(sMensajeAux);
        	       borrarExcepciones(iFolio);
        	       LimpiarHuellas(iFolio);
		           mensajeNuevoEnrol = 'Una de las huellas del trabajador coincide con la del promotor,Favor de intentar de nuevo (WS 26).';
			       mensajeNuevoEnrolamiento(mensajeNuevoEnrol);
        	      }
		}
		else //(iRespuesta == -1 && iRespuesta == 17)
		{
				contIntentos++;

				if (contIntentos >= 3)
				{
					sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Se present\u00F3 un problema al guardar las huellas del Enrolamiento, Favor de intentar de nuevo(WS 26).';
					grabarLogJs(sMensajeAux);
					LimpiarHuellas(iFolio);
					mensajeNuevoEnrol = 'Promotor: Se present\u00F3 un problema al guardar las huellas del Enrolamiento, Favor de intentar de nuevo(WS ' + iRespuesta + ')';
					mensajeNuevoEnrolamiento(mensajeNuevoEnrol);
					contIntentos = 0
					contIntentosEnrol++;
				}
				else
				{					
					//iContPulgar = 0;
					iOpcion = 11; //llamar guardar
					sParametrox = "";
					sParametrox = "2 0 2 0 "+ iFolio;

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					//SE GUARDA EL ENROLAMIENTO
					//ejecutarApplet(sParametrox,sRutaM);
					ejecutaWebService(sRutaM, sParametrox);

					// SE EJECUTA METODO PARA OBTENER DATOS PARAMETRIZABLES...
					ObtenerDatosConfiguracion();
				}
		}		
		break;
		case VALIDAR_NIST:
			//llamar el metodo para validar nist
			iOpcion = 15;
			nist = validarNIST(iFolio);
			sMensaje = '[capturaenrolamientotrabajador::respuestaApplet] --> Respuesta: ' + nist;
	 		grabarLogJs(sMensaje);
		break;
		case VERIFICACION_EMPLEADO:
			if (iRespuesta == iEmpleado)
				{
					mostraDialogoHuellas();
				}
			else
				{
					mensaje = 'La huella capturada no coincide con el empleado. Favor de revisar';
					mensajeHuella(mensaje)
				}
		break;
		case VERIFICACION_EMPLEADO_HE: // se anexa caso solicitado enla incidencia 603
			grabarLogJs("validar que sea el mismo Promotor, para el mismo Trabajador" + "Opcion: " + iOpcion + " Promotor Autoriza: " + iRespuesta);
			
			if (iRespuesta == numeroEmpleadoAutoriza) 
			{
				if (iExcepcionesRet != 0) // si la exepcion es diferente de 0 ocupa la autorizacion del gerente
				{
					obtenermensajegerente(iExcepcionesRet);
					iOpcion = VALIDACION_HUELLA_GERENTE;
					sParametrox = servidorIP;
					iexcepcion = iExcepcionesRet;

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaHE + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaHE, sParametrox);
				}
				else 
				{
					//enrolamiento normal
					msj = 'El folio solicitud: -> ' + iFolioSolicitud + ' cuenta con un proceso de enrolamiento terminado ';
					grabarLogJs(msj);
					iexcepcion = iExcepcionesRet;
					verificacionEnrolAutoriza(iFolio);
				}
			} 
			else 
			{
				mensaje = 'La huella capturada no coincide con el empleado. Favor de revisar';
				mensajeHuella(mensaje)
			}
			break;
		case CAPTURA_FIRMA: //4
			if (iRespuesta == 1) //Esta conectada la singpad
			{
				sMensajeAux = 'singpad conectada[CAPTURA_FIRMA]: ' + iRespuesta +', :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				iRespuesta = 0;
				optenerLigaEnrolamiento();
			}
			else if (iRespuesta == -6) //No esta conectada singpad
			{
				//llamar de nuevo a la singpad
				sMensajeAux = 'singpad desconectada y se mostro mensaje a promotor[CAPTURA_FIRMA]: ' + iRespuesta +', :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajefirmaF =  'Promotor: Verifique que la SINGPAD este conectada';
				iRespuesta = 0;
				mostrarMensajeFirmaConectar(mensajefirmaF);
			}
			else if (iRespuesta == -1) //No esta conectada singpad
			{
				//Metodo de Cancelar Enrolamieto
				//aqui llamo la funci�n para cancelar
				sMensajeAux = 'singpad desconectada y se cancelo la firma(se borro la informacion)[CAPTURA_FIRMA]' + iRespuesta +', :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajeCancelarFirma = 'La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada';
				mostrarMensajeCancelarFirma(mensajeCancelarFirma);
			}
			else
			{
				sMensajeAux = 'ocurrio un error en la singpad ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
			}
		break;
		case VERIFICACION:

			mensajeLog = 'Valor de la respuesta para la verificacion del promotor igual a :' + iRespuesta;
			grabarLogJs(mensajeLog);//jcla
			
			if (iRespuesta == 1)
			{
				//validar que se hayan guardado los campos correspondientes con el folio de enrolamiento en la tabla enroltrabajadorautoriza
				verificacionEnrolAutoriza(iFolio);
			}
			else if (iRespuesta == 99)
			{
				//borrar el enrolexcepcionestrabajador
				borrarExcepciones(iFolio);
				LimpiarHuellas(iFolio);
			}
			else
			{
				//reintentar con la huella solo dos veces mas
				numeroIntentos++;
				
				if (numeroIntentos >= 2 )
				{
					//Aqui va el mensaje de empezar de nuevo con el enrolamiento
					mensajeLog = 'Se volveran a pedir las huellas del trabajador :' + numeroIntentos + 'Respuesta: ' + iRespuesta ;
					grabarLogJs(mensajeLog);//jcla
					numeroIntentos = 0;
					mensajeNuevoEnrol = 'Promotor: se iniciar\u00E1 el tr\u00E1mite de enrolamiento nuevamente ya que hubo un problema con la captura de tu huella.';
					mensajeNuevoEnrolamiento(mensajeNuevoEnrol);
				}
				else
				{
					mensajeLog = 'Promotor intentara poner su huella nuevamente, numero de intento :' + numeroIntentos;
					grabarLogJs(mensajeLog);//jcla
					mensajeVerificacion = 'Promotor: coloca tu huella nuevamente.';
					mensajeReintentoVerificacion(mensajeVerificacion);
				}
			}
		break;
		case ENROLAMIENTODOCTOS:
		    if (iRespuesta == 1)
		    {
		    	llamarMarcaEI(iEmpleado, iFolioSolicitud);  //AQUI SE LE MANDA EL:   FolioServicio, EN LA VARIABLE: iFolioSolicitud
		    	//mensajeExitoFinalPrevio("Promotor. Favor de indicarle al trabajador que debe regresar en "+iDias+" d&iacuteas h&aacutebiles para obtener respuesta del Expediente Biom&eacutetrico y/o Expediente de Identificaci&oacuten.");
		    	//$("#contenedorPrincipal").hide();
				setTimeout(function (){
					cMensajeLog = '[solicitarServiciosAfore::ValidarExpedientes] --> [Filtro: x] PROMOTOR: Favor de indicarle al trabajador que debe regresar en '+iDias+' d&iacuteas h&aacutebiles para obtener respuesta del Expediente Biom&eacutetrico y Expediente de Identificaci&oacuten.';
					grabarLogJs(cMensajeLog);
					mensajeFin = 'Promotor. Favor de indicarle al trabajador que debe regresar en '+iDias+' d&iacuteas h&aacutebiles para obtener respuesta del Expediente Biom&eacutetrico y Expediente de Identificaci&oacuten.';
					mensajeFinal(mensajeFin);
				},6000);
		    }
		    else
		    {
		    	mensajeFinal("El Enrolamiento no se digitaliz&oacute; correctamente, por favor, reporte a Mesa de Ayuda.");
		    }
		break;

		//folio 316
		case VALIDACION_HUELLA_GERENTE:
			intentosSensor++;
			
			if( iRespuesta == 0 )
			{
				mostrarMensajeCancelarHuellaGerente('¿Cancelar validaci&oacuten de Huella del Gerente?');
			}
			else
			{
				iNumEmpSensor = iRespuesta;
				if( iNumEmpSensor.length >= 8 )
				{
					fnvalidarnumempleado(iNumEmpSensor);
				}
				else
				{
					if( intentosSensor == 3 )
					{
						intentosSensor = 0;
						$('#divMensajeGerente').dialog('close');
						mostrarMensajeErrorHuella('La huella no coincide con la registrada. Favor de pedir autorizaci&oacuten a otro Gerente Titular.');
					}
					else
					{
						iOpcion = VALIDACION_HUELLA_GERENTE;
						// ejecutarApplet(servidorIP, sRutaHE);
						ejecutaWebService(sRutaHE, servidorIP);
					}
				}
			}
		break;
		case TOMAR_FOTO_CLIENTE:
			console.log('ENTRO EN LA RESPUESTA DE LA CAPTURA DE LA CAMARA ->' + iRespuesta);
			if( iRespuesta == 1 )
			{
				switch (iexcepcion || iExcepcionesRet) {
					case 1: //solo mano derecha
						iOpcion = 1;
						sParametrox = "2 0 1 3 "+ iFolio;
						// ejecutarApplet(sParametrox,sRutaM);
						ejecutaWebService(sRutaM, sParametrox);
					break;
					case 2: //solo mano izquierda
						iOpcion = 2;
						sParametrox = "2 1 1 3 " + iFolio;
						// ejecutarApplet(sParametrox,sRutaM);
						ejecutaWebService(sRutaM, sParametrox);
					break;
					case 3: //ambas manos lesionadas
					case 4: //ambas manos amputadas
						//mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
						//mensajeHuellaVerificacion(mensajeVerificacion);
						huellaDeVerificacion();
					break;
					default:
					break;
				}
			}
			else if( iRespuesta == 0 )
			{
				sMensaje = '&iquest;Estas seguro que deseas salir?';
				mostrarMensajeSalirCamara(sMensaje);
			}
		break;

		case CAPTURAR_FIRMA_GERENTE:
			console.log('ENTRO EN LA RESPUESTA DE LA CAPTURA DE LA FIRMA DEL GERENTE ->' + iRespuesta);
			if (iRespuesta == 1) //Esta conectada la singpad
			{
				sMensajeAux = 'singpad conectada[CAPTURAR_FIRMA_GERENTE]: ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				iSelecciono = 4;
				mensajefirmaS = 'Promotor: Favor de indicarle al trabajador que firmar\u00E1  el  Formato  de  Enrolamiento  autorizando  los datos  biom\u00E9tricos  los cuales  se  limitar\u00E1  a  la  ejecuci\u00F3n  de  las  acciones  y procesos  establecidos  en t\u00E9rminos de la Ley de los Sistemas de Ahorro para el Retiro';
				mostrarSolicitarFirma(mensajefirmaS);
			}
			else if (iRespuesta == -6) //No esta conectada singpad
			{
				//llamar de nuevo a la singpad
				sMensajeAux = 'singpad desconectada y se mostro mensaje a promotor[CAPTURAR_FIRMA_GERENTE]: ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajefirmaF =  'Promotor: Verifique que la SINGPAD este conectada';
				iRespuesta = 0;
				mostrarMensajeFirmaConectar(mensajefirmaF);
			}
			else if (iRespuesta == -1) //No esta conectada singpad
			{
				//Metodo de Cancelar Enrolamieto
				//aqui llamo la funcion para cancelar
				sMensajeAux = 'singpad desconectada y se cancelo la firma(se borro la informacion)[CAPTURAR_FIRMA_GERENTE]' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajeCancelarFirma = 'La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada';
				mostrarMensajeCancelarFirma(mensajeCancelarFirma);
			}
			else
			{
				sMensajeAux = 'ocurrio un error en la singpad ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
			}
		break;

		case CAPTURA_LEYENDA_EXCEPCION:
			console.log('PRMOTOR CAPTURO LEYENDA');
			if (iRespuesta == 1) //Esta conectada la singpad
			{
				sMensajeAux = 'singpad conectada [CAPTURA_LEYENDA_EXCEPCION]: ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				iSelecciono = 4;
				obtenermensajegerente(8);
			}
			else if (iRespuesta == -6) //No esta conectada singpad
			{
				//llamar de nuevo a la singpad
				sMensajeAux = 'singpad desconectada y se mostro mensaje a promotor[CAPTURA_LEYENDA_EXCEPCION]: ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajefirmaF =  'Promotor: Verifique que la SINGPAD este conectada';
				iRespuesta = 0;
				mostrarMensajeFirmaConectar(mensajefirmaF);
			}
			else if (iRespuesta == -1) //No esta conectada singpad
			{
				//Metodo de Cancelar Enrolamieto
				//aqui llamo la funcion para cancelar
				sMensajeAux = 'singpad desconectada y se cancelo la firma(se borro la informacion)[CAPTURA_LEYENDA_EXCEPCION]' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajeCancelarFirma = 'La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada';
				mostrarMensajeCancelarFirma(mensajeCancelarFirma);
			}
			else
			{
				sMensajeAux = 'ocurrio un error en la singpad ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
			}
		break;

		case FIRMA_PROMOTOR_EXCEPCION:
			console.log('PROMOTOR FIRMA POR EXCEPCION');

			if (iRespuesta == 1) //Esta conectada la singpad
			{
				sMensajeAux = 'singpad conectada [FIRMA_PROMOTOR_EXCEPCION]: ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				optenerLigaEnrolamiento();
			}
			else if (iRespuesta == -6) //No esta conectada singpad
			{
				//llamar de nuevo a la singpad
				sMensajeAux = 'singpad desconectada y se mostro mensaje a promotor[FIRMA_PROMOTOR_EXCEPCION]: ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajefirmaF =  'Promotor: Verifique que la SINGPAD este conectada';
				iRespuesta = 0;
				mostrarMensajeFirmaConectar(mensajefirmaF);
			}
			else if (iRespuesta == -1) //No esta conectada singpad
			{
				//Metodo de Cancelar Enrolamieto
				//aqui llamo la funcion para cancelar
				sMensajeAux = 'singpad desconectada y se cancelo la firma(se borro la informacion)[FIRMA_PROMOTOR_EXCEPCION]' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
				mensajeCancelarFirma = 'La informaci\u00F3n capturada hasta el momento ser\u00E1 borrada';
				mostrarMensajeCancelarFirma(mensajeCancelarFirma);
			}
			else
			{
				sMensajeAux = 'ocurrio un error en la singpad ' + iRespuesta +' :Parametros obtenidos: '+ sParametros;
				grabarLogJs(sMensajeAux);//jcla
			}
		break;
	}
}

function mensajeNuevoEnrolamiento(mensajeNuevoEnrol)
{
  $('.content-loader').hide();
  $("#divMensaje8").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeNuevoEnrol+"</p>");
		},
		buttons:
			{
				"Aceptar" : function()
				{
					//de nuevo a enrolar
					numeroIntentos = 0;
					limpiar();
					if (smTipoSolicitud == 33 || smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527) {
						obtener_localstoraje();
					}else{
					mostraDialogoHuellas();
					$(this).dialog("close");
				}
					
				},
			}
	});
}

function obtener_localstoraje(){
	let contador = localStorage.getItem("contador");
	sMensajeAux = 'en el localstorage';
	grabarLogJs(sMensajeAux);
    if(contador != 2){
        contador++;
        localStorage.setItem("contador", contador);
        window.location.reload(true);
    }else if(contador == 2){
		sMensajeAux = 'en el localstorage redireccionar';
		grabarLogJs(sMensajeAux);
        localStorage.removeItem("contador");
        window.location.href='http://'+ servidorIP +'/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=' + iEmpleado,true;    
    }
}

function mensajeReintentoVerificacion(mensajeVerificacion)
{
  $('.content-loader').hide();
  $("#divMensaje7").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeVerificacion+"</p>");
		},
		buttons:
			{
				"Aceptar" : function()
				{
					//Verificar Huella del Empleado
					iOpcion = 13;
					sParametrox = "2 -1 1 11 "+ iFolio; //llamamos la verificacion

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaM, sParametrox);
				  $( this ).dialog("close");
				},
			}
	});
}

function borrarExcepciones(iFolioGenerado)
{
	var iRetBorro = 0;
    var cadena = '';
	var sNumEnrolador = iEmpleado;

	cadena = "opcion=10&folio=" + iFolioGenerado;
	$.ajax(
		{
			async: false,
			cache: false,
			data:cadena,
			 url: 'php/capturaenrolamientotrabajador.php',
			 type: 'POST',
			 dataType: 'json',
			 success: function(data)
			{
			 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				//desbloquearUI();
				iRetBorro = datos.borro;

				if (iRetBorro == 1)
				{
					mostraDialogoHuellas();
				}
			},
			error: function(a, b, c)
			{
				//desbloquearUI();
			},
			beforeSend: function()
			{
				//bloquearUI("Espere un momento por favor...");
			}
		}
	);
	return iRetBorro;
}

function mensajeCalidadLlamar(mensajeCalidad)
{
	$('.content-loader').hide();
  $("#divMensaje10").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeCalidad+"</p>");
		},
		buttons:
			{
				"Aceptar" : function()
				{
				//	window.parent.close();
				if (reIzquierda == 2)
				{
					//llamar mano izquierda y maracar para que entren los pulgares
					iOpcion = 5;
					sParametrox = "2 1 1 3 "+ iFolio;
					//ejecutarApplet(sParametrox,sRutaM); //abrimos la mano derecha ya que selecciono las dos manos
					ejecutaWebService(sRutaM, sParametrox);
				}
				else if (reDerecha == 1)
				{
					//llamar mano Derecha y marco para que entre en el pulgar
					iOpcion = 5;
					sParametrox = "2 0 1 3 "+ iFolio;
					//ejecutarApplet(sParametrox,sRutaM); //abrimos la mano derecha ya que selecciono las dos manos
					ejecutaWebService(sRutaM, sParametrox);
				}
				else if (reAmbas == 3)
				{
					//llamar Ambas Manos aqui levanto mano derecha y marco para entre a la izquierda
					iOpcion = 1;
					sParametrox = "2 0 1 3 "+ iFolio;
					//ejecutarApplet(sParametrox,sRutaM); //abrimos la mano derecha ya que selecciono las dos manos
					ejecutaWebService(sRutaM, sParametrox);
				}
				  $( this ).dialog("close");
				},
			}
	});
}

function GuardarDedosFaltantes(OBJ,sCodigo1,sCodigo2,sCodigo3,sCodigo4,sCodigo5,sCodigo6,sCodigo7,sCodigo8,sCodigo9,sCodigo10)
{
	var iInserto = 0;
	var sCadena = '';

	if (OBJ.total > 0)
	{
		for ( var i=1; i<= OBJ.total; i++ )
		{
			if (OBJ.numdedo[i] == 1) //Pulgar  Derecho
			{
				sCadena = "opcion=3&excepcion=" + sCodigo1 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 2) //Indice  Derecho
			{
				sCadena = "opcion=3&excepcion=" + sCodigo2 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 3) //Medio  Derecho
			{
				sCadena = "opcion=3&excepcion=" + sCodigo3 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 4) //Anular  Derecho
			{
				sCadena = "opcion=3&excepcion=" + sCodigo4 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 5) //Me�ique  Derecho
			{
				sCadena = "opcion=3&excepcion=" + sCodigo5 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 6) //Pulgar  Izquierdo
			{
				sCadena = "opcion=3&excepcion=" + sCodigo6 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 7) //Indice  Izquierdo
			{
				sCadena = "opcion=3&excepcion=" + sCodigo7 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 8) //Medio  Izquierdo
			{
				sCadena = "opcion=3&excepcion=" + sCodigo8 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 9) //Anular  Izquierdo
			{
				sCadena = "opcion=3&excepcion="+ sCodigo9 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}
			else if(OBJ.numdedo[i] == 10) //Me�ique  Izquierdo
			{
				sCadena = "opcion=3&excepcion=" + sCodigo10 + "&dedo=" + OBJ.numdedo[i] + "&folio=" + iFolio;
			}

			$.ajax(
				{
					async: false,
					cache: false,
					data:sCadena,
					 url: 'php/capturaenrolamientotrabajador.php',
					 type: 'POST',
					 dataType: 'json',
					 success: function(data)
					 {
						//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
						datos = data;

						//Valida que se aya realizado el inser correctamente
						if (datos.ires == 1)
						{
							iInserto = 1;
						}
						else
						{
							iInserto = 0;
						}
					},
					error: function(a, b, c)
					{
					},
					beforeSend: function()
					{
					}
				}
			);
		}

		if (iInserto == 1)
		{
				 nist = validarNIST(iFolio);
				$('#huellasFaltantes').dialog("close");
		}
		else
		{

		}
	}
	else
	{
		//llamara la funcion del NIST
		 nist = validarNIST(iFolio);
	}
}

function obtenerDedosFaltantes()
{
	var sCadena = '';
	var iTotalEncont = 0;
	var sHtml = '';
	var i = 0;
	//Inicializo el objeto global
	OBJ.nombrededo = '';
	OBJ.empleado = '';
	OBJ.numdedo = '';
	OBJ.imagen = '';
	OBJ.total = 0;

	if( iexcepcion == 0 )
	{
		iSelecciono = 1;
	}
	else if ( iexcepcion == 1 )
	{
		iSelecciono = 2;
	}
	else if( iexcepcion == 2 )
	{
		iSelecciono = 3;
	}

	sCadena += "opcion=8&folio=" +iFolio +"&selecciono=" +iSelecciono;

	$.ajax({
		async: false,
		cache: false,
		data:sCadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			OBJ = datos;
			iTotalEncont = datos.total;
			/*
			En el siguiente orden corresponden el numero de dedo.
			1-Pulgar  Derecho         cboPulDer
			2-Indice  Derecho         cboIndDer
			3-Medio  Derecho          cboMedDer
			4-Anular  Derecho         cboAnuDer
			5-Me�ique  Derecho        cboMenDer
			6-Pulgar  Izquierdo       cboPulIzq
			7-Indice  Izquierdo       cboIndIzq
			8-Medio  Izquierdo        cboMedIzq
			9-Anular  Izquierdo       cboAnuIzq
			10-Me�ique  Izquierdo      cboMenIzq
			*/
			if (iTotalEncont > 0) //Significa que por lo menos tiene un dedo que le falta.
			{
				sHtml += '<tr>';
				sHtml +=	'<td  > No se detectaron las huellas de los siguientes dedos, debes identificar el motivo o realizar nuevamente la captura completa. </td> ';
				sHtml +='</tr>';

				for ( i=1; i<= iTotalEncont; i++ )
				{
					if (datos.numdedo[i] == 1) //Pulgar  Derecho
					{
						iBandDed1 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<br><label for=\"Name\">Pulgar  Derecho</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboPulDer\" name=\"cboPulDer\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 2) //Indice  Derecho
					{
						iBandDed2 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Indice  Derecho</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboIndDer\" name=\"cboIndDer\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 3) //Medio  Derecho
					{
						iBandDed3 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Medio  Derecho</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboMedDer\" name=\"cboMedDer\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 4) //Anular  Derecho
					{
						iBandDed4 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Anular  Derecho</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboAnuDer\" name=\"cboAnuDer\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 5) //Me�ique  Derecho
					{
						iBandDed5 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Me&ntilde;ique  Derecho</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboMenDer\" name=\"cboMenDer\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 6) //Pulgar  Izquierdo
					{
						iBandDed6 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Pulgar  Izquierdo</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboPulIzq\" name=\"cboPulIzq\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 7) //Indice  Izquierdo
					{
						iBandDed7 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Indice  Izquierdo</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboIndIzq\" name=\"cboIndIzq\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 8) //Medio  Izquierdo
					{
						iBandDed8 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Medio  Izquierdo</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboMedIzq\" name=\"cboMedIzq\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 9) //Anular  Izquierdo
					{
						iBandDed9 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Anular  Izquierdo</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<select id=\"cboAnuIzq\" name=\"cboAnuIzq\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
					else if (datos.numdedo[i] == 10) //Me�ique  Izquierdo
					{
						iBandDed10 = 1;
						sHtml +='<tr >';
						sHtml += '<td>';
							sHtml +='<label for=\"Name\">Me&ntilde;ique  Izquierdo</label>';
							sHtml +=	'&nbsp;&nbsp;&nbsp;<select id=\"cboMenIzq\" name=\"cboMenIzq\" tabindex = 1 class =\"comboBoxVista\">';
							sHtml +=		'<option value = \"0\">SELECCIONE UNA OPCION </option>';
							sHtml +=		'<option value = \"007\"> Huella no se puede leer </option>';
							sHtml +=		'<option value = \"010\"> Con huella dactilar parcial </option>';
							sHtml +=		'<option value = \"008\"> Amputado </option>';
							sHtml +=		'<option value = \"009\"> Lesionado </option>';
							sHtml +=	'</select>';
							sHtml +='</td>';
						sHtml += '</tr>';
					}
				}//cierra el for

				$("#huellasFaltantes").empty();
				$("#huellasFaltantes").html(sHtml);
				$('#huellasFaltantes').dialog('open');
			}
			else
			{
				 nist = validarNIST(iFolio);
			}
		},
		error: function(a, b, c)
		{
		},
		beforeSend: function()
		{
		}
	});
}

function  validarEnrolador()
{
	var iRet2 = 0;
	var sNumEnrolador = iEmpleado;
	if (sNumEnrolador !='')
	{
		if (sNumEnrolador.length  == 8)
		{
	       iRet2 = verificaEnrolador();
		}
		else
		{
		   mensaje = 'N\u00FAmero de empelado incompleto';
	       mostrarmensaje(mensaje,iEliminaCont);
		}
	}//fin primer if
	else
	{
	   mensaje = 'Favor de introducir su n\u00FAmero de empleado';
	   mostrarmensaje(mensaje,iEliminaCont);
	}
}

function validarNISTMorpho(iFolio)
{
	var iRetNist = 0;
	var cadena = '';
	var sNumEnrolador = iEmpleado;
	cadena = "opcion=7&folioenrol=" + iFolio;
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
		 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();
			iRetNist = datos.resulta;

			sMensajeAux = '[capturaenrolamientotrabajador::validarNISTMorpho] --> Respuesta: ' + iRetNist;
			grabarLogJs(sMensajeAux);
			
			if (iRetNist == 1)
			{
				mensajeCalidad = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo.';
				mensajeCalidadLlamar(mensajeCalidad);
			}
			else if (iRetNist == -1)
			{
				//llamar de Nuevo y limpiar las variables necesarias
				mensajeCalidad = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo.';
				mensajeCalidadLlamar(mensajeCalidad);
			}
			else if (iRetNist == 0)
			{
				//No se cumplio con el NIST ya que no se guardo nada en las  tablas de los templates
				//Pedir de nuevo
				mensajeCalidad = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo.';
				mensajeCalidadLlamar(mensajeCalidad);
			}
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
			//	bloquearUI("Espere un momento por favor...");
		}
	});
	return iRetNist;
}

function validarNIST(iFolio)
{
	var iRetNist = 0;
	var cadena = '';
	var sNumEnrolador = iEmpleado;
	var mensajeVerificacion = '';
	cadena = "opcion=7&folioenrol=" + iFolio;
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
		 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			
			iRetNist = datos.resulta;
			
			sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Folio Enrolamiento: ' + iFolio + ' Respuesta: ' + iRetNist + ' NumCapturaHuella: ' + iNumCapturaHuella + ' ContadorNIST: ' + contadorNIST;
			grabarLogJs(sMensajeAux);
			if (iRetNist == 1)//Cumple validacion NIST tabla Procesar NIST
			{
				//Aqui mando el mensaje solicitando la 11 Huella
				if (OSName == "Android") {
					mensajeVerificacion = 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Five-O.';
				}/* else{
					mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
				}*/
				//mensajeHuellaVerificacion(mensajeVerificacion); 
				huellaDeVerificacion();//manda solicitar la 11 huella
			}
			else {
				if( iNumCapturaHuella >= 2 ) //Si es la segunda captura
				{
					excepcionarDedosEnRolTrabajador(iFolio);//Excepcionamos los dedos para cumplir tabla NIST
				}
				else//continuar con el flujo para pedir captura de huellas de nuevo
				{
					if (iRetNist == -1)
					{
						//Mandar mensaje de que no se cumplio con LA CALIDAD y mostrar el Dialog De Nuevo
						contadorNIST = contadorNIST + 1;
						
						if (contadorNIST == 3)
						{
							sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Trabajador se enrolo sin huellas dactilares.';
							grabarLogJs(sMensajeAux);
							//mensaje que el trabajador se va enrolar sin huellas y mandar la excepcion 007
							mensajeSinHuellas = 'Trabajador se enrolo sin huellas dactilares.';
							mensajeEnroladoSinHuellas(mensajeSinHuellas);//obtenerDedosFaltantes();//vemos que dedos faltan para mostrar el Dialog
							contadorNIST = 0;
						}
						else
						{
							sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Indicarle al trabajador que las huellas capturadas no fueron correctas.';
							grabarLogJs(sMensajeAux);
							mensajeNIST = 'Promotor: Indicarle al trabajador que las huellas capturadas no fueron correctas.';
							mensajeCalidadNIST(mensajeNIST);
						}
					}
					else if (iRetNist == 0)
					{
						//No se cumplio con el NIST ya que no se guardo nada en las  tablas de los templates
						//Pedir de nuevo
						sMensajeAux = '[capturaenrolamientotrabajador::validarNIST] --> Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo.';
						grabarLogJs(sMensajeAux);
						mensajeNIST = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo.';
						mensajeCalidadNIST(mensajeNIST);
					}
				}
			}
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
		//	bloquearUI("Espere un momento por favor...");
		}
	});
	return iRetNist;
}

function mensajeCalidadNIST(mensajeNIST)
{
	$('.content-loader').hide();
	$("#divMensaje9").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeNIST+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
			//Capturar de Nuevo la pantalla dependiendo de la opcion seleccionada
				if (reIzquierda == 2)
			{
				//llamar mano izquierda y maracar para que entren los pulgares
				iOpcion = 5;
				sParametrox = "2 1 1 3 "+ iFolio;

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (reDerecha == 1)
			{
				//llamar mano Derecha y marco para que entre en el pulgar
				iOpcion = 5;
				sParametrox = "2 0 1 3 "+ iFolio;

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';


				ejecutaWebService(sRutaM, sParametrox);
			}
			else if (reAmbas == 3)
			{
				//llamar Ambas Manos aqui levanto mano derecha y marco para entre a la izquierda
				iOpcion = 1;
				sParametrox = "2 0 1 3 "+ iFolio;

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);
			}
			$( this ).dialog("close");
			},
		}
	});
}

function mensajeEnroladoSinHuellas(mensajeSinHuellas)
{
	$('.content-loader').hide();
	$("#divMensaje11").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeSinHuellas+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				if (iSelecciono == 1) //Ambas manos
				{
					guardarExcepciones(iFolio,'007',11);
				}
				else if (iSelecciono == 2) //Solo mano Derecha
				{
					guardarExcepciones(iFolio,'007',12);
				}
				else if (iSelecciono == 3)//solo mano izquierda
				{
					guardarExcepciones(iFolio,'007',13);
				}
				$( this ).dialog("close");
			},
		}
	});
}

function huellaDeVerificacion()//se cra funcion para no ocupar el modal de la funcion mensajeHuellaVerificacion en el folio 790
{
	if (OSName == "Android" && iOpcion == 15) {
		iOpcion = 13;
		sParametrox = "2 -1 1 11 " + iFolio; //llamamos la verificacion
		//ejecutarApplet(sParametrox,sRutaM);
		ejecutaWebService(8, 8);
	} else {
		iOpcion = 13;
		sParametrox = "2 -1 1 11 " + iFolio; //llamamos la verificacion
		//ejecutarApplet(sParametrox,sRutaM);
		ejecutaWebService(sRutaM, sParametrox);
	}

}

function mensajeHuellaVerificacion(mensajeVerificacion)
{
	$('.content-loader').hide();
	$("#divMensaje6").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeVerificacion+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Verificar Huella del Empleado
				//Verificar Huella del Empleado
				if(OSName == "Android" && iOpcion == 15 ){
					iOpcion = 13;
					sParametrox = "2 -1 1 11 "+ iFolio; //llamamos la verificacion
					//ejecutarApplet(sParametrox,sRutaM);
					ejecutaWebService(8, 8);
				}else{
					iOpcion = 13;
					sParametrox = "2 -1 1 11 "+ iFolio; //llamamos la verificacion

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaM + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaM, sParametrox);
				}

				$( this ).dialog("close");
			},
		}
	});
}

function cancelarEnrolamiento()
{
	var iRetActualizar = 0;
	var cadena = '';
	var sNumEnrolador = iEmpleado;
	var estatusAct = 9;
	cadena = "opcion=9&folioenrol=" + iFolio +  "&estatusCancelar=" + estatusAct;
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();
			iRetActualizar = datos.actualizo;

			if (iRetActualizar == 1)
			{
				cerrarNavegador();
			}
			else if (iRetActualizar == 0)
			{
			}
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
		//	bloquearUI("Espere un momento por favor...");
		}
	});
	return iRetNist;
}

function verificaEnrolador()
{
    var iRet = 0;
	var cadena = '';
	var sNumEnrolador = iEmpleado;
	cadena = "opcion=1&empleado=" + sNumEnrolador;
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();
			iRet = datos.valor;
			
			if (iRet == 1)
			{
				ifolio = guardarFolio();
				sTexto = '[capturaenrolamientotrabajador::verificaEnrolador] --> Entro ala verificación del enrolador para guardar folioenrolamiento para la solicituf ->[' + iFolioSolicitud + ']' + 'Folio: ' + ifolio;
				grabarLogJs(sTexto);
			}
			else if (iRet == 0)
			{
				sMensajeAux = '[capturaenrolamientotrabajador::verificaEnrolador] --> Promotor, no tienes un enrolamiento aceptado y no puedes realizar ningun Enrolamiento. Consulta el Estatus de tu Enrolamiento.';
				grabarLogJs(sMensajeAux);
				mensaje = "Promotor, no tienes un enrolamiento aceptado y no puedes realizar ningun Enrolamiento. Consulta el Estatus de tu Enrolamiento.";
				mensajeEnrolador(mensaje,iRet);
			}
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
		//	bloquearUI("Espere un momento por favor...");
		}
	});
	return iRet;
}
function optenerLigaEnrolamiento()
{
	guardarbitacoraenrolamiento();
    var cadena = '';
	cadena = "opcion=5&folio=" + iFolio;
	var sNumEnrolador = '';
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();
			mostrarFormato(datos.dato);

			sMensajeAux = '[capturaenrolamientotrabajador::optenerLigaEnrolamiento] --> Liga: ' + datos.dato;
			grabarLogJs(sMensajeAux);
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
			//bloquearUI("Espere un momento por favor...");
		}
	});
}

function getQueryVariable(varGet)
{
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	
	for (var i=0;i<vars.length;i++)
	{
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		
		if(par[0] == varGet) // [0] nombre de variable, [1] valor
		{
			return par[1];
		}
	}
	return(false);
}

function mostrarFormato(liga)
{
		
	$('.content-loader').hide();
	$.ajax({
		url 		: liga,
		async		: true,
		cache		: true,
		type: 'POST',
		dataType: 'json',
		success: function(data){
			var ruta = data.rutapdf;
			recibirPDF(ruta);
		},
		beforeSend: function(){
		},
		error: function(a,b,c){

		}
	});
}

function recibirPDF (ruta)
{
	console.log(ruta);
	ruta = ruta.replace("/sysx/progs/web/","../");
	console.log(ruta);
	$('.content-loader').hide();
	var sHtml =  '<iframe src=\"' + ruta + '\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>';
	$("#carga").empty();
	$("#carga").html(sHtml);
	$('#carga').dialog('open');
}

function mensajeHuellaPromotor(mensajeFinPromotor)
{
	$('.content-loader').hide();
  $("#divMensaje5").dialog
	({
		title: 'Promotor',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeFinPromotor+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				cancelarEnrolamiento();
				$( this ).dialog("close");
			},
		}
	});
}

/*
//pend. 280
function mensajeFinalPrevio(mensajeFin)
{
  $("#divMensaje4").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeFin+"</p>");
		},
		buttons:
			{
				"Aceptar" : function()
				{
				//	window.parent.close();
				  $( this ).dialog("close");
				  mensajeFinal('Se ha concluido con el enrolamiento, de clic en el bot&oacuten aceptar para finalizar.');
				},
			}
	});
}
//pend. 280
*/

function mensajeFinal(mensajeFin)
{
	$('.content-loader').hide();
	$("#divMensaje4").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeFin+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				verificaconsecutivoeibio(iFolioSolicitud);
			//	window.parent.close();
				$( this ).dialog("close");
				cerrarNavegador();
			},
		}
	});
}

function mensajeFinalAndroid(mensajeFin)
{
	$('.content-loader').hide();
	$("#divMensaje4").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeFin+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$( this ).dialog("close");
				Android.terminarEnrolamiento("1");
			},
		}
	});
}

function mensajeFinalAndroidError(mensajeFin)
{
	$('.content-loader').hide();
	$("#divMensaje4").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajeFin+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$( this ).dialog("close");
				$('#huellas').dialog("open");
			},
		}
	});
}

function cerrarNavegador()
{
	if(OSName == 'Android'){
		Android.terminarEnrolamiento("0");
	}else{
		//firefox
		if(navigator.appName.indexOf('Netscape') >= 0 )
		{
			javascript:window.close();
		}
		else
		{
			if(navigator.appName.indexOf('Microsoft') >= 0)
			{//internet explorer
				var ventana = window.self;
				ventana.opener = window.self;
				ventana.close();
			}
		}
	}	
}

//bloque la pantalla
function bloquearUI(mensaje)
{
    //armamos un codigo html para representar el dialogo de cargando
 	var html = "<div id='bloquea'>";
	html += "<span class='Espera'>";
	html += "</span><p style='font-size:1.2em;float:left; margin: 7px 0px 0px 25px;' >" + mensaje +"</p>";
	html += "</div>";
    //inicializamos el bloqueo de pantalla con sus respectivas opciones.
    $.blockUI({ message: html, css: {'border-radius': '17px', 'width':'auto' }});
}//cierre de la function bloquearUI

//DESBLOQUEA LA PANTALLA...
function desbloquearUI()
{
    $.unblockUI();
}//cierre de la function desbloquearUI

function mostrarMensajeFirma(mensajefirma)
{
	$('.content-loader').hide();
	$("#divMensaje2").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajefirma+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Para probar mandare un mensaje con la solicitud de la firma
				mensajefirmaS = 'Promotor: Favor de indicarle al trabajador que firmar\u00E1 el  Formato  de  Enrolamiento  autorizando  los datos  biom\u00E9tricos  los cuales  se  limitar\u00E1  a  la  ejecuci\u00F3n  de  las  acciones  y procesos  establecidos  en t\u00E9rminos de la Ley de los Sistemas de Ahorro para el Retiro.';
				mostrarSolicitarFirma(mensajefirmaS);
				$(this).dialog( "close" );
			},
		}
	});
}

function mostrarSolicitarFirma(mensajefirmaS)
{
	$('.content-loader').hide();
  $("#divMensaje").dialog
	({
		title: 'Firma Digital del Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajefirmaS+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Para probar mandare un mensaje con la solicitud de la firma
				iOpcion = 4;
				
				if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
				{
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "_FTRAB";
				}
				else
				{
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "-S_FTRAB";
				}
				ruta =	"C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = ruta + ' ' + sParametros;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';
				
				ejecutaWebService(ruta, sParametros);
				$(this).dialog( "close" );
			},
		}
	});
}

function mostrarMensajeFirmaConectar(mensajefirmaF)
{
	$('.content-loader').hide();
	$("#divMensaje17").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajefirmaF+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				if( iexcepcion == 3 || iexcepcion == 4 )
				{
					iOpcion = CAPTURA_LEYENDA_EXCEPCION;
					sParametros = 'nombre-firma-leyenda';  // ---------------------------------- aqui ira el nombre de la leyenda de la excepcion
					if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
					{	sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "_LTRAB";	}
					else
					{	sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "-S_LTRAB";	}
					ruta =	"C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = ruta + ' ' + sParametros;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(ruta, sParametros);
					$(this).dialog( "close" );
				}
				else{
					//Para probar mandare un mensaje con la solicitud de la firma
					iOpcion = 4;
					
					if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
					{
						sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "_FTRAB";
					}
					else
					{
						sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "-S_FTRAB";
					}
					ruta =	"C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = ruta + ' ' + sParametros;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(ruta, sParametros);
					$(this).dialog( "close" );
				}
			},
		}
	});
}

function mostrarMensajePrincipal(msj)
{
	$('.content-loader').hide();
	$("#divMensaje16").dialog
	({
		title: 'Captura de Huellas',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+msj+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				sTexto = 'Entro al enrolamiento por primera vez para  el folio ->[' + iFolioSolicitud + ']';
				grabarLogJs(sTexto);
				//ifolio = guardarFolio();
				//Integramos la funcion de verificar folio pendiante 603
				ifolio = verificarFolio();
				$(this).dialog( "close" );
			},
		}
	});
}

//Funci�n para mostrar mensaje
function mostrarmensaje(mensaje,iEliminaCont)
{
	$('.content-loader').hide();
	var iOpc  = 0;
	$("#divMensaje15").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensaje+"</p>");
		},
		buttons:
		{
			"SI" : function()
			{
				cancelarEnrol();
				$(this).dialog( "close" );
			},
			"NO" : function()
			{
			    $('#btnAceptar').attr('disabled', false);
				$(this).dialog( "close" );
				window.close(); //cierra el navegador.
			}
		}
	});
}

function mensajeHuella(mensaje)
{
	var con  = 0;
	$('.content-loader').hide();
	$("#divMensaje14").dialog
	({
		title: 'Captura de Huella',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensaje+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				con = con + 1;
				if (con == 2)
				{
					cont = 0;
				}
				else
				{
					sRutax = "C:\\SYS\\PROGS\\heAfore.exe";
					//ejecutarApplet(ipServidor,sRutax);
					ejecutaWebService(sRutax, ipServidor);
				}
				$(this).dialog( "close" );
			}
		}
	});
}

//Funci�n que muestra el mensaje de validacion de enrolador
function mensajeEnrolador(mensaje,iEstado)
{
	var iOpc  = 0;
	$('.content-loader').hide();
	$("#divMensaje13").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensaje+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				location.reload(); //Recargamos la pagina
				$(this).dialog( "close" );
			}
		}
	});
}

function guardarFolio()
{
	grabarLogJs("Entro a guardar folio");
	//var iFolio = 0;
	var cadena = '';
	var sNumEnrolador = iEmpleado;
	cadena = "opcion=2&empleado=" + sNumEnrolador + "&curp=" + chCurp + "&solicitud=" + smTipoSolicitud  + "&foliosolicitud=" + iFolioSolicitud;
	sMensajeAux = '[capturaenrolamientotrabajador::guardarFolio] --> Cadena Parametros: ' + cadena;
	grabarLogJs(sMensajeAux);
	$.ajax({
		async: true,
		cache: false,
		data: cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
		 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();

			iFolio = data.ires;
			
			iOpcion = 3;
			sMensajeAux = '[capturaenrolamientotrabajador::guardarFolio] --> Valor Folio: ' + iFolio;
			grabarLogJs(sMensajeAux);
			//Aqui llamar metodo para obtener ipServidor
			if (iFolio > 0)
			{
			  var cadena = '';
			  cadena = "opcion=6";
				$.ajax({
					async: false,
					cache: false,
					data:cadena,
					url: 'php/capturaenrolamientotrabajador.php',
					type: 'POST',
					dataType: 'json',
					success: function(data)
					{
						//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
						datos = data;
						//desbloquearUI();
						ipServidor = datos;
						
						if(smTipoSolicitud == 33 || smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527){
							MensajeAux = 'entra a obtener excepcion del trabajador si no viene de algun flujo de servicios';
							grabarLogJs(sMensajeAux);
							ObtenerExcepcion2(chCurp);
						}else{
							mostraDialogoHuellas();
						}
						//sRutax = "C:\\SYS\\PROGS\\heAfore.exe";
						//ejecutarApplet(ipServidor,sRutax);
					},
					error: function(a, b, c)
					{
						//desbloquearUI();
					},
					beforeSend: function()
					{
						//bloquearUI("Espere un momento por favor...");
					}
				});
			}
			else
			{	}
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
			//bloquearUI("Espere un momento por favor...");
		}
	});

	return iFolio;
}

//Verificacion del folio de enrolamiento pendiene ( 603 )
function verificarFolio() {  //divenrolamientoTerminado
	var cadena = '';
	cadena = "opcion=29&curp=" + chCurp + "&foliosolicitud=" + iFolioSolicitud + "&solicitud=" + smTipoSolicitud;
	sMensajeAux = '[capturaenrolamientotrabajador::verificarfolio] --> Cadena Parametros: ' + cadena;
	grabarLogJs(sMensajeAux);
	$.ajax({
		async: true,
		cache: false,
		data: cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function (data) {
			//pasamos los datos obtenidos
			datos = data;
			iFolio = data.folioenrolamiento;
			iExcepcionesRet = data.excepcion; // variable declarada para verificar si el folio se guardo con exepciones
			iSelecciono = data.selecciono;
			numeroEmpleadoAutoriza = data.numeroempleadoautoriza;

			msj = 'Folio Verificado: ' + iFolio;
			grabarLogJs(msj);


			if (iFolio == 0 || (iEmpleado != numeroEmpleadoAutoriza)) {
				if (iFolio == 0) {
					msj = 'Folio: ' + iFolioSolicitud + ' no cuenta con un proceso de enrolamiento del dia de hoy, Se continuara con la captura de enrolamiento normal.'					
				} else {
					msj = 'Promotor: ' + iEmpleado + ' no es el que realizo el enrolamiento anterior: '+numeroEmpleadoAutoriza +' se va generar un nuevo folio.'
				}
				grabarLogJs(msj);
				ifolio = guardarFolio();
			}
			else {
				msj = "Promotor, el Trabajador ya cuenta con un enrolamiento previo terminado, no es necesario volver a generarlo, favor de continuar con el trámite";
				mostrarEnrolamientoTerminado(msj)
			}
		},
		error: function (a, b, c) {
			//desbloquearUI();
		},
		beforeSend: function () {
			//bloquearUI("Espere un momento por favor...");
		}
	});

	return;
}

//funcion para mostrar mensaje de enrolamiento terminado pendiente (603)
function mostrarEnrolamientoTerminado(msj) {
	$("#divMensajeEnrolamientoTerminado").dialog
		({
			title: 'Enrolamiento Terminado',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + msj + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {

					msj = 'Promotor, favor de colocar tu dedo indice en el sensor';
					MismoPromotorMismoTrabajador(msj);
					$(this).dialog("close");

				},
			}
		});
}

//funcion para mostrar mensaje de enrolamiento terminado pendiente (603)
function MismoPromotorMismoTrabajador(msj) {
	$("#divMensajeMismoPromotorMismoTrabajador").dialog
		({
			title: 'Enrolamiento Terminado',
			autoOpen: true,
			resizable: false,
			width: '350',
			height: 'auto',
			modal: true,
			closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
			position: { my: "center", at: "center", of: $("body"), within: $("body") },//Posicionar el dialog en el centro
			open: function (event, ui) {
				$(".ui-dialog-titlebar-close", ui.dialog).hide();
				$(this).html("<p>" + msj + "</p>");
			},
			buttons:
			{
				"Aceptar": function () {

					//VERIFICACION_EMPLEADO_HE
					iOpcion = VERIFICACION_EMPLEADO_HE;
					sParametrox = servidorIP;

					//PDTE 961: Tabla control de los params de ejecucion.
					cParametrosEnvio = sRutaHE + ' ' + sParametrox;
					guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
					////cParametrosEnvio='';

					ejecutaWebService(sRutaHE, sParametrox);

					$(this).dialog("close"); //divMensajeMismoPromotorMismoTrabajador
				},
			}
		});
}

function guardarExcepciones(iFolioGenerado,chExcepcion,idedos)
{
	var iRet = 0;
    var cadena = '';
	var sNumEnrolador = iEmpleado;
	cadena = "opcion=3&folio=" + iFolioGenerado + "&excepcion=" + chExcepcion + "&dedo=" + idedos;
	sMensajeAux = '[capturaenrolamientotrabajador::guardarExcepciones] --> Cadena Parametros: ' + cadena;
	grabarLogJs(sMensajeAux);
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
		 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();
			iRet = datos.ires;
			
			if (chExcepcion == '007')
			{
				//aqui hay q llamar lo verificacion del empleado
				//Verificar Huella del Empleado
				iOpcion = 13;
				sParametrox = "2 -1 1 11 "+ iFolio; //llamamos la verificacion

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = sRutaM + ' ' + sParametrox;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(sRutaM, sParametrox);

				//mensajefirmaS = 'Promotor: Favor de indicarle al trabajador que firmar\u00E1  el  Formato  de  Enrolamiento  autorizando  los datos  biom\u00E9tricos  los cuales  se  limitar\u00E1  a  la  ejecuci\u00F3n  de  las  acciones  y procesos  establecidos  en t\u00E9rminos de la Ley de los Sistemas de Ahorro para el Retiro';
				//mostrarSolicitarFirma(mensajefirmaS);
			}
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
			//bloquearUI("Espere un momento por favor...");
		}
	});
	return iRet;
}

function cancelarEnrol()
{
	var iRetCancelar = 0;
    var cadena = '';
	var sNumEnrolador = iEmpleado;
	cadena = "opcion=4&estatus=9" + "&folio=715" ;
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
		 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
			datos = data;
			//desbloquearUI();
			iRetCancelar = datos.val;
			sMensajeAux = '[capturaenrolamientotrabajador::cancelarEnrol] --> Respuesta: ' + iRetCancelar;
			grabarLogJs(sMensajeAux);
		},
		error: function(a, b, c)
		{
			//desbloquearUI();
		},
		beforeSend: function()
		{
			//bloquearUI("Espere un momento por favor...");
		}
	});
	return iRetCancelar;
}

//Se agrega Funcion para verificar que esten los registros una vez que el promotor coloca la huella que autoriza
function verificacionEnrolAutoriza(iFolio)
{
	var iRetAutoriza = 0;
	cadena = "opcion=11&folio=" + iFolio;
	sMensajeAux = '[capturaenrolamientotrabajador::verificacionEnrolAutoriza] --> Cadena Parametros: ' + cadena;
	grabarLogJs(sMensajeAux);
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			datos = data;

			iRetAutoriza = datos.retorno;
			
			sMensajeAux = '[capturaenrolamientotrabajador::verificacionEnrolAutoriza] --> Folio: ' + iFolio + ' Respuesta: ' + iRetAutoriza;
			grabarLogJs(sMensajeAux);
			if (iRetAutoriza == 1)
			{
				iSelecciono = 4;
				if( iexcepcion == 3  )
				{
					obtenermensajegerente(7);
				}
				else if( iexcepcion == 4 )
				{
					obtenermensajegerente(6);
				}
				else{
					if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
					{
						guardarbitacoraenrolamiento();
						//mensajeFin = 'Se ha concluido con el enrolamiento, de clic en el boton aceptar para finalizar.';
						//mensajeFinal(mensajeFin);
						cerrarNavegador();//790
					}
					else
					{
						mensajefirmaS = 'Promotor: Favor de indicarle al trabajador que firmar\u00E1  el  Formato  de  Enrolamiento  autorizando  los datos  biom\u00E9tricos  los cuales  se  limitar\u00E1  a  la  ejecuci\u00F3n  de  las  acciones  y procesos  establecidos  en t\u00E9rminos de la Ley de los Sistemas de Ahorro para el Retiro';
						mostrarSolicitarFirma(mensajefirmaS);
					}
					
				}
			}
			else if (iRetAutoriza == 0)
			{
			    //Mandar mensaje y pedir de nuevo la huella 3 intentos
				numeroIntentos++;
				mensajeVerificacion = 'Promotor: coloca tu huella nuevamente.';
				mensajeReintentoVerificacion(mensajeVerificacion);
			}
		},
		error: function(a, b, c)
		{

		},
		beforeSend: function()
		{

		}
	});
	return iRetAutoriza;
}

function LimpiarHuellas(iFolio)
{
	var iRetorno = 0;
    var cadena = '';
	cadena = "opcion=12" + "&folio=" + iFolio ;
	sMensajeAux = '[capturaenrolamientotrabajador::LimpiarHuellas] --> Cadena Parametros: ' + cadena;
	grabarLogJs(sMensajeAux);
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
			datos = data;
			iRetorno = datos.regreso;

			sMensajeAux = '[capturaenrolamientotrabajador::LimpiarHuellas] --> Folio: ' + iFolio + ' Respuesta: ' + iRetorno;
			grabarLogJs(sMensajeAux);
		},
		error: function(a, b, c)
		{

		},
		beforeSend: function()
		{

		}
	});
	return iRetorno;
}

if (OSName == "Windows") {
		smTipoSolicitud = getQueryVariable("tiposolicitud");
	function mostraDialogoHuellas() 
	{		
			if (iBanderaSalir){
				$('.content-loader').show();
			}else{
				$('.content-loader').hide();
			}
			
			var sHtml = '';
			sHtml += '<script type=\"text/javascript\">';
			sHtml += '	$( "#huellas" ).dialog({ closeOnEscape: false });';
			sHtml += '</script >';
			sHtml += '<tr>';
			sHtml += '<td  > Promotor a Continuaci\u00F3n debes indicar las huellas de las manos a capturar. </td> ';
			sHtml += '<br></br>';
			sHtml += '</tr>';
			sHtml += '<tr>';
			sHtml += '<td  >';
			sHtml += '<br><input type=\"radio\"  id =\"rbnHuellaA\" name=\"rbnHuella\" value=\"1\" checked> Ambas Manos.';
			sHtml += '<br><br><input type=\"radio\" id =\"rbnHuellaB\" name=\"rbnHuella\" value=\"2\" >Solo Mano Derecha.';
			sHtml += '<br><br><input type=\"radio\" id =\"rbnHuellaC\" name=\"rbnHuella\" value=\"3\" >Solo Mano Izquierda.';
			//Se quita la opci�n de Manos Amputadas ya que se solicito un cambio a traves de Operaciones (Gabriela Sarralde)
			//sHtml +=		'<br><br><input type=\"radio\" id =\"rbnHuellaD\" name=\"rbnHuella\" value=\"4\" >Sin Manos.';

			//se vuelve habilitar la opcion Manos Amputadas a peticion del cliente Mario Guzman y ademas de modifican los caminos de
			//de manos incapacitadas
			sHtml += '<br><br><input type=\"radio\" id =\"rbnHuellaD\" name=\"rbnHuella\" value=\"4\" >Ambas Manos Amputadas.';
			sHtml += '<br><br><input type=\"radio\" id =\"rbnHuellaD\" name=\"rbnHuella\" value=\"5\" >Ambas Manos Lesionadas.';

			sHtml += '</td  >';
			sHtml += '</tr>';
			$("#huellas").empty();
			$("#huellas").html(sHtml);
			$('#huellas').dialog('open');	
	}
}
else	
{
	function mostraDialogoHuellas()
	{
		$('.content-loader').hide();
		var sHtml = '';
		sHtml +='<script type=\"text/javascript\">';
		sHtml +='</script >';
		sHtml +='	$( "#huellas" ).dialog({ closeOnEscape: false });';
		sHtml += '<tr>';
		sHtml +=	'<td  > Promotor a Continuaci\u00F3n debes indicar las huellas de las manos a capturar. </td> ';
		sHtml +=	'<br></br>';
		sHtml +='</tr>';
		sHtml += '<tr>';
		sHtml +=	'<td  >';
		sHtml +=		'<br><input type=\"radio\"  id =\"rbnHuellaA\" name=\"rbnHuella\" value=\"1\" checked> Ambas Manos.';
		sHtml +=	'</td  >';
		sHtml +='</tr>';
		$("#huellas").empty();
		$("#huellas").html(sHtml);
		$('#huellas').dialog('open');
	}
}

//Funcion que muestra el dialogo para solo mano derecha
function mostraDialogoHuellasDerecha()
{
	$('.content-loader').hide();
	var sHtml = '';
	sHtml +='<script type=\"text/javascript\">';
	sHtml +='	$( "#soloManoDerecha" ).dialog({ closeOnEscape: false });';
	sHtml +='</script >';
	sHtml += '<tr>';
	sHtml +=	'<td  >';
	sHtml +=		'<br><input type=\"radio\"  id =\"rbnManoDerecha\" name=\"rbnManoDerecha\" value=\"006\" checked> Lesi\u00F3n Mano Izquierda';
	sHtml +=		'<br><br><input type=\"radio\" id =\"rbnManoDerecha\" name=\"rbnManoDerecha\" value=\"003\" >No Tiene Mano Izquierda';
	sHtml +=	'</td  >';
	sHtml +='</tr>';

	$("#soloManoDerecha").empty();
	$("#soloManoDerecha").html(sHtml);
	$('#soloManoDerecha').dialog('open');
}

//Funcion que muestra el dialogo para solo mano Izquierda
function mostraDialogoHuellasIzquierda()
{
	$('.content-loader').hide();
	var sHtml = '';
	sHtml +='<script type=\"text/javascript\">';
	sHtml +='	$( "#soloManoIzquierda" ).dialog({ closeOnEscape: false });';
	sHtml +='</script >';
	sHtml +='<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />';
	sHtml += '<tr>';
	sHtml +=	'<td  >';
	sHtml +=		'<br><input type=\"radio\"  id =\"rbnManoIzquierda\" name=\"rbnManoIzquierda\" value=\"005\" checked> Lesi\u00F3n Mano Derecha.';
	sHtml +=		'<br><br><input type=\"radio\" id =\"rbnManoIzquierda\" name=\"rbnManoIzquierda\" value=\"002\" >No Tiene Mano Derecha.';
	sHtml +=	'</td  >';
	sHtml +='</tr>';
	$("#soloManoIzquierda").empty();
	$("#soloManoIzquierda").html(sHtml);
	$('#soloManoIzquierda').dialog('open');
}

//Funcion que muestra el dialogo para sin manos
function mostraDialogoSinManos()
{
	$('.content-loader').hide();
	var sHtml = '';
	sHtml +='<script type=\"text/javascript\">';
	sHtml +='	$( "#sinManos" ).dialog({ closeOnEscape: false });';
	sHtml +='</script >';
	sHtml += '<tr>';
	sHtml +=	'<td  >';
	sHtml +=		'<br><input type=\"radio\"  id =\"rbnsinmano\" name=\"rbnsinmano\" value=\"001\" checked> Manos Amputadas.';
	sHtml +=		'<br><br><input type=\"radio\" id =\"rbnsinmano\" name=\"rbnsinmano\" value=\"004\" >Manos Lesionadas.';
	sHtml +=	'</td  >';
	sHtml +='</tr>';

	$("#sinManos").empty();
	$("#sinManos").html(sHtml);
	$('#sinManos').dialog('open');
}

function limpiar()
{
var VERIFICACION_MORPHO = 0;
var MANO_DERECHA = 1;
var MANO_IZQUIERDA = 2;
var VERIFICACION_EMPLEADO = 3;
var CAPTURA_FIRMA = 4;
var PULGARES =  5;
var GUARDAR = 6;
var GUARDAR_IZQUIERDA = 7;
var VALIDAR_OPCION = 8;
var AMBAS_MANOS = 9;
var VALIDAR_NIST = 10;
var FALTAN_DEDOS = 11;
var iOpcion = 0;
var iEliminaCont = 1;
var iSelecciono = 0;
OBJ = '';
contadorNIST = 0;

sCodigo1 = '';
sCodigo2 = '';
sCodigo3 = '';
sCodigo4 = '';
sCodigo5 = '';
sCodigo6 = '';
sCodigo7 = '';
sCodigo8 = '';
sCodigo9 = '';
sCodigo10 = '';
iBandDed1 = 0;
iBandDed2 = 0;
iBandDed3 = 0;
iBandDed4 = 0;
iBandDed5 = 0;
iBandDed6 = 0;
iBandDed7 = 0;
iBandDed8 = 0;
iBandDed9 = 0;
iBandDed10 = 0;
}

function actualizarEnrolamiento(iFolio)
{
	var iRetorno = 0;
    var cadena = '';
	cadena = "opcion=13" + "&folio=" + iFolio ;
	$.ajax(
		{
			async: false,
			cache: false,
			data:cadena,
			url: 'php/capturaenrolamientotrabajador.php',
			type: 'POST',
			dataType: 'json',
			success: function(data)
			{
				datos = data;
				iRetorno = datos.regreso;

			},
			error: function(a, b, c)
			{

			},
			beforeSend: function()
			{

			}
		}
	);
	return iRetorno;
}

/*
Metodo para excepcionar las huellas, obtienen los dedos donde exista un template para validar las huellas legibles
y el nist en base a la tabla establecida por procesar, en caso de que no cumpla el nist requerido en dicha tabla,
se validara en base al registro inferior y por ende se agregaran excepciones a los dedos que sean necesarios.
*/

function excepcionarDedosEnRolTrabajador(iFolioEnRol)
{
	var iRespuesta = 0;
	var mensaje = '';
	var cadena = '';
	var sNumEnrolador = iEmpleado;
	cadena = "opcion=15&folioenrol=" + iFolioEnRol;
	sMensajeAux = '[capturaenrolamientotrabajador::excepcionarDedosEnRolTrabajador] --> Cadena Parametros: ' + cadena;
	grabarLogJs(sMensajeAux);
	$.ajax(
		{
			async: false,
			cache: false,
			data:cadena,
			url: 'php/capturaenrolamientotrabajador.php',
			type: 'POST',
			dataType: 'json',
			success: function(data)
			{
			 	//pasa el arreglo data a datos, para su posterior uso en la creacion de opciones
				datos = data;
				iRespuesta = datos.resulta;

				sMensajeAux = '[capturaenrolamientotrabajador::excepcionarDedosEnRolTrabajador] --> Respuesta: ' + iRespuesta;
				grabarLogJs(sMensajeAux);
				
				if (iRespuesta == 1)
				{  //mandar llamar firma gerente  solo mano derecha E IZQUIERDA
					iExcepcionesHuellas = 1;
					if( iSelecciono == 1 )
					{
						obtenermensajegerente(5);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;

						//PDTE 961: Tabla control de los params de ejecucion.
						cParametrosEnvio = sRutaHE + ' ' + sParametrox;
						guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
						////cParametrosEnvio='';

						ejecutaWebService(sRutaHE, sParametrox);
					}
					else{
						//Aqui mando el mensaje solicitando la 11 Huella
						//mensaje	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
						//mensajeHuellaVerificacion(mensaje);
						huellaDeVerificacion();
					}
				}
				else
				{
					//Ocurrio un error al ejecutar la funcion
					//Pedir de nuevo
					mensaje = 'Promotor: Se present\u00F3 un problema al capturar las huellas del Enrolamiento, Favor de intentar de nuevo.';
					mensajeCalidadNIST(mensaje);
				}
			},
			error: function(a, b, c)
			{
				//desbloquearUI();
			},
			beforeSend: function()
			{
			//	bloquearUI("Espere un momento por favor...");
			}
		}
	);
}

// METODO PARA OBTENER LOS DATOS PARAMETRIZABLES
function ObtenerDatosConfiguracion() // MARIO LARA
{
	$.ajax(
	{
		async: false,
		cache: false,
		data:{
			opcion: 18,
		},
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(respuesta)
		{
			//arrResp = eval(respuesta);
			arrResp = respuesta;

			iMinutos = arrResp.iTotalMinutos;
			iDias = arrResp.iTotalDias;
			iIntentos = arrResp.iTotalIntentos;
		},
		error: function (a, b, c)
		{
			alert("error ajax 3" + a + " " + b + " " + c);
		}
	});
}

function llamarMarcaEI(iEmpleado, cFolio)
{
	var iRetorno = 0;
    var cadena = '';
	cadena = "opcion=20" + "&folioservicio=" + cFolio + "&empleado=" + iEmpleado;
	$.ajax(
		{
			async: false,
			cache: false,
			data:cadena,
			 url: 'php/capturaenrolamientotrabajador.php',
			 type: 'POST',
			 dataType: 'json',
			 success: function(data)
			{
				//datos = data;
				//iRetorno = datos.regreso;
				iRetorno = 1;
			},
			error: function(a, b, c)
			{

			},
			beforeSend: function()
			{

			}
		}
	);
	return iRetorno;
}

function ValidarLevantamientoEI()
{
	//bloquearModal();
		$.ajax({
		url: "php/capturaenrolamientotrabajador.php",
		type: "POST",
		async: false,
		cache: false,
		dataType: "json",
		data: {
			opcion: 21,
			folioservicio:iFolioSolicitud,
			curp:chCurp
		},
		success: function(datosrespei) {
			grabarLogJs("Se obtiene respuesta");
			datosEi = datosrespei;
			grabarLogJs("Se asigno respuesta");
			iValExpIde = datosEi.tieneExp; // 1 no levanta pagina mostrar mensaje 6 dias
			console.log("VALOR DE VARIABLE iValExpIde: " + iValExpIde);
			grabarLogJs("VALOR DE VARIABLE iValExpIde: " + iValExpIde);
			//desbloquearModal();
		},
		error: function(a, b, d) {
		//desbloquearModal();
			alert("error ajax " + a + " " + b + " " + d);
		}
	});
}

//inicio peticion 316 - cambio a peticion del cliente Mario Guzman
//Cambio -> pedirle la huella al gerente titular de la tienda cuando se seleccione una opcion de manos dispacacitadas
//			ademas de que le pedira la toma fotografica de las manos mencionadas

function fnMostrarMensajeGerente(mensaje)
{
	$('.content-loader').hide();
	var sHtml = '';
	sHtml +='<script type=\"text/javascript\">';
	sHtml +='	$( "#divMensajeGerente" ).dialog({ closeOnEscape: false });';
	sHtml +='</script >';
	sHtml += '<tr>';
	sHtml +=	'<td>' + mensaje + '</td> ';
	sHtml +=	'<br></br>';
	sHtml +='</tr>';
	$("#divMensajeGerente").empty();
	$("#divMensajeGerente").html(sHtml);
	$('#divMensajeGerente').dialog('open');
}

function fnobteneripservidor()
{
	$.ajax({
		async   : false,
		cache   : false,
		url     : 'php/capturaenrolamientotrabajador.php',
		type    : 'POST',
		dataType: 'JSON',
		data    : {
			'opcion':22
		},
		success : function(datos){
			servidorIP= datos;
		}
	});
}

function fnvalidarnumempleado(iNumEmpleado)
{
	$.ajax({
		async   : false,
		cache   : false,
		url     : 'php/capturaenrolamientotrabajador.php',
		type    : 'POST',
		dataType: 'JSON',
		data    : {
			'mensaje': 'FUNCION PARA VALIDAR EL NUMERO DE EMPLEADO',
			'opcion': 23, //validacion de numero de empleado para ver si es gerente Titular
			'empleado': iNumEmpleado
		},
		success : function(datos){
			if( datos.status == OK___ )
			{
				if( datos.respuesta == 1 )
				{
					if( iEmpleado == iNumEmpSensor )
					{
						mostrarMensajeSensor('<b>GERENTE:</b> no es v&aacute;lida tu autorizaci&oacute;n en el enrolamiento que t&uacute; mismo est&aacute;s realizando. Favor de pedir autorizaci&oacute;n a otro Gerente Titular.');
					}
					else{
						iAutorizacionGerencial = 1;
						$('#divMensajeGerente').dialog("close");
						if( iexcepcion == 0 )
						{	//significa que tuvo excepcion en todas las huellas al momento de enrolarse una persona con ambas manos normales ( sin excepcion en las manos)
							fnobtenerfechaactual();
							iOpcion = CAPTURAR_FIRMA_GERENTE;
							if( smTipoSolicitud == 2040 || smTipoSolicitud == 2022 )
							{	sParametros = "1 1 " + "SP00_"+ iNumEmpSensor + iFolioSolicitud + '' + dFechaFirma + "-S_FGTE";	}
							else
							{	sParametros = "1 1 " + "SP00_"+ iNumEmpSensor + iFolioSolicitud + '' + dFechaFirma + "_FGTE";	}
							ejecutaWebService(cRutaFirma, sParametros);
						}
						else{
							//se manda llamar el componente de la camara
							fnobtenerclaveoperacion();
						}
					}
				}
				else{
					mostrarMensajeSensor('No cuentas con el puesto autorizado, favor de pedir autorizaci&oacute;n a un Gerente Titular v&aacute;lido.');
				}
			}
			else
			{
				mostrarMensajeGenerico(datos.descripcion);
			}
		}
	});
}

function mostrarMensajeGenerico(msj)
{
	$('.content-loader').hide();
	$("#divMensaje3").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+msj+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$(this).dialog( "close" );
			},
		}
	});
}

function mostrarMensajeSensor(msj)
{	
	$('.content-loader').hide();
	//metodo que manda mensaje a la pantalla y cuando se presiona aceptar ejecuta el he.Exe
	$("#divMensaje3").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+msj+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$(this).dialog( "close" );
				iOpcion = VALIDACION_HUELLA_GERENTE;
				// ejecutarApplet(ipServidor, sRutaHE);
				if(OSName == "Android"){
					ejecutaWebService(2, 2);
				}else{
					ejecutaWebService(sRutaHE, ipServidor);
				}
			},
		}
	});
}

function mostrarMensajeSalirCamara(sMensaje)
{	
	$('.content-loader').hide();
	//mensaje que saldra cuando le den clic al boton salir del applet de la camara
	$("#divPreguntaSalirCamara").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+sMensaje+"</p>");
		},
		buttons:
		{
			"Si" : function()
			{
				$(this).dialog( "close" );
				mostraDialogoHuellas();
			},
			"No" : function()
			{
				fnobtenerclaveoperacion();
			/*	iOpcion = TOMAR_FOTO_CLIENTE;
				sParametros = 'f fotocliente.tif';  //el nombre de la foto va a cambiar
				ejecutarApplet(sParametros, cRutaCamara); */
				$(this).dialog("close");
			}
		},
	});
}

function obtenermensajegerente(opcion)
{	//funcion que obtiene el mensaje para el gerente correspondiente a la excepcion seleccionada en el menu huellas
	$.ajax({
		async   : false,
		cache   : false,
		url     : 'php/capturaenrolamientotrabajador.php',
		type    : 'POST',
		dataType: 'JSON',
		data    : {
			'opcion': 24,
			'opcionmensaje': opcion
		},
		success : function(datos){
			if( datos.status == OK___)
			{
				if(datos.datosmensaje.idmensaje > 0)
				{
					if( (opcion == 6 || opcion == 7 ) && (smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33) ) // mensaje leyenda manos amputadas
					{
						guardarbitacoraenrolamiento();
						//mensajeFin = 'Se ha concluido con el enrolamiento, de clic en el boton aceptar para finalizar.';
						//mensajeFinal(mensajeFin); 790
						cerrarNavegador();
					}
					else if( opcion == 6 || opcion == 7 ) // mensaje leyenda manos amputadas
					{
						mostrarSolicitarLeyendaPromotor(datos.datosmensaje.mensaje);
					}
					else if( opcion == 8)
					{
						mostrarSolicitarFirma(datos.datosmensaje.mensaje);
					}
					else{
						fnMostrarMensajeGerente(datos.datosmensaje.mensaje);
					}
				}
				else{
					mostrarMensajeCancelar('Ocurrio un error al intentar obtener datos internos, favor de volver a iniciar el enrolamiento');
				}
			}
			else{
				mostrarMensajeCancelar('Ocurrio un error al intentar obtener datos internos, favor de volver a iniciar el enrolamiento');
			}
		}
	});
}

function guardarbitacoraenrolamiento()
{
	$('.content-loader').hide();
	grabarLogJs('[guardarbitacoraenrolamiento]Entro a guardar bitacora de autorizacion de enrolamiento por gerente  FOLIO -> ' + iFolio);
	$.ajax({
		async   : false,
		cache   : false,
		url     : 'php/capturaenrolamientotrabajador.php',
		type    : 'POST',
		dataType: 'JSON',
		data    : {
			'opcion': 25,
			'foliosolicitud': iFolioSolicitud,
			'folioenrol': iFolio,
			// el numero de tienda se sacara en el php
			'empleado': iEmpleado,
			'numempgerente': iNumEmpSensor,
			'curp': chCurp,
			'autorizaciongerencial': iAutorizacionGerencial,
			'tipoenrolamiento': iexcepcion,
			'excepcioneshuellas': iExcepcionesHuellas
		},
		success : function(datos) {
			if( datos.status == OK___ ) {
				console.log('se registro en la bitacora exitosamente');
				grabarLogJs('SE GUARDO EN LA BITACORA CORRECTAMENTE FOLIO ENROLAMIENTO -> ' + iFolio);
			}
			else{
				grabarLogJs('OCURRIO UN ERROR AL INTENTAR GUARDAR EN LA BITACORA DEL ENROLAMIENTO, FOLIO ENROLAMIENTO -> ' + iFolio);
			}
		}
	});
}

function mostrarMensajeErrorHuella(msj)
{	
	$('.content-loader').hide();
	//metodo que manda mensaje a la pantalla y cuando se presiona aceptar ejecuta el he.Exe
	$("#divMensaje3").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+msj+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$(this).dialog( "close" );
				if( iexcepcion != 0 )
				{	obtenermensajegerente(iexcepcion);	}
				else
				{	obtenermensajegerente(5);	}
				iOpcion = VALIDACION_HUELLA_GERENTE;
				// ejecutarApplet(ipServidor, sRutaHE);
				ejecutaWebService(sRutaHE, ipServidor);
			},
		}
	});
}

function mostrarDialogoNumeroGerente()
{	
	$('.content-loader').hide();
	//metodo que manda mensaje a la pantalla y cuando se presiona aceptar ejecuta el he.Exe
	$("#divMensaje3").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<center><br><input type='text' id='numGerente' maxlength='8'></center>");
			$("#numGerente").keypress(function(event){return validarKeypressSoloNumero(event); });
		},
		buttons:
		{
			"Aceptar" : function()
			{
				iNumEmpSensor = document.getElementById('numGerente').value;
				if( iNumEmpSensor.length == 8 )
				{
					if( intentosSensor == 6 )
					{
						$('#divMensaje3').dialog("close");
						$('#divMensajeGerente').dialog("close");
						iAutorizacionGerencial = 0;
						fnobtenerclaveoperacion();
						$('#divMensaje3').dialog("close");
					}
					else
					{
						$('#divMensaje3').dialog("close");
						iOpcion = VALIDACION_HUELLA_GERENTE;
						// ejecutarApplet(servidorIP, sRutaHE);
						ejecutaWebService(sRutaHE, ipServidor);
					}
				}
				else
				{
					$('#divMensaje3').dialog("close");
					mostrarmensajeNumEmp('GERENTE: Favor de proporcionar su n&uacute;mero de empleado.');
				}
			},
		}
	});
}

function validarKeypressSoloNumero(event)
{//funcion que valida que solo se presionen numeros sobre el campo solicitado
	if(!(event.which >= 48 && event.which <= 58) && event.which!=8 && event.which!=0)event.preventDefault();
}

function mostrarmensajeNumEmp(msj, funcion)
{
	$('.content-loader').hide();
	$("#divMensaje3").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+msj+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$(this).dialog( "close" );
				mostrarDialogoNumeroGerente();
			},
		}
	});
}

function fnobtenerclaveoperacion()
{
	if( smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33 )
	{
		$.ajax({
			async   : false,
			cache   : false,
			url     : 'php/capturaenrolamientotrabajador.php',
			type    : 'POST',
			dataType: 'JSON',
			data    : {
				'opcion': OBTENER_CLAVE_OPERACION,
				'foliosolicitud': iFolioSolicitud
			},
			success : function(datos){
				if( datos.status == OK___ )
				{
					iclaveoperacion = datos.claveoperacion;
					switch ( iclaveoperacion.toString().length ) {
						case 3:
							iclaveoperacion = '000'+iclaveoperacion;
						break;
						case 4:
							iclaveoperacion = '00'+iclaveoperacion;
						break;
						case 5:
							iclaveoperacion = '0'+iclaveoperacion;
						break;
						default:
						break;
					}
				}
				else{
					mostrarMensajeCancelar('Ocurrio un error al intentar obtener la clave de operaci&oacute;n, favor de volver a iniciar el enrolamiento');
				}
			}
		});
	}
	else
	{
		f_obtenerclaveoperacionservicios();
	}
	if( fnobtenerfechaactual() )
	{
		iOpcion = TOMAR_FOTO_CLIENTE;
		if( smTipoSolicitud == 2040 || smTipoSolicitud == 2022 )
		{
			sParametros = 'f '+ iFolioSolicitud + '-S_'+ chCurp + '' + iclaveoperacionservicio + '' + dFechaactual +'FTE0.tif';
		}
		else
		{
			sParametros = 'f '+ iFolioSolicitud + '_'+ chCurp + '' + iclaveoperacion + '' + dFechaactual +'FTE0.tif';
		}
		// ejecutarApplet(sParametros, cRutaCamara);
		ejecutaWebService(cRutaCamara, sParametros);
	}
}

function fnobtenerfechaactual()
{
	var bandera = false;
	$.ajax({
		async   : false,
		cache   : false,
		url     : 'php/capturaenrolamientotrabajador.php',
		type    : 'POST',
		dataType: 'JSON',
		data    : {
			'opcion': 27
		},
		success : function(datos){
			if( datos.status == OK___ )
			{
				dFechaactual = datos.fecha;
				dFechaFirma = datos.fechafirma;
				bandera = true;
			}
		}
	});
	return bandera;
}

function f_obtenerclaveoperacionservicios()
{
	$.ajax({
		async   : false,
		cache   : false,
		url     : 'php/capturaenrolamientotrabajador.php',
		type    : 'POST',
		dataType: 'JSON',
		data    : {
			'opcion': OBTENER_CLAVE_OPERACION_SERVICIO,
			'foliosolicitud': iFolioSolicitud
		},
		success : function(datos){
			if( datos.status == OK___ )
			{
				iclaveoperacionservicio = datos.claveoperacion;
				switch ( iclaveoperacionservicio.toString().length ) {
					case 3:
						iclaveoperacionservicio = '000'+iclaveoperacionservicio;
					break;
					case 4:
						iclaveoperacionservicio = '00'+iclaveoperacionservicio;
					break;
					case 5:
						iclaveoperacionservicio = '0'+iclaveoperacionservicio;
					break;
					default:
					break;
				}
			}
			else
			{
				mostrarMensajeCancelar('Ocurrio un error al intentar obtener la clave de operaci&oacute;n, favor de volver a iniciar el enrolamiento');
			}
		}
	});
}

function mostrarMensajeCancelarHuellaGerente(sMensaje)
{
	$('.content-loader').hide();
	//mensaje que saldra cuando le den clic al boton salir del applet de la camara
	$("#divPreguntaSalirCamara").dialog
	({
		title: 'Enrolamiento Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+sMensaje+"</p>");
		},
		buttons:
			{
				"Si": function () {
					$('#divMensajeGerente').dialog("close");
					if (smTipoSolicitud == 33 || smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 1 || smTipoSolicitud == 527) {
						localStorage.removeItem("contador");
						window.location.href='http://'+ servidorIP +'/constanciasafiliacion/indexConstanciasafiliacion.html?empleado=' + iEmpleado,true;
					}else{
					mostraDialogoHuellas();
					}
				},
			"No" : function()
			{
				$( this ).dialog("close");
				iOpcion = VALIDACION_HUELLA_GERENTE;
				// ejecutarApplet(servidorIP, sRutaHE);
				ejecutaWebService(sRutaHE, servidorIP);
			}
		},
	});
}

function mostrarSolicitarFirmaPROMOTOR(mensajefirma)
{
  $('.content-loader').hide();
  $("#divMensaje").dialog
	({
		title: 'Firma Digital del Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajefirma+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				//Para probar mandare un mensaje con la solicitud de la firma
				iOpcion = CAPTURA_FIRMA;
				if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
				{
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "_FTRAB";
				}
				else
				{
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "-S_FTRAB";
				}
				ruta =	"C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";
				//ejecutarApplet(sParametros,ruta);
				ejecutaWebService(ruta, sParametros);
				$(this).dialog( "close" );
			},
		}
	});
}

function mostrarSolicitarLeyendaPromotor(mensajefirma)
{
	$('.content-loader').hide();
  $("#divMensaje").dialog
	({
		title: 'Firma Digital del Trabajador',
		autoOpen: true,
		resizable: false,
		width: '350',
		height: 'auto',
		modal: true,
		closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo
		position:  {my: "center",at: "center",of: $("body"),within: $("body")},//Posicionar el dialog en el centro
		open: function(event, ui)
		{
			$(".ui-dialog-titlebar-close", ui.dialog).hide();
			$(this).html("<p>"+mensajefirma+"</p>");
		},
		buttons:
		{
			"Aceptar" : function()
			{
				$(this).dialog( "close" );

				iOpcion = CAPTURA_LEYENDA_EXCEPCION;

				if(smTipoSolicitud == 26 || smTipoSolicitud == 27 || smTipoSolicitud == 33)
				{	
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "_LTRAB";	
				}
				else
				{	
					sParametros = "1 1 " + "FAHD_" + iFolioSolicitud + "-S_LTRAB";	
				}
				
				ruta =	"C:\\sys\\signpad\\CAPTURAFIRMAAFORE.EXE";

				//PDTE 961: Tabla control de los params de ejecucion.
				cParametrosEnvio = ruta + ' ' + sParametros;
				guardarParametrosControl(cParametrosEnvio, iFolio, 0, 1);
				////cParametrosEnvio='';

				ejecutaWebService(ruta, sParametros);
			},
		}
	});
}

/* Funcion que recibe la respuesta del componente EnrolMovil */
/*function getComponenteHuella(estatusProceso, jsonFingers, opcionHuella){
	if(opcionHuella==1){
		// RECIBE LA RESPUESTA DEL ENROLLMOVIL
		document.getElementById("FingersAmbasManos").value = jsonFingers;

		iOpcion = GUARDAR;
		respuestaWebService(1);
	}
}*/

function getComponenteHuellasCapturadas(estatusProceso, opcionHuella, fEnrolamiento){
	if(opcionHuella==1 && estatusProceso==1){ /* Si son ambas manos y se guardaron correctamente */
		// RECIBE LA RESPUESTA DEL ENROLLMOVIL

		iFolio = fEnrolamiento;
		guardarbitacoraenrolamiento();
		//mensajeFin = 'Se ha concluido con el enrolamiento, de clic en el boton aceptar para finalizar.';
		//mensajeFinalAndroid(mensajeFin);
		Android.terminarEnrolamiento("1");
	}
	
	if(opcionHuella==1 && estatusProceso==0){ /* Si son ambas manos y no se guardaron correctamente */
		// RECIBE LA RESPUESTA DEL ENROLLMOVIL
		$('.content-loader').hide();
		mensajeFin = 'Ocurrio un error al finalizar el enrolamiento, favor de intentar de nuevo.';
		mensajeFinalAndroidError(mensajeFin);
	}
}

// MOVIL
/* Obtener parametros get desde el webview android */
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function guardarParametrosControl(parametros,folioenrola,response,opcionfun){

	$.ajax({
		async   	: false,
		cache   	: false,
		url     	: 'php/capturaenrolamientotrabajador.php',
		type    	: 'POST',
		dataType	: 'JSON',
		data    	: {
			"opcion"          : "30",
			"iopcion"         : opcionfun,
			"parametrosenvio" : parametros,
			"response"        : response,
			"folioenrol"      : folioenrola
		},
		success : function(datos){
			if( datos.respuesta == OK___ ){
				//console.log('Se guardo ejecución en tabla control');
			}
			else{
				console.log('Hubo un error al guardar en tabla control');
			}
		}
	});

}

function verificaconsecutivoeibio(iFolioSolicitud)
{
	grabarLogJs("INICIA SEGUNDA VERIFICACION MARCA CUENTA");
	
	$.ajax({
		async: false,
		cache: false,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'JSON',
		data: {'opcion':32,'folioservicio':iFolioSolicitud},
		success: function(respuesta)
 	  	{
 	  	 	if (respuesta == 1)
 	  	 	{
				grabarLogJs("CUENTA MARCADA CORRECTAMENTE ["+iFolioSolicitud+"]");
 	  	 		//bMarca = true;
 	  	 	}
 	  	 	else if (respuesta == 0)
 	  	 	{
				grabarLogJs("CUENTA SIN MARCA ["+iFolioSolicitud+"]");
				grabarLogJs("INCIA MARCAR CUENTA ["+iFolioSolicitud+"]");
				llamarMarcaEI(iEmpleado, iFolioSolicitud);
				grabarLogJs("FINALIZA MARCAR CUENTA");
 	  	 		//bMarca = false;
 	  	 	}
			else if (respuesta == -1)
			{
				grabarLogJs("FOLIO INCOMPLETO ["+iFolioSolicitud+"]");
			}
 	  	},
		error: function (xhr, ajaxOptions, thrownError) 
 	  	{	
 	  		return false;
 	  	}
	});
	grabarLogJs("FINALIZA SEGUNDA VERIFICACION MARCA CUENTA");
}


function ObtenerExcepcion2(chCurp)	//790
{
	grabarLogJs("Entro a la funcion Obtener la excepcion del trabajador");
	var cadena = '';
	cadena = "opcion=31&chCurp=" + chCurp ;
	$.ajax({
		async: false,
		cache: false,
		data:cadena,
		url: 'php/capturaenrolamientotrabajador.php',
		type: 'POST',
		dataType: 'json',
		success: function(data)
		{
				datos = data;
				excepcion = datos.excepcion;
				iSelecciono = 0;
				iSelecMano = 0;
				mensajeLog='Valor de la excepcion del trabajador: '+excepcion;

				if (excepcion == 26) {
					excepcion = 1;
				} else if (excepcion == 27) {
					excepcion = 4;
				} else if (excepcion == 33) {
					excepcion = 5;
				}


				iSelecciono = 0;
				iSelecMano = 0;
				iSelecciono = excepcion;//$('input:radio[name=rbnHuella]:checked').val();
				iSelecMano = iSelecciono;
				//Pasa a variable Global para validar solo la mano derecha o solo mano izquierda.
				iSeleccionoMano = iSelecciono;
				if (iSelecciono == 1) //Ambas manos
				{
					mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS = ' + iSelecciono;
					grabarLogJs(mensajeLog); //jcla
					iexcepcion = 0;
					iOpcion = 1;
					reAmbas = 3;
					sParametrox = "2 0 1 3 " + iFolio;
					//ejecutarApplet(sParametrox,sRutaM); //abrimos la mano derecha ya que selecciono las dos manos
					if (OSName == "Android") {
						ejecutaWebService(1, 1);
					} else {
						ejecutaWebService(sRutaM, sParametrox);
					}
					$(this).dialog("close");
				}
				else if (iSelecciono == 2) //Solo mano Derecha
				{
					//Manda llamar el dialogo correspondiente a la opcion Solo mano Derecha.
					mensajeLog = 'PROMOTOR SELECCIONO SOLO MANO DERECHA = ' + iSelecciono;
					grabarLogJs(mensajeLog);//JCLA
					reDerecha = 1;
					mostraDialogoHuellasDerecha();
					$(this).dialog("close");
				}
				else if (iSelecciono == 3)//solo mano izquierda
				{
					mensajeLog = 'PROMOTOR SELECCIONO SOLO MANO IZQUIERDA = ' + iSelecciono;
					grabarLogJs(mensajeLog);//JCLA
					reIzquierda = 2;
					mostraDialogoHuellasIzquierda();
					$(this).dialog("close");
				}

				// else if (iSelecciono == 4)//sin manos
				// {
				//     mostraDialogoSinManos();
				// 	$(this).dialog("close");
				// }

				else if (iSelecciono == 4) //ambas manos amputadas
				{
					mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS AMPUTADAS = ' + iSelecciono;
					grabarLogJs(mensajeLog);//JCLA
					guardarExcepciones(iFolio, '001', 0);
					//$(this).dialog("close");
					iexcepcion = 4;

					//ejecutarApplet(sParametrox,sRutaHE);
					if (OSName == "Android") {
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
					} else {
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
						ejecutaWebService(sRutaHE, sParametrox);
					}
					/*
					mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
					mensajeHuellaVerificacion(mensajeVerificacion);
					*/
				}
				else if (iSelecciono == 5) //ambas manos lesionadas
				{
					mensajeLog = 'PROMOTOR SELECCIONO AMBAS MANOS LESIONADAS = ' + iSelecciono;
					grabarLogJs(mensajeLog);//JCLA
					guardarExcepciones(iFolio, '004', 0);
					//$(this).dialog("close");
					iexcepcion = 3;

					// ejecutarApplet(sParametrox,sRutaHE);
					if (OSName == "Android") {
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
					} else {
						obtenermensajegerente(iexcepcion);
						iOpcion = VALIDACION_HUELLA_GERENTE;
						sParametrox = servidorIP;
						ejecutaWebService(sRutaHE, sParametrox);
					}
					/*
					mensajeVerificacion	= 'Promotor: Favor de colocar tu huella del dedo \u00EDndice en el dispositivo Morpho.';
					mensajeHuellaVerificacion(mensajeVerificacion);
					*/
				}	

		},
			error: function (a, b, c) {
				//desbloquearModal();
				//console.log("data: "+data);
				alert("error ajax " + a + " " + b + " " + c);
			},
			beforeSend: function () {
			}
		}
	);
	
}















