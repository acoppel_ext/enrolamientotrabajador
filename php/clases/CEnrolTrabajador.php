<?php
include_once ('global.php');
include_once ('CMetodoGeneral.php');
include_once ('CLogImpresion.php');
include_once("JSON.php");

class CEnrolTrabajador
{
	//funcion que valida el Estatus del promotor.
	public static function validarEstadoPromotor($iEmpleado)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = " select fnvalidarenroltrabajadores as valor from fnvalidarenroltrabajadores ($iEmpleado); ";	
				
				CLogImpresion::escribirLog('Funcion que valida el estatus del promotor: '.$cSql);
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet as $reg)
					{	
						$arrDatos['valor'] = $reg['valor'];
						CLogImpresion::escribirLog('Respuesta: '.$reg['valor']);
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	
	}
	
	public static function borrarExcepciones($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = " select fnborrarexcepcionesenroltrab as borro from fnborrarexcepcionesenroltrab($iFolio); ";	
				CLogImpresion::escribirLog('Funcion para borrar Excepciones: '.$cSql);
				
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet as $reg)
					{	
						$arrDatos['borro'] = $reg['borro'];
						CLogImpresion::escribirLog('Respuesta: '.$reg['borro']);
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	
	}
	
	public static function actualizarEstatusEnrol($iFolio,$iEstatusCancelar)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = " select fnactestatusenrolatrabajador as actualizo from fnactestatusenrolatrabajador($iFolio,$iEstatusCancelar); ";			
				CLogImpresion::escribirLog('Funcion que actualiza el estatus de Enrolamiento: '.$cSql);
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet as $reg)
					{	
						$arrDatos['actualizo'] = $reg['actualizo'];
						CLogImpresion::escribirLog('Respuesta: '.$reg['actualizo']);
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	
	}
	
	public static function guardarDatos($iEmpleado,$solicitud,$curp,$iFolioSolicitud)
	{
	   //crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd2)
			{
				CLogImpresion::escribirLog("Entro a guardar la los Datos: ");
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql2 = "select fnguardarenrolamientotrabajador as ires from fnguardarenrolamientotrabajador($iEmpleado,$solicitud,'$curp',$iFolioSolicitud);";
				CLogImpresion::escribirLog($cSql2);
				//Ejecuta la consulta
				$resulSet2 = $cnxBd2->query($cSql2);
				//Verifica que se haya ejecutado correctamente
				if($resulSet2)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					//$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet2 as $reg2)
					{	
						$arrDatos['ires'] = $reg2['ires'];
						CLogImpresion::escribirLog("Se Ejecuto la Funcion Correctamente");
				    	CLogImpresion::escribirLog('Respuesta: '.$arrDatos['ires']);
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql2);
				}
				//cierra la conexion a base de datos
				$cnxBd2 = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	public static function cancelarEnrolamiento($iFolio,$estatus)
	{
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd2)
			{
				CLogImpresion::escribirLog("Entro a la Funcion Cancelar Enrolamiento");
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql2 = "select fnactestatusenrolatrabajador as val from fnactestatusenrolatrabajador($iFolio,$estatus);";
				CLogImpresion::escribirLog($cSql2);
				//Ejecuta la consulta
				$resulSet2 = $cnxBd2->query($cSql2);
				//Verifica que se haya ejecutado correctamente
				if($resulSet2)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					//$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet2 as $reg2)
					{	
						$arrDatos['val'] = $reg2['val'];
						CLogImpresion::escribirLog("Se ejecuto correctamente la funcion");
				    	CLogImpresion::escribirLog('Respuesta: '.$arrDatos['val']);	
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql2);
				}
				//cierra la conexion a base de datos
				$cnxBd2 = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	public static function validaNIST($iFolioEnrol)
	{
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd2)
			{
				CLogImpresion::escribirLog("Entro a la Funcion de validar NIST");
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql2 = "select regresa as resulta from fnvalidarnisttrabajador($iFolioEnrol);";
				CLogImpresion::escribirLog($cSql2);
				//Ejecuta la consulta
				$resulSet2 = $cnxBd2->query($cSql2);
				//Verifica que se haya ejecutado correctamente
				if($resulSet2)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					//$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet2 as $reg2)
					{	
						$arrDatos['resulta'] = $reg2['resulta'];
						CLogImpresion::escribirLog("Se ejecuto correctamente la funcion");
				    	CLogImpresion::escribirLog('Respuesta: '.$arrDatos['resulta']);	
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql2);
				}
				//cierra la conexion a base de datos
				$cnxBd2 = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	public static function consultarDedosEnManos($iFolio,$iSelecciono)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();

		//declara un arreglo
		$arrDatos = array();
		
		$iCont = 0;
		
		$arrDatos['estatus'] = _DEFAULT_;
		
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = "select numdedo,trim(nombrededo) as nombrededo from fnvalidardedosenroltrabajador($iFolio,$iSelecciono);"; 
				CLogImpresion::escribirLog("Funcion consultar dedos en manos: ".$cSql);
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $cSql);
				
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);

				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					
					$arrDatos['descripcion'] = "EXITO";

					foreach($resulSet as $reg)
					{	
						$iCont++;
						$arrDatos['total'] = $iCont;
						$arrDatos['respuesta'][$iCont] = OK___;
					//	$arrDatos['empleado'][$iCont] = $reg['empleado'];
					//	$arrDatos['imagen'][$iCont] = $reg['imagen'];
						$arrDatos['numdedo'][$iCont] = $reg['numdedo'];
						$arrDatos['nombrededo'][$iCont] = utf8_encode($reg['nombrededo']);
						
						$mensaje = 'Dedo Faltante->'.utf8_encode($reg['nombrededo']);
						$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
					}
					
				}
				else
				{
					// Si existe un error en la consulta mostrar� el siguiente mensaje 
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					//throw new Exception("Ccapturaenrolamiento.php\tconsultarDedosEnManos"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
				//$mensaje = "\tCcapturaenrolamiento.php\tconsultarDedosEnManos"."\tNo pudo abrir la conexion a PostGreSQL (aforeglobal)";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "No abrio Conexion a PostGreSQL";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	public static function guardaExcepciones($iFolio,$chExcepcion,$idedos)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd2)
			{
				CLogImpresion::escribirLog("Entro a la Funcion Guardar Excepciones");
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql2 = "select fngrabarenrolexcepcionestrabajador as ires from fngrabarenrolexcepcionestrabajador($iFolio,'$chExcepcion',$idedos);";
				CLogImpresion::escribirLog($cSql2);
				//Ejecuta la consulta
				$resulSet2 = $cnxBd2->query($cSql2);
				//Verifica que se haya ejecutado correctamente
				if($resulSet2)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					//$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet2 as $reg2)
					{	
						$arrDatos['ires'] = $reg2['ires'];
						CLogImpresion::escribirLog("Se Ejecuto correctamente la Funcion");
				    	CLogImpresion::escribirLog('Respuesta: '.$arrDatos['ires']);	
					}
				}
				else
				{
				}
				//cierra la conexion a base de datos
				$cnxBd2 = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	//Funcion que permite obtener la liga del formato de enrolamiento segun los parametros
	public static function obtenerLigaFormatoEnrolamiento($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		$ipTienda = '';
		$iOpcion = 6;
		$iTestigo = 0;
		$iNumTestigo = 0;
		//declara un arreglo
		$arrDatos = array();
		
		$arrDatos['estatus'] = _DEFAULT_;
		
		$ipTienda = CEnrolTrabajador::obtIpTienda();

		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar			
				$cSql = " SELECT trim(fnobtenerligapaginaenrolamiento) as dato FROM fnobtenerligapaginaenrolamiento( $iOpcion,$iFolio,$iTestigo,$iNumTestigo,'$ipTienda');";
				CLogImpresion::escribirLog("Funcion para obtener la liga del formato: ".$cSql);
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					
					$arrDatos['descripcion'] = "EXITO";

					foreach($resulSet as $reg)
					{			
						$arrDatos['respuesta'] = 1;
						$arrDatos['dato'] = $reg['dato'];
						$arrDatos['estatus'] = OK___;
					}
				}
				else
				{
					// Si existe un error en la consulta mostrar� el siguiente mensaje 
					$arrDatos['estatus'] = ERR__;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
					throw new Exception("Ccapturaenrolamiento.php\tobtenerLigaFormatoEnrolamiento"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
				$mensaje = "\tCcapturaenrolamiento.php\tobtenerLigaFormatoEnrolamiento"."\tNo pudo abrir la conexion a PostGreSQL (aforeglobal)";
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "No abrio Conexion a PostGreSQL";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	public static function obtIpTienda()
	{
		$ipCliente = "";
		
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
			$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR']; 
		else
			$ipCliente = $_SERVER['REMOTE_ADDR'];

		return $ipCliente;
	}
	
	public static function verificacionEnrolAutoriza($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = " SELECT fnvalidarenrolautoriza AS retorno FROM fnvalidarenrolautoriza ($iFolio); ";			
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet as $reg)
					{	
						$arrDatos['retorno'] = $reg['retorno'];
						CLogImpresion::escribirLog('Respuesta: '.$arrDatos['retorno']);
					}
					CLogImpresion::escribirLog("FUNCION fnvalidarenrolautoriza (1 = GUARDO HUELLA QUE AUTORIZA, 0 = NO GUARDO): Folio Enrolamiento = ".$iFolio);
				    CLogImpresion::escribirLog("RETORNO DE LA fnvalidarenrolautoriza ".$arrDatos['retorno']);
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	
	}
	
	public static function limpiarHuellasEnrol($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = " SELECT fneliminarhuellasenroltrabajador AS regreso FROM fneliminarhuellasenroltrabajador($iFolio); ";			
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet as $reg)
					{	
						$arrDatos['regreso'] = $reg['regreso'];
					}
					CLogImpresion::escribirLog("FUNCION fneliminarhuellasenroltrabajador (1 = BORRO, 0 = NO BORRO): Folio Enrolamiento = ".$iFolio);
				    CLogImpresion::escribirLog("RETORNO DE LA fneliminarhuellasenroltrabajador ".$arrDatos['regreso']);
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	
	}
	
	public static function actualizarEnrolamiento($iFolio)
	{
		//crea objeto de la clase 
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd)
			{
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql = " SELECT fnactestatusenrolatrabajador AS regreso FROM fnactestatusenrolatrabajador($iFolio, 0); ";			
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet as $reg)
					{	
						$arrDatos['regreso'] = $reg['regreso'];
					}
					if($arrDatos['regreso'] == 1)
					{
						CLogImpresion::escribirLog("Se actualizo correctamente el estatus del enrolamiento, Folio Enrolamiento = ".$iFolio);
					}
					else
					{
						CLogImpresion::escribirLog("Ocurrio un error al actualizar el estatus del enrolamiento, Folio Enrolamiento = ".$iFolio);
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;	
	}
	public static function grabarLogJs($sTexto)
	{
		$arrDatos = array();
		CLogImpresion::escribirLog($sTexto);

		if($sTexto != '')
		{
			$arrDatos['estatus'] = OK___;
			$arrDatos['descripcion'] = "EXITO";
		}
		else
		{
			$arrDatos['estatus'] = ERR__;
		}
		
		return $arrDatos;	
	}
	
	public static function excepcionarDedosEnRolTrabajador($iFolioEnrol)
	{
		$objGn = new CMetodoGeneral();
		//declara un arreglo
		$arrDatos = array();
		try
		{
			//Se abre una conexion
			$cnxBd2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			//Valida si se abrio a bd
			if($cnxBd2)
			{
				CLogImpresion::escribirLog("Entro a la Funcion de validar NIST");
				//En caso de que se abra la conexion, forma la consulta a ejecutar
				$cSql2 = "select fnexcepcionardedosenroltrabajador as resulta from fnexcepcionardedosenroltrabajador($iFolioEnrol);";
				CLogImpresion::escribirLog($cSql2);
				//Ejecuta la consulta
				$resulSet2 = $cnxBd2->query($cSql2);
				//Verifica que se haya ejecutado correctamente
				if($resulSet2)
				{
					//indicador que asigna estatus 1, osea correctamente y su descripcion
					//$arrDatos['descripcion'] = "EXITO";
					foreach($resulSet2 as $reg2)
					{	
						$arrDatos['resulta'] = $reg2['resulta'];
						CLogImpresion::escribirLog("Se ejecuto correctamente la funcion");
				    	CLogImpresion::escribirLog($arrDatos['resulta']);	
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql2);
				}
				//cierra la conexion a base de datos
				$cnxBd2 = null;
			}
			else
			{
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	
	/* NOTA: Pend. 301 - SE INABILITA ESTA SECCION (CAMBIOS QUE SE HABIAN METIDO EN EL Pend. 280), DEBIDO A CAMBIOS EN LA NORMATIVA, Fecha del cambio: 06/Nov/2017
	//INICIO pend. 280
	// METODO PARA VALIDAR LA OBTENCION DEL SELLO DE VOLUNTAD
	public static function validarObtencionDeSelloDeVoluntad($iFolioServicio) // MARIO LARA
	{
		$objGn = new CMetodoGeneral();
		//$arrResp->iCodigoRespuesta = 0;
		//$arrResp->cDescripcion = "";
		
		$arrResp = array();

		try
		{
			//ABRIR CONEXION A AFOREGLOBAL...
			$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//VALIDA SI SE ABRIO LA CONEXION A LA BD...
			if($cnxBdAforeGlobal)
			{
				//INDICA EN EL LOG QUE ENTRO A EJECUTAR LA FUNCION ValidarObtencionDeSelloDeVoluntad...
				CLogImpresion::escribirLog("Entro a la Funcion ValidarObtencionDeSelloDeVoluntad");
				
				//EN CASO DE QYE SE ABRA LA CONEXION, FORMA LA CONSULTA A EJECUTAR...
				$cSql = "select fnvalidarobtenciondesellodevoluntad as estatus from fnvalidarobtenciondesellodevoluntad($iFolioServicio);";
				
				//INDICA EN EL LOG LA FUNCION ValidarObtencionDeSelloDeVoluntad...
				CLogImpresion::escribirLog($cSql);
				
				//EJECUTA LA CONSULTA...
				$resulSet = $cnxBdAforeGlobal->query($cSql);
				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE...
				if($resulSet)
				{
					
					CLogImpresion::escribirLog("Se ejecuto correctamente la funcion: ValidarObtencionDeSelloDeVoluntad");
					
					foreach($resulSet as $registro)
					{
						//$arrResp->iCodigoRespuesta = TRIM($registro['estatus']);
						$arrResp['iCodigoRespuesta'] = TRIM($registro['estatus']);

						CLogImpresion::escribirLog("EstatusSello->".$arrResp['iCodigoRespuesta']);
					}
					CLogImpresion::escribirLog("Finaliza validarObtencionDeSelloDeVoluntad Log 1");
				}
				else
				{
					$arrResp['cDescripcion'] = "Error al realizar la consulta en la base de datos";
				}
				//CIERRA LA CONEXION A BD...
				$cnxBdAforeGlobal = null;
				CLogImpresion::escribirLog("Finaliza validarObtencionDeSelloDeVoluntad Log 2");
			}
			else
			{
				$arrResp['cDescripcion'] = "Error, No Se Abrio La Conexion A BD";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn2->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		CLogImpresion::escribirLog("Finaliza validarObtencionDeSelloDeVoluntad Log 3");
		return $arrResp;
	}
	
	// METODO PARA LA OBETENCION DEL FLUJO DE SERVICIO
	public static function ObtencionFlujoServicio($iFolioServicio) // MARIO LARA
	{
		$objGn = new CMetodoGeneral();
		//$arrResp->cFlujo = "";
		//$arrResp->cDescripcion = "";
		$arrResp = array();
		CLogImpresion::escribirLog("Entro al metodo ObtencionFlujoServicio");
		
		try
		{
			//ABRIR CONEXION A AFOREGLOBAL...
			$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//VALIDA SI SE ABRIO LA CONEXION A LA BD...
			if($cnxBdAforeGlobal)
			{
				//INDICA EN EL LOG QUE ENTRO A EJECUTAR LA FUNCION ObtencionFlujoServicio...
				CLogImpresion::escribirLog("Entro a la Funcion ObtencionFlujoServicio");
				
				//EN CASO DE QYE SE ABRA LA CONEXION, FORMA LA CONSULTA A EJECUTAR...
				$cSql = "select fnobtenerflujoservicio as flujo from fnobtenerflujoservicio($iFolioServicio);";
				
				//INDICA EN EL LOG LA FUNCION ObtenerFlujoServicio...
				CLogImpresion::escribirLog($cSql);
				
				//EJECUTA LA CONSULTA...
				$resulSet = $cnxBdAforeGlobal->query($cSql);
				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE...
				if($resulSet)
				{
					CLogImpresion::escribirLog("Se ejecuto correctamente la funcion: ObtenerFlujoServicio");
					
					foreach($resulSet as $registro)
					{
						//$arrResp->cFlujo = TRIM($registro['flujo']);
						$arrResp['cFlujo'] = TRIM($registro['flujo']);

						CLogImpresion::escribirLog("FlujoServicio->".$arrResp['cFlujo']);
					}
					CLogImpresion::escribirLog("Finaliza Obtencion del Flujo Log 1");
				}
				else
				{
					$arrResp['cDescripcion'] = "Error al realizar la consulta en la base de datos";
				}
				//CIERRA LA CONEXION A BD...
				$cnxBdAforeGlobal = null;
				CLogImpresion::escribirLog("Finaliza Obtencion del Flujo Log 2");
			}
			else
			{
				$arrResp['cDescripcion'] = "Error, No Se Abrio La Conexion A BD";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn2->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		CLogImpresion::escribirLog("Finaliza Obtencion del Flujo Log 3");
		return $arrResp;
	}
	*/

	// METODO PARA EXTRAER LOS DATOS PARAMETRIZABLES
	public static function ObtenerDatosConfiguracion() // MARIO LARA
	{
		$objGn = new CMetodoGeneral();
		//$arrResp->iTotalMinutos = 0;
		//$arrResp->iTotalDias = 0;
		//$arrResp->iTotalIntentos = 0;
		//$arrResp->cDescripcion = "";
		$arrResp = array();
		
		try
		{
			//ABRIR CONEXION A AFOREGLOBAL...
			$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//VALIDA SI SE ABRIO LA CONEXION A LA BD...
			if($cnxBdAforeGlobal)
			{
				CLogImpresion::escribirLog("Entro a la Funcion ObtenerDatosConfiguracion");
				
				//EN CASO DE QYE SE ABRA LA CONEXION, FORMA LA CONSULTA A EJECUTAR...
				$cSql = "SELECT totalminutos, totaldias, totalintentos FROM fnobtenervaloresconfiguracion();";
				
				//INDICA EN EL LOG LA FUNCION ObtenerDatosConfiguracion...
				CLogImpresion::escribirLog($cSql);
				
				//EJECUTA LA CONSULTA...
				$resulSet = $cnxBdAforeGlobal->query($cSql);
				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE...
				if($resulSet)
				{
					CLogImpresion::escribirLog("Se ejecuto correctamente la funcion: ObtenerDatosConfiguracion");
					
					foreach($resulSet as $registro)
					{
						$arrResp['iTotalMinutos'] = TRIM($registro['totalminutos']);
						CLogImpresion::escribirLog("Totalmin->".$arrResp['iTotalMinutos']);
						
						$arrResp['iTotalDias'] = TRIM($registro['totaldias']);
						CLogImpresion::escribirLog("Totaldias->".$arrResp['iTotalDias']);
						
						$arrResp['iTotalIntentos'] = TRIM($registro['totalintentos']);
						CLogImpresion::escribirLog("Totalint->".$arrResp['iTotalIntentos']);
					}

					CLogImpresion::escribirLog("Finaliza Datos Configuracion Log 1");
				}
				else
				{
					$arrResp['cDescripcion'] = "Error al realizar la consulta en la base de datos";
				}
				//CIERRA LA CONEXION A BD...
				$cnxBdAforeGlobal = null;
				CLogImpresion::escribirLog("Finaliza Datos Configuracion Log 2");
			}
			else
			{
				$arrResp['cDescripcion'] = "Error, No Se Abrio La Conexion A BD";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		CLogImpresion::escribirLog("Finaliza Datos Configuracion Log 3");
		return $arrResp;
	}
	
	/*
	public static function RechazarEnrolamiento($iFolioServicio)
	{
		$objGn = new CMetodoGeneral();
		//$arrResp->iCodigoRespuesta = 0;
		//$arrResp->cDescripcion = "";
		$arrResp = array();
		$iOpcion = 0;
		$iEstatus = 0;
		try
		{
			//ABRIR CONEXION A AFOREGLOBAL...
			$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//VALIDA SI SE ABRIO LA CONEXION A LA BD...
			if($cnxBdAforeGlobal)
			{
				CLogImpresion::escribirLog("Entro a la Funcion fnrechazarenrolamiento");
				
				//EN CASO DE QYE SE ABRA LA CONEXION, FORMA LA CONSULTA A EJECUTAR...
				$cSql = "SELECT fnrechazarenrolamiento as resultado FROM fnrechazarenrolamiento($iOpcion, $iFolioServicio, $iEstatus );";
				
				CLogImpresion::escribirLog("Inicio de la funcion: ".$cSql);
				
				//EJECUTA LA CONSULTA...
				$resulSet = $cnxBdAforeGlobal->query($cSql);
				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE...
				if($resulSet)
				{					
					CLogImpresion::escribirLog("Se ejecuto la funcion: ".$cSql);
					
					foreach($resulSet as $registro)
					{
						$arrResp['iCodigoRespuesta'] = TRIM($registro['resultado']); 
						CLogImpresion::escribirLog("Resultado de Ejecucion: ".$arrResp['iCodigoRespuesta'] ); 
					}
				}
				else
				{
					$arrResp['cDescripcion'] = "Error al realizar la consulta en la base de datos.";
					CLogImpresion::escribirLog("Error al realizar la consulta en la base de datos.");
				}
				//CIERRA LA CONEXION A BD...
				$cnxBdAforeGlobal = null;
			}
			else
			{
				$arrResp['cDescripcion'] = "Error, No Se Abrio La Conexion A BD";
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn2->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrResp;
	}
	//FIN pend. 280
	*/
	
	/*
	public static function obtenerValorNss($iFolioServicio) // MARIO LARA
	{
	CLogImpresion::escribirLog("a111");
		$objGn = new CMetodoGeneral();
		//$cValorNss = '';
		$cDatosResp = array();
		$cDatosResp['cNss'] = '';

		try
		{
		CLogImpresion::escribirLog("a2");
			//ABRIR CONEXION A AFOREGLOBAL...
			$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			//VALIDA SI SE ABRIO LA CONEXION A LA BD...
			if($cnxBdAforeGlobal)
			{
			CLogImpresion::escribirLog("a3");
				//INDICA EN EL LOG QUE ENTRO A EJECUTAR LA FUNCION obtenerValorNss...
				CLogImpresion::escribirLog("Entro a la Funcion obtenerValorNss");
				
				//EN CASO DE QYE SE ABRA LA CONEXION, FORMA LA CONSULTA A EJECUTAR...
				$cSql = "select nss, folioenrolamiento, flujoservicio from recserviciosafore Where folioservicio = $iFolioServicio Limit 1;";
				
				//INDICA EN EL LOG LA FUNCION obtenerValorNss...
				CLogImpresion::escribirLog($cSql);
				
				//EJECUTA LA CONSULTA...
				$resulSet = $cnxBdAforeGlobal->query($cSql);
				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE...
				if($resulSet)
				{
					
					CLogImpresion::escribirLog("Se ejecuto correctamente la funcion: obtenerValorNss");
					
					foreach($resulSet as $registro)
					{
						//$cValorNss = TRIM($registro['nss']);
						$cDatosResp['cNss'] = TRIM($registro['nss']);
						$cDatosResp['iFolioEnrolamiento'] = $registro['folioenrolamiento'];
						$cDatosResp['cFlujoServicio'] = TRIM($registro['flujoservicio']);

						CLogImpresion::escribirLog("Valor NSS->".$cValorNss);
					}
					
					CLogImpresion::escribirLog("Finaliza obtenerValorNss Log 1");
				}
				else
				{
					$cDatosResp['cNss'] = '-1';
				}
				
				//CIERRA LA CONEXION A BD...
				$cnxBdAforeGlobal = null;
				CLogImpresion::escribirLog("Finaliza obtenerValorNss Log 2");
			}
			else
			{
				$cDatosResp['cNss'] = '-1';
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn2->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		    $cDatosResp['cNss'] = '-1';
		}
		CLogImpresion::escribirLog("Finaliza obtenerValorNss");

		return $cDatosResp;
	}
	*/
	
	public static function llamarMarcaEI($iEmpleado, $cFolioServicio)  
	{
		$cDatosRespuesta = array();
		//$cDatosRespuesta = obtenerValorNss($cFolioServicio);
		$cNss = '';
		$iFolioEnrolamiento = 0;
		$cFlujoServicio = '';
		
		
		
		
		
		
		//inicia nueva funcion
		$objGn = new CMetodoGeneral();
		//$cValorNss = '';
		$cDatosResp = array();
		$cDatosResp['cNss'] = '';

		try
		{
			//ABRIR CONEXION A AFOREGLOBAL...
			$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		
			//VALIDA SI SE ABRIO LA CONEXION A LA BD...
			if($cnxBdAforeGlobal)
			{
				//INDICA EN EL LOG QUE ENTRO A EJECUTAR LA FUNCION obtenerValorNss...
				CLogImpresion::escribirLog("Entro a la Funcion obtenerValorNss");
				
				//EN CASO DE QYE SE ABRA LA CONEXION, FORMA LA CONSULTA A EJECUTAR... // Se añade codigo motivo para validacion por tipo de servicio..
				$cSql = "select nss, folioenrolamiento, flujoservicio, codigomotivo from recserviciosafore Where folioservicio = $cFolioServicio;";
				
				//INDICA EN EL LOG LA FUNCION obtenerValorNss...
				CLogImpresion::escribirLog($cSql);
				
				//EJECUTA LA CONSULTA...
				$resulSetAux = $cnxBdAforeGlobal->query($cSql);
				
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE...
				if($resulSetAux)
				{
					CLogImpresion::escribirLog("Se ejecuto correctamente la funcion: obtenerValorNss");
					
					foreach($resulSetAux as $registroAux)
					{
						$cDatosResp['cNss'] = TRIM($registroAux['nss']);
						$cDatosResp['iFolioEnrolamiento'] = $registroAux['folioenrolamiento'];
						$cDatosResp['cFlujoServicio'] = TRIM($registroAux['flujoservicio']);
						$cDatosResp['iCodigoMotivo'] = $registroAux['codigomotivo']; // Se añade para validacion del tipo de servicio antes de marcar.
						
						$cNssAux = $cDatosResp['cNss'];
						$iFolioEnrolamientoAux = $cDatosResp['iFolioEnrolamiento'];
						$cFlujoServicioAux = $cDatosResp['cFlujoServicio'];
						$iCodigoMotivoAux = $cDatosResp['iCodigoMotivo']; // Se añade para validacion del tipo de servicio antes de marcar.
						
						CLogImpresion::escribirLog("Entro a Leer: NSS: ".$cNssAux.", FolioEnrolamiento: ".$iFolioEnrolamientoAux.", FlujoServicio: ".$cFlujoServicioAux.", CodigoMotivo: ".$iCodigoMotivoAux);
					}
					//Se añade el codigomotivo a los log. 
					CLogImpresion::escribirLog("Datos: NSS: ".$cNssAux.", FolioEnrolamiento: ".$iFolioEnrolamientoAux.", FlujoServicio: ".$cFlujoServicioAux.", CodigoMotivo: ".$iCodigoMotivoAux);
					CLogImpresion::escribirLog("Finaliza obtenerValorNss Log 1");
				}
				else
				{
					$cDatosResp['cNss'] = '-1';
				}
				
				//CIERRA LA CONEXION A BD...
				$cnxBdAforeGlobal = null;
				CLogImpresion::escribirLog("Finaliza obtenerValorNss Log 2");
			}
			else
			{
				$cDatosResp['cNss'] = '-1';
			}
			
			
			/*$cNss = TRIM($cDatosRespuesta['cNss']);
			$iFolioEnrolamiento = $cDatosRespuesta['iFolioEnrolamiento'];
			$cFlujoServicio = TRIM($cDatosRespuesta['cFlujoServicio']);*/
			
			$cNss = TRIM($cDatosResp['cNss']);
			$iFolioEnrolamiento = $cDatosResp['iFolioEnrolamiento'];
			$cFlujoServicio = TRIM($cDatosResp['cFlujoServicio']);
			$iCodigoMotivo = $cDatosResp['iCodigoMotivo'];
			
			if ($cFlujoServicio == '3A')
			{
				//if ($cNss != '-1' && strlen($cNss) == 11 && $iFolioEnrolamiento > 0 && $cFlujoServicio == '3A')
				if ($cNss != '-1' && strlen($cNss) == 11 && $iFolioEnrolamiento > 0 )
				{
					//$datos = new stdClass();    
					//$datosServicio = new stdClass();
					$cnxOdbc =  new PDO( "informix:host=".IPSERVIDORINF."; service=1521; database=".BASEDEDATOSINF."; server=safre_tcp; protocol=onsoctcp; EnableScrollableCursors=1", USUARIOINF, PASSWORDINF);
									
					$cSqlCons = null;
					$cDescServicio = null;
					$iResultadoGuardado = array("iActualizacionrecservicios"=>0,"iActualizacion"=>0,"marca"=>'', "codigo"=>0,"descripcion"=>'',"codigoRespuesta"=>0,"descripcionretorno"=>'');
					if ($cnxOdbc) 
					{
						
	
						
						//SECCION PARA OBTENER EL CONSECUTIVO
						$cSqlCons = "EXECUTE FUNCTION fn_obten_ret_consecutivo();";
						CLogImpresion::escribirLog('Funcion del Consecutivo: '.$cSqlCons);
						$resultSetCons = $cnxOdbc->query($cSqlCons);			
						if ($resultSetCons) 
						{
							foreach ($resultSetCons as $resultadoCons) 
							{														
								$iConsecutivo = $resultadoCons[0];
							}
							CLogImpresion::escribirLog('Consecutivo de la Marca --> '.$iConsecutivo);
						}	
						else
						{
							CLogImpresion::escribirLog( '[ConsecutivoMarcarCuenta] Error en la consulta: '.$cSqlCons);
						}
						//FIN SECCION PARA OBTENER EL CONSECUTIVO
						
						
						$cSql2 = null;
						if ($iCodigoMotivo == 2022)
						{
							$cSql2 = "EXECUTE FUNCTION fn_afop_marcarcuenta(1, '$cNss', $iConsecutivo, 'MD', '$iEmpleado') ";
							CLogImpresion::escribirLog( '[ConsecutivoMarcarCuenta] Se marco como Modificacion de Datos: '.$cSql2);
						}
						else 
						{
							$cSql2 = "EXECUTE FUNCTION fn_afop_marcarcuenta(1, '$cNss', $iConsecutivo, 'AE', '$iEmpleado') ";	
							CLogImpresion::escribirLog( '[ConsecutivoMarcarCuenta] Se marco como Alta de Expediente: '.$cSql2);
						}
						//..Entra validacion anterior para ver el tipo de movimiento antes de marcar la cuenta. Se invalida la de abajo.
						//$cSql2 = "EXECUTE FUNCTION fn_afop_marcarcuenta(1, '$cNss', $iConsecutivo, 'AE', '$iEmpleado') "; 
						
	
						CLogImpresion::escribirLog($cSql2);
						$resultSet = $cnxOdbc->query($cSql2);   
						if ($resultSet) 
						{
							foreach ($resultSet as $resultado) 
							{              
								$iResultadoGuardado['marca'] = $resultado[0];
								$iResultadoGuardado['codigo'] = $resultado[1];
								$iResultadoGuardado['descripcion'] = $resultado[2];
							}
							CLogImpresion::escribirLog($iResultadoGuardado['codigo']);
	
							if ($iResultadoGuardado['codigo'] == 0)
							{
								
								$cnxOdbc2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
								$iResultadoGuardado["codigoRespuesta"] = OK__;
								$iResultadoGuardado["descripcionretorno"] = "";
								CLogImpresion::escribirLog( '[llamarMarcaEI] Cuenta Marcada --> '.$cFolioServicio.',Marca: '.$iResultadoGuardado['marca']);
								CLogImpresion::escribirLog('Codigo: '.$iResultadoGuardado['codigo']);
								CLogImpresion::escribirLog('Codigo: '.$iResultadoGuardado['descripcion']);
	
								/*$cSql3 = null;
								$cSql3 = "UPDATE enroltrabajadores SET estatus = 199 WHERE folioenrolamiento = $iFolioEnrolamiento;";
	
								$resultSet2 = $cnxOdbc2->query($cSql3);
								if ($resultSet) 
								{
									$iResultadoGuardado['iActualizacion'] = 1;
								*/
									//Actualizar recServiciosAfore 
									$cSql4 = null;
									$cSql4 = "UPDATE recserviciosafore SET consecutivomarca = $iConsecutivo WHERE folioservicio = $cFolioServicio";
									CLogImpresion::escribirLog($cSql4);
									$resultSet3 = $cnxOdbc2->query($cSql4);
	
									if ($resultSet3) 
									{
										$iResultadoGuardado['iActualizacionrecservicios'] = 1;
										CLogImpresion::escribirLog('Se actualizo el consecutivo en la recServiciosAfore: '.$iResultadoGuardado['iActualizacionrecservicios']);
									}
									else
									{
										$datos['codigoRespuesta'] = ERR__;
										$datos['descripcion'] = "Ocurrio un problemas en la conexion en la base de datos.";
										//Enviamos el error al log
										$arrErr = $cnxOdbc2->errorInfo();
										CLogImpresion::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
									}
								/*}
								else
								{
									$datos['codigoRespuesta'] = ERR__;
									$datos['descripcion'] = "Ocurrio un problemas en la conexion en la base de datos.";
									//Enviamos el error al log
									$arrErr = $cnxOdbc2->errorInfo();
									CLogImpresion::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
								}*/
								$cnxOdbc2 = null;
							}
						} 
						else
						{
							$iResultadoGuardado["codigoRespuesta"] = ERR__;
							$iResultadoGuardado["descripcionretorno"] = "Error al Marcar la Cuenta.";
							//Enviamos el error al log
							//$arrErr = $cnxOdbc->errorInfo();
							CLogImpresion::escribirLog( '[llamarMarcaEI] Error en la consulta: Folio --> '.$cFolioServicio);
						}
						$cnxOdbc = null;
					} 
					else 
					{
						$iResultadoGuardado["codigoRespuesta"] = ERR__;
						$iResultadoGuardado["descripcionretorno"] = "Ocurri&oacute; un error de conexion a informix al consultar la marca";
						//Enviamos el error al log
						//$arrErr = $cnxOdbc->errorInfo();
						CLogImpresion::escribirLog( '[llamarMarcaEI] Error consulta: --> ' .$cFolioServicio );
					}
				}
				else
				{
					CLogImpresion::escribirLog( '[llamarMarcaEI] FolioServicio: '.$cFolioServicio.', Nss: '.$cNss.', FolioEnrolamiento: '.$iFolioEnrolamiento.', FlujoServicio: '.$cFlujoServicio);
				}
			}

			$cnxOdbc = null;
			
			
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		    $cDatosResp['cNss'] = '-1';
		}
		CLogImpresion::escribirLog("Finaliza obtenerValorNss");
		
 	}

 	public static function validarLevantamientoEI($iFolio, $sCurpTrabajador)
	{
		$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		try
		{
			$cSql = "SELECT tieneexpiden FROM fnconsultarestatusbiometricos($iFolio, '$sCurpTrabajador')";
			CLogImpresion::escribirLog("[CEnrolTrabajador::validarLevantamientoEI] --> Funcion: ".$cSql);

			if($cnxBd)
			{
				//ejecutamos el query
				$resultset = $cnxBd->query($cSql);
				
				if($resultset)
				{
					$datos['tieneExp'] = -1;
					foreach ($resultset as $reg) 
					{	
						$datos['codigoRespuesta'] = 1;
						$datos['tieneExp'] = $reg['tieneexpiden'];	
					}
					CLogImpresion::escribirLog("[CEnrolTrabajador::validarLevantamientoEI] --> Respuesta: ".$datos['tieneExp']);
				}
				else
				{
					$datos['codigoRespuesta'] = ERR__;
					$datos['descripcion'] = "Ocurrio un error al realizar la consulta a la base de datos metodo --> fnconsultarestatusbiometricos .";
					//Enviamos el error al log
					$arrErr = $cnxBd->errorInfo();
					CLogImpresion::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$datos['codigoRespuesta'] = ERR__;
				$datos['descripcion'] = "Ocurrio un problemas en la conexion en la base de datos.";
				//Enviamos el error al log
				$arrErr = $cnxBd->errorInfo();
				CLogImpresion::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			//NOS ASEGURAMOS DE CERRAR LA CONEXION
			$cSql = "";
			$cnxBD = null;
			//$resultset = null;
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    CLogImpresion::escribirLog( '[' . __FILE__ . ']' . $mensaje);
		}
		return $datos;	
	}

	
	//inicia peticion 316
	public static function fnObtenerIpServidor()
	{
		$ipServidor = null;

		$ipServidor = IPSERVIDOR;

		return $ipServidor;
	}

	public static function fnconsultarnumempleado($iEmpleado)
	{
		$arrDatos = array();
		$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		if($cnxBdAforeGlobal)
		{
			$cSql = "SELECT fnconsultarnumempleado AS resultado FROM fnconsultarnumempleado(".$iEmpleado.");";
			CLogImpresion::escribirLog('[fnconsultarnumempleado]-> '.$cSql);

			$resulSet = $cnxBdAforeGlobal->query($cSql);

			if($resulSet)
			{
				$arrDatos['status'] = 1;
				$arrDatos['descripcion'] = "CONSULTA EXITOSA";
				foreach ($resulSet as $reg) {
					$arrDatos['respuesta'] = $reg['resultado'];
				}
			}
			else
			{
				$arrDatos['status'] = -1;
				$arrDatos['descripcion'] = "OCURRIO UN ERROR AL CONSULTAR DATOS DEL GERENTE";
			}
			$cnxBdAforeGlobal = null;
		}
		else {
			CLogImpresion::escribirLog('NO SE ESTABLECIO CONEXION CON AFOREGLOBAL ->'.IPSERVIDOR.' - '.BASEDEDATOS);
		}
		CLogImpresion::escribirLog('[Respuesta] -> '.$arrDatos['descripcion']);
		
		return $arrDatos;
	}

	public static function fnobtenermensajegerenteenrol($opcionmensaje)
	{
		$arrDatos = array();
		$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		if($cnxBdAforeGlobal)
		{
			$cSql = "SELECT idmensaje, mensaje FROM fnobtenermensajegerenteenrol(".$opcionmensaje.");";
			CLogImpresion::escribirLog('[fnobtenermensajegerenteenrol]-> '.$cSql);
			$resulSet = $cnxBdAforeGlobal->query($cSql);
			if($resulSet){
				$arrDatos['status'] = 1;
				$arrDatos['descripcion'] = 'CONSULTA EXITOSA';
				foreach ($resulSet as $reg) {
					$arrDatos['datosmensaje']['idmensaje'] = $reg['idmensaje'];
					$arrDatos['datosmensaje']['mensaje'] = TRIM($reg['mensaje']);
				}
			}
			else{
				$arrDatos['status'] = -1;
				$arrDatos['descripcion'] = 'OCURRIO UN ERROR AL OBTENER EL MENSAJE PARA EL GERENTE';
			}
			$cnxBdAforeGlobal = null;
		}
		else{
			CLogImpresion::escribirLog('NO SE ESTABLECIO CONEXION CON AFOREGLOBAL ->'.IPSERVIDOR.' - '.BASEDEDATOS);
		}
		CLogImpresion::escribirLog('[Respuesta] -> '.$arrDatos['descripcion']);
		return $arrDatos;
	}

	public static function fnguardarbitacoraenrolamiento($iFolioSolicitud, $iFolioEnrol, $iEmpleado, $iNumEmpGerente, $curp, $iautorizaciongerencial, $itipoenrolamiento, $iExcepcionesHuellas)
	{
		$objGn = new CMetodoGeneral();
		$ipModulo = NULL;
		$ipModulo = $objGn->getRealIP();
		$iTienda = null;
		$arrDatos = array();
		//obtenemos el numero de tienda primero
		$cSql = null;
		$cSql = "SELECT fnobtenertienda AS tienda FROM fnobtenertienda('".$ipModulo."');";
		// guardamos en el logx la consulta
		CLogImpresion::escribirLog('[fnguardarbitacoraenrolamiento] OBTENER TIENDA -> '.$cSql);
		// se abre conexion a aforeglobal
		$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		if( $cnxBdAforeGlobal )
		{
			// se ejecuta la consulta
			$resulSet = $cnxBdAforeGlobal->query($cSql);
			// verificamos resultados
			if( $resulSet )
			{
				foreach ($resulSet as $reg) {
					$iTienda = $reg['tienda'];
				}
				// limpiamos la variable para volverla a usar
				$cSql = null;
				// consulta para guardar en la bitacora del enrolamiento
				$cSql = "SELECT fnregistrarbitacoraautorizacionenrolamiento AS resultado FROM fnregistrarbitacoraautorizacionenrolamiento(".$iFolioSolicitud.", ".$iFolioEnrol.",".$iTienda.",".$iEmpleado.", ".$iNumEmpGerente.", '".$curp."', ".$iautorizaciongerencial.", ".$itipoenrolamiento.", ".$iExcepcionesHuellas.");";
				// guardamos en el logx la consulta
				CLogImpresion::escribirLog("[fnguardarbitacoraenrolamiento] GUARDAR EN BITACORA -> ".$cSql);
				try
				{
					// ejecutamos la consulta
					$resulSet = $cnxBdAforeGlobal->query($cSql);

					// validamos el resultado
					if( $resulSet ){
						foreach ($resulSet as $reg) {
							$arrDatos['status'] = 1;
							$arrDatos['descripcion'] = 'CONSULTA EXITOSA';
							$arrDatos['resultado'] = $reg['resultado'];
						}
					}
					else{
						$arrDatos['status'] = -1;
						$arrDatos['descripcion'] = 'OCURRIO UN ERROR AL GUARDAR EN BITACORA';
					}
					$cnxBdAforeGlobal = null;
				}
				catch (Exception $e)
				{
					//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
					$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
					CLogImpresion::escribirLog( '[' . __FILE__ . ']' . $mensaje);
					$arrDatos['status'] = -1;
					$arrDatos['descripcion'] = 'OCURRIO UN ERROR AL GUARDAR EN BITACORA';
				}
			}
			else{
				$arrDatos['status'] = -1;
				$arrDatos['descripcion'] = 'OCURRIO UN ERROR AL OBTENER EL NUMERO DE TIENDA';
			}
			$cnxBdAforeGlobal = null;
		}
		else{
			CLogImpresion::escribirLog('NO SE ESTABLECIO CONEXION CON AFOREGLOBAL ->'.IPSERVIDOR.' - '.BASEDEDATOS);
		}
		$cnxBdAforeGlobal = null;
		CLogImpresion::escribirLog('[Respuesta] -> '.$arrDatos['descripcion']);
		return $arrDatos;
	}

	public static function fnobtenerclaveoperacion($iFolioSolicitud)
	{
		$objGn = new CMetodoGeneral();
		$arrDatos = array();
		$cSql = null;
		
		//se crea cosnulta a base de datos
		$cSql = "SELECT fnobtenerdoctosdigitalizacion AS resultado FROM fnobtenerdoctosdigitalizacion(".$iFolioSolicitud.", 3);";
		CLogImpresion::escribirLog("[fnobtenerclaveoperacion] consulta -> ".$cSql);
		//se abre conexion afore global
		$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		//se valida conexion
		if( $cnxBdAforeGlobal )
		{
			// se ejecuta la consulta
			$resulSet = $cnxBdAforeGlobal->query($cSql);

			if( $resulSet )
			{
				foreach ($resulSet as $reg ) {
					$arrDatos['status'] = 1;
					$arrDatos['descripcion'] = 'CONSULTA EXITOSA';
					$arrDatos['claveoperacion'] = $reg['resultado'];
				}
			}
			else {
				$arrDatos['status'] = -1;
				$arrDatos['descripcion'] = 'Ocurrio un error al obtener la clave de operacion';
				CLogImpresion::escribirLog('[fnobtenerclaveoperacion]-> OCURRIO UN ERROR AL OBTENER LOS DATOS DE LA CONSULTA');
			}
			$cnxBdAforeGlobal = null;
		}
		else {
			CLogImpresion::escribirLog('[fnobtenerclaveoperacion] -> NO SE ESTABLECIO CONEXION CON AFOREGLOBAL ->'.IPSERVIDOR.' - '.BASEDEDATOS);
			$arrDatos['status'] = -1;
			$arrDatos['descripcion'] = 'NO SE ABRIO CONEXION A BASE DE DATOS';
		}
		CLogImpresion::escribirLog('[fnobtenerclaveoperacion] -> '.$arrDatos['descripcion']);
		$cnxBdAforeGlobal = null;
		return $arrDatos;
	}

	public static function f_obtenerclaveoperacionservicios($iFolioSolicitud)
	{
		$cSql = null;
		$arrDatos = array();
		$claveoperacion = null;

		//se crea la consulta a base de datos para obtener la clave de operacion cuando sea un servicio (entro con parametro 2040 en la liga del enrolamiento)
		$cSql = "SELECT fnobtenerdoctosdigitalizacionservicios AS claveoperacion FROM fnobtenerdoctosdigitalizacionservicios('".$iFolioSolicitud."', 50);";
		CLogImpresion::escribirLog("[f_obtenerclaveoperacionservicios] CONSULTA ->" . $cSql);
		//se abre conexion AFOREGLOBAL
		$cnxBdAforeGlobal =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		//se prueba conexion
		if( $cnxBdAforeGlobal )
		{
			//se ejecuta la consulta
			$resultado = $cnxBdAforeGlobal->query($cSql);
			//validamos la ejecucion de la funcion
			if( $resultado )
			{
				foreach ( $resultado as $r ) {
					$arrDatos['status'] = 1;
					$arrDatos['descripcion'] = 'CONSULTA EXITOSA';
					$claveoperacion = substr( TRIM($r['claveoperacion']) , 0, 3);
					$arrDatos['claveoperacion'] = $claveoperacion;
				}
			}
			else
			{
				$arrDatos['status'] = -1;
				$arrDatos['descripcion'] = 'ERROR AL OBTENER LOS DATOS DE LA CONSULTA';
				CLogImpresion::escribirLog("[f_obtenerclaveoperacionservicios] ERROR AL OBTENER LOS DATOS DE LA CONSULTA");
			}
			$cnxBdAforeGlobal = null;
		}
		else
		{
			CLogImpresion::escribirLog("[f_obtenerclaveoperacionservicios] -> NO SE ESTABLECIO CONEXION CON AFOREGLOBAL -> ".IPSERVIDOR.' - '.BASEDEDATOS);
			$arrDatos['status'] = -1;
			$arrDatos['descripcion'] = 'NO SE ABRIO CONEXION A BASE DE DATOS';
		}
		CLogImpresion::escribirLog('[f_obtenerclaveoperacionservicios] -> '.$arrDatos['descripcion']);
		$cnxBdAforeGlobal = null;
		return $arrDatos;
	}

	//fin peticion 316

	//inicia pendiente 603
	public static function verificarFolio($curp,$iFolioSolicitud,$solicitud)
	{
		//crea objeto de la clase 
		 $objGn = new CMetodoGeneral();
		 //declara un arreglo
		 $arrDatos = array();
		 try
		 {
			 //Se abre una conexion
			 $cnxBd2 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
 
			 //Valida si se abrio a bd
			 if($cnxBd2)
			 {
				 CLogImpresion::escribirLog("Entro a verificar si existe folio del dia de hoy: ");
				 //En caso de que se abra la conexion, forma la consulta a ejecutar
				 $cSql2 = "select folioenrolamiento,excepcion,selecciono,numeroempleadoautoriza  from fnverificarenrolamientotrabajadordiahoy('$curp',$iFolioSolicitud,$solicitud);";
				//select folioenrolamiento,excepcion from fnverificarenrolamientotrabajadordiahoy_nueva('CATF920317HVZGTR01',32917229,2040);

				 CLogImpresion::escribirLog($cSql2);
				 //Ejecuta la consulta
				 $resulSet2 = $cnxBd2->query($cSql2);
				 //Verifica que se haya ejecutado correctamente
				 if($resulSet2)
				 {
					 //indicador que asigna estatus 1, osea correctamente y su descripcion
					 //$arrDatos['descripcion'] = "EXITO";
					 foreach($resulSet2 as $reg2)
					 {	
						 $arrDatos['folioenrolamiento'] = $reg2['folioenrolamiento'];
						 $arrDatos['excepcion'] = $reg2['excepcion'];
						 $arrDatos['selecciono'] = $reg2['selecciono'];//se agrega para saber que radio button selecciono
						 $arrDatos['numeroempleadoautoriza'] = $reg2['numeroempleadoautoriza'];
						 CLogImpresion::escribirLog("Se Ejecuto la Funcion Correctamente" ."\n". 
													 'Folioenrolamiento: '.$arrDatos['folioenrolamiento']."\n" .
													 'Excepcion: '.$arrDatos['excepcion']."\n" . 
													 'selecciono: '.$arrDatos['selecciono']."\n" . 
													 'numeroempleadoautoriza: '.$arrDatos['numeroempleadoautoriza']);
						//  CLogImpresion::escribirLog('Folioenrolamiento: '.$arrDatos['folioenrolamiento']);
						//  CLogImpresion::escribirLog('Excepcion: '.$arrDatos['excepcion']);
						//  CLogImpresion::escribirLog('selecciono: '.$arrDatos['selecciono']);
						//  CLogImpresion::escribirLog('numeroempleadoautoriza: '.$arrDatos['numeroempleadoautoriza']);
					 }
				 }
				 else
				 {
					 CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql2);
				 }
				 //cierra la conexion a base de datos
				 $cnxBd2 = null;
			 }
			 else
			 {
			 }
		 }
		 catch (Exception $e)
		 {
			 //Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode().'SQL->'.$cSql;
			 $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		 }
		 return $arrDatos;
	 }// fin pendiente 603

	 //PDTE 961: Funcion para guardar los parametros generados al ejecutar aplicaciones externas a la pagina y el response.
	 public static function guardarParametrosControl($cParametrosEnvio,$iFolioEnrolamiento,$iResponse,$iOpcion){

		$arrDatos = array();

		try {
			
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{
				CLogImpresion::escribirLog("Se guardan parametros de ejecucion");

				$ipTienda = CEnrolTrabajador::obtIpTienda();

				$cSql = "SELECT fnguardarparametroscontrol AS respuesta FROM fnguardarparametroscontrol('$cParametrosEnvio',$iFolioEnrolamiento,$iResponse,$iOpcion,'$ipTienda');";

				//CLogImpresion::escribirLog($cSql);
				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);
				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{

					foreach($resulSet as $reg)
					{	
						$arrDatos['respuesta'] = $reg['respuesta'];
					}
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql);
				}
				//cierra la conexion a base de datos
				$cnxBd = null;
			}
			else
			{
				CLogImpresion::escribirLog('Error al abrir conexion a base de datos');
			}

		} catch (Exception $e) {

			$mensaje = 'Excepcion: '. $e->getMessage() . ' Linea: ' . $e->getLine() . ' Codigo: ' . $e->getCode(). ' SQl: '. $cSql;
			CLogImpresion::escribirLog($mensaje);

		}

		return $arrDatos;

	}
	
	
	public static function guardarExcepcion($chCurp)
	{
		
		CLogImpresion::escribirLog('ENTRO A LA FUNCION DE GUARDAREXCEPCION');
		$arrDatos31 = array();
		$arrSecundario = array();

		try {
			
			$cnxBd31 =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd31)
			{
				//CLogImpresion::escribirLog("entro a guardar excepcion");

				$cSql31 = "SELECT fnretornaexcepcion AS excepcion FROM fnretornaexcepcion('$chCurp');";

				//CLogImpresion::escribirLog($cSql);
				//Ejecuta la consulta
				$resulSet31 = $cnxBd31->query($cSql31);
				//Verifica que se haya ejecutado correctamente
				if($resulSet31)
				{
				CLogImpresion::escribirLog('Funcion a consumir : '.$cSql31);
					foreach($resulSet31 as $reg)
					{	
						$arrDatos31['excepcion'] = $reg['excepcion'];
						
					}
					
					CLogImpresion::escribirLog('Valor de retorno de la excepcion : '.$arrDatos31['excepcion']);
				}
				else
				{
					CLogImpresion::escribirLog('Excepcion al Ejecutar la Funcion: '.$cSql31);
				}
				//cierra la conexion a base de datos
				$cnxBd31 = null;
				$arrSecundario = $arrDatos31;
			}
			else
			{
				CLogImpresion::escribirLog('Error al abrir conexion a base de datos');
			}

		} catch (Exception $e) {

			$mensaje = 'Excepcion: '. $e->getMessage() . ' Linea: ' . $e->getLine() . ' Codigo: ' . $e->getCode(). ' SQl: '. $cSql31;
			CLogImpresion::escribirLog($mensaje);
		}
		//	var_dump($arrDatos31);
			return $arrSecundario;
		//return $arrDatos31;
		
	}
	
	public static function verificaconsecutivoeibio($iFolioServicio)
	{
		$cnxOdbc =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
		$iResultadoconsecutivo;
		
		if ($cnxOdbc) 
		{
			$cSql = "SELECT fnverificaconsecutivoeibio AS respuesta FROM fnverificaconsecutivoeibio($iFolioServicio);";
			CLogImpresion::escribirLog("[CEnrolTrabajador::] --> Funcion: verificar_consecutivo -->  ".$cSql);
			$resultsetFolio = $cnxOdbc->query($cSql);
			if($resultsetFolio)
			{
				foreach ($resultsetFolio as $regfolio) 
				{
					$iResultadoconsecutivo = $regfolio["respuesta"];
				}
			}
			else
			{
				CLogImpresion::escribirLog("Ocurrio un error al realizar la consulta a la base de datos metodo --> verificaconsecutivoeibio.");
				//Enviamos el error al log
				CLogImpresion::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}

			$cnxOdbc = null;
			return $iResultadoconsecutivo;
		}
	}

	
} //Fin de la clase 
?>